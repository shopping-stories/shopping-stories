# Shopping Stories

**`Documentation`** |
------------------- |
[![API Documentation](https://img.shields.io/badge/api-reference-blue.svg)](http://docs.shoppingstories.org/guide/express/overview.html) 
[![Database Documentation](https://img.shields.io/badge/data-reference-blue.svg)](http://docs.shoppingstories.org/guide/neo4j/neo4j-getting-started.html) |


## Table of Contents
* [About the Project](https://shoppingstories.org/)



## About the Project


