import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'


Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: "Shopping Stories",
    }
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    meta: {
      title: 'About'
    }
  },
  {
    path:'/dashboard/user-settings',
    name:'User Settings',
    component:()=>import('../components/UserSettings.vue'),
    meta:{
      title:'UserSettings'
    }
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search.vue'),
    meta: {
      title: 'Search'
    }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('../views/Dashboard.vue'),
    meta: {
      title: 'Dashboard'
    }
  },
  {
    path: '/project',
    name: 'Project',
    component: () => import('../views/Project.vue'),
    meta: {
      title: 'About Project'
    }
  },
  {
    path: '/acknowledgements',
    name: 'Acknowledgements',
    component: () => import('../views/Acknowledgements.vue'),
    meta: {
      title: 'Acknowledgments'
    }
  },
  {
    path: '/ledgers',
    name: 'Ledgers',
    component: () => import('../views/Ledgers'),
    meta: {
      title: 'How Currency Worked'
    }
  },
  {
    path: '/transcription',
    name: 'Transcription',
    component: () => import('../views/Transcription.vue'),
    meta: {
      title: 'Transcription Process'
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/Profile'),
    meta: {
      title: 'Profile'
    }
  },
  {
    path: '/auth/signin',
    name: 'Signin',
    component: () => import('../views/Signin'),

    meta: {
      title: 'Sign In'
    }
  },
  {
    path: '/auth/forgotpassword',
    name: 'ForgotPassword',
    component: () => import('../views/ForgotPassword'),
    meta: {
      title: 'Sign In'
    }
  },
  {
    path: '/auth/verify',
    name: 'VerifyEmail',
    component: () => import('../views/Verify'),
    meta: {
      title: 'Verify Email'
    }
  },
  {
    path: '/auth/signup',
    name: 'Signup',
    component: () => import('../views/Signup'),
    meta: {
      title: 'Sign Up'
    }
  },
  {
    path: '/team',
    name: 'Team',
    component: () => import('../views/Team'),
    meta: {
      title: 'Meet the Team'
    }
  },
  {
    path: '/transcripts',
    name: 'Transcripts',
    component: () => import('../views/Files.vue'),
    meta: {
      title: 'Transcripts',
    }
  },
  { path: '*', redirect: '/' }

]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
