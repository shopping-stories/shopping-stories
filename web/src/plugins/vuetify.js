import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);


export default new Vuetify({
    icons: {
        iconfont: 'mdi', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
    },
    theme: {
        themes: {
            light: {
                primary: '#BBDEFB',
                secondary: '#1565C0',
                backgroundColor: '#FFF8E1',
                accent: '#B23850',
                appBarAccent: "#1565C0",
                error: '#b71c1c',
            },
        },
    },
});
