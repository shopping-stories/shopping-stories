// services/http-common.js

import axios from 'axios';

// grab API base url & set header
export const HTTP = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  headers: {
    Authorization: 'Bearer ' + process.env.VUE_APP_API_TOKEN
  }
})
