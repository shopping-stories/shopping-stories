import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import vuetify from './plugins/vuetify';
import { ValidationProvider, extend } from 'vee-validate';
import { required } from 'vee-validate/dist/rules';

extend('required', {
  ...required,
  message: 'This field is required'
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  components: {
    ValidationProvider
  },
  render: h => h(App)
}).$mount('#app')
