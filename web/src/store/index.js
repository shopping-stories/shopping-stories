// store/index.js

import Vue from 'vue'
import Vuex from 'vuex'
import attachCognitoModule from '@vuetify/vuex-cognito-module'
Vue.use(Vuex)

const set = property => (store, payload) => (store[property] = payload)

// initialize values
const store = new Vuex.Store({
  state: {
    username: '',
    email: '',
    isLoading: false,
    isReady: false,
    password: '',
    snackbar: {}
  },
  mutations: {
    setUsername: set('username'),
    setEmail: set('email'),
    setIsReady: set('isReady'),
    setIsLoading: set('isLoading'),
    setPassword: set('password'),
    setUser: set('user'),
    setSnackbar: set('snackbar')
  }
})

attachCognitoModule(store, {
  userPoolId: process.env.VUE_APP_COGNITO_USERPOOLID,
  identityPoolId: process.env.VUE_APP_COGNITO_IDENTITYPOOLID,
  userPoolWebClientId: process.env.VUE_APP_COGNITO_WEBCLIENTID,
  region: process.env.VUE_APP_COGNITO_REGION
}, 'cognito')


store.dispatch('cognito/fetchSession')
  .catch(err => console.log("thisisanerror", err))
  .finally(() => store.commit('setIsReady', true))

export default store
