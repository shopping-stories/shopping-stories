# About Frequent Scripts

These scripts are intended to be used as a copy paste template. The generic names and attributes do not reflect the database its self.
Anything enclosed in `<some_name>` can and should be replaced with relevant information.
