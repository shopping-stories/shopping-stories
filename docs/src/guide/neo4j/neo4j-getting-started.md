# Getting Started 

## Introduction
Neo4j is a native graph database where unlike traditional 
databases, which arrange data in rows, columns and tables, 
Neo4j has a flexible structure defined by stored relationships 
between data records.

With Neo4j, each data record, or node, 
stores direct pointers to all the nodes it’s connected to. 
Because Neo4j is designed around this, it performs queries 
with complex connections more efficiently.  

 Neo4j utilizes Cypher, a graph-optimized query language that understands,
 and takes advantage of these stored connections to perform the same queries 
 other query languages but in an easy to read / write syntax.
 ## Logging in 
  Visit https://shoppingstories.org/ and log on to your account, ensure that 
  you have the role of researcher or any role above to have access to the database. If you
  do not have any of the designated roles, create an account and contact the website 
  admin to assign you the role.Once logged in, press the database instance on the side bar, 
  and it will allow you to
  manipulate the database. 
  
The instance mey require you to log in with a seperate account, if that's the case utilize this log in information:
<br>
<br>
Type in this url in the "connect" box: https://ec2-23-21-85-60.compute-1.amazonaws.com:7687/
<br>
username: \<username\>
<br>
password: \<password\>
  
## Data Documentation
Template | Description
------ | ------
reel | References the microfilm reel # from whence the original image came.
storeName | The name of store or the store location.
ledgerYear | Year where ledger was recorded.
folioPage | Refers to the page number indicated by the primary source 
entryID | Auto generated number that allows the order of the entries found on the original to be recreated.
marginalia | Anything on the page that is not associated with a specific account; like things found in the margins, headers, footers, etc.
prefix | Prefix for account recipient.
accFirstName| First name for the account recipient.
accLastName | Last name for the account recipient.
suffix | Words that are attached to the name, but not the name itself.
profession | Profession for account recipient.
location | Location for where transaction took place.
reference | Any additional information provided as part of the account holder's identification; 
debitCredit (Dr/Cr) | Distinguishes credit or debit for the account.  
originalEntry (OO) | Indicates if this entry was in the original collection.
year | Year of transaction.
month | Month of transaction.
day | Day of transaction.
entry | Broad range of items on a transaction.
entryType | Categorizes the entry with Items, Cash, Totals, Balance to/from, credit transactions.
folioReference | Refers to any additional information provided by the primary source. 
currencyType | Categorizes currency into Tobacco, Sterling or Virginia.
currencyMeasure | Measuring the currency type. ie: pounds (when Tobacco).
currencyParts | Converted XLXsX nomenclature from entry.
challenges | This will be marked as 1 if there was any issues on the files.
comments | Will describe the issues if challenges was set to one.
genMat |  A code to allow us to quickly identify any entries that contain additional people or place information that is NOT the account holder (0: nothing; 1: people and places; 2: places or organizations only

## Important Notes
On transcribing data files to the current template, if there are db files that are broken up to multiple parts, 
they can be inputted into one file. ie: db_C_157C & db_C_157D -> db_C_157.
<br>
<br>
Older data files will have "Owner" or "Store Location" instead of "Store Name", so place "Store Location" 
into the template for the index "storeName".
 
## How to add data into the Database.
1. Ensure the data that will be added in matches the Excel template.  
2. Log into the website and import the data into the parser. 
3. Enter the db instance and use pre-made scripts to create relationships of the data or 
create your own relationships as needed. 
