# Searching

In the event you need to find a specific node we can use a few techniques to find almost any node.

## Labels

The first option we have is to start by searching using node labels.

```cypher
MATCH (n:<LabelName>)
// Returning the nodes with the label name
RETURN n
```

## Attributes

The second option we have is to search for specific attributes.

```cypher
// Finding nodes by label
MATCH (n:<LabelName>)

// Searching for specific attributes
WHERE n.<attribute_name_one> = <value_one>
AND n.<attribute_name_two> = <value_two>
AND EXISTS (n.<attribute_name_three>)

// Returning the nodes with the label name
RETURN n
```

## Relationships

We can use the relationships to search the data as well. In this example we are searching for exclusively for the type of relationship.

```cypher
// Searching for a relationship type
MATCH p = ()-[r:<RELATIONSHIP_NAME>]->()

RETURN p
```

We can extend upon the relationship search buy searching the attributes of that relationship.

```cypher
// Searching for a relationship type
MATCH p = ()-[r:<RELATIONSHIP_NAME>]->()

// Searching the attributes of that relationship
WHERE r.<attribute_name_one> = <value_one>
AND r.<attribute_name_two> = <value_two>

// Returning the relationship
return p
```

## Bringing it all together
This is an example query using all of the various methods listed above.
```cypher
// Matching two specific nodes with a particular relationship
MATCH p = (m:Person)-[r:RELATED]->(n:Person)

// Searching for person m
WHERE n.first_name = 'bell'
AND n.last_name ='thomas'
AND n.age = 26

// Searching for person n
AND n.first_name = 'michael'
AND n.last_name = 'thomas'
AND n.age = 28

// Searching for the relationship r
AND r.status = 'married'

// Returning the relationship
RETURN p
```

## Alternatives

Occasionally there will come a time where you may not be able to find a specific node. In the event that occurs we can leverage NEO4J's `ID()` method which allows us to find a node based on its numerical id.

```cypher
MATCH (n) WHERE ID(n)=<ID_NUM> RETURN n
```
