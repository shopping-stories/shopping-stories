# Deleting

When deleting nodes its important to take great care. Many of the problems you may face can usually be resolved prior to deleting and this should be a last resort.

## Nodes

The following is a generic way to delete all nodes of a specific type.

```cypher
// Matching and deleting all nodes with this label.
MATCH (n:<LabelName>) DELETE n

// Alternative to the above we can search multi label nodes and delete those
// Matching and deleting all nodes with this label.
MATCH (n:<LabelNameOne>:<LabelNameTwo>) DELETE n
```

Deleting nodes based on a specific set of attributes.

```cypher
// Matching and deleting the node containing the specific attributes
MATCH (n:<LabelName>{name:'Peter'}) DELETE n

// Alternative format to deleting nodes with a specific attribute
MATCH (n:<LabelName>)
WHERE n.name = 'Peter'
DELETE n
```

Deleting nodes by id using NEO4J's `ID()` function

```cypher
// Deleting nodes by ID
MATCH (n) WHERE ID(n) = <ID_NUM> DELETE n
```

Deleting nodes by relationship type

```cypher
// Matching the relationship type
MATCH p=(a)-[r:<RELATIONSHIP_NAME>]->(b)

// Removing the relationship and deleting both a and b from the database
DETACH DELETE a,b
```

## Attributes

More often than not the need to delete attributes will come from cleaning the data. If this is the case it is important to be as specific as possible.

**_Important Note_:** When deleting attributes from nodes doesn't remove them from the property key tab. So if the key you just removed is still present in the property key tab this is the reason.

Deleting a specific attribute from all nodes.

```cypher
// Matching any node with these attributes
MATCH (n)

// Specifying the node attributes
WHERE EXISTS n.<attribute_name_one>
AND n.<attribute_name_one> = <some_value>

// Removing the specific attribute
// from all the nodes that match the clause
REMOVE n.<attribute_name_one>

// Returning the effected nodes
RETURN n
```

Matching a specific label and removing the desired attribute

```cypher
// Matching the specific label
MATCH (n:<LabelName>)

// Specifying the attributes to look for
WHERE n.<attribute_name_one> = <value_one>
AND n.<attribute_name_two> = <value_two>

// Removing the attribute
REMOVE n.<attribute_name_one>

// Returning the effected node
RETURN n
```

Matching a specific relationship and deleting the attribute found on that relationship.

```cypher
// Matching the relationship
MATCH p=(n:<LabelName>)-[r:<RELATIONSHIP_NAME>]->(m:<LabelName>)

// Specifying the attributes to look for
WHERE r.<attribute_name_one> = <value_one>

// Removing the attribute
REMOVE r.<attribute_name_two>

// Returning the relation that was effected
RETURN p
```

## Relationships

Deleting specific relationships

```cypher
// Matching the relationship
MATCH (n:<LabelName>)-[r:<RELATIONSHIP_NAME>]->(m:<LabelName>)

// Removing that specific relation from all associated nodes.
DELETE r

// Returning the effected nodes
RETURN n, m
```

Deleting specific relationships with a specific attribute

```cypher
// Matching the relationship
MATCH (n:<LabelName>)-[r:<RELATIONSHIP_NAME>]->(m:<LabelName>)

// Specifying the relationship attributes to find
WHERE r.<attribute_name_one> = <value_one>
AND r.<attribute_name_two> = <value_two>

// Removing that specific relation from all associated nodes.
DELETE r

// Returning the effected nodes
RETURN n, m
```
