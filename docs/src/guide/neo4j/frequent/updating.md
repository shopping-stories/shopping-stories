# Updating

Updating information plays a large roll in modeling data. There are a few methods we can use to update various parts of the database.

## Labels

To remove labels from a node we will use the `REMOVE` function.

```cypher
// The node we are looking at has two labels NodeLabelOne and NodeLabelTwo
MATCH (n {name:'Peter'})

// Removing the desired label
REMOVE n:NodeLabelTwo

// Returning the label names and the node attribute
RETURN n.name, LABELS(n)
```

| n.name  | labels(n)        |
| ------- | ---------------- |
| "Peter" | ["NodeLabelOne"] |

Adding a label to an existing node using the `SET` function.

```cypher
// The node we are looking at has one label, NodeLabelTwo
MATCH (n {name:'Peter'})

// Setting the desired label
SET n:NodeLabelTwo

// Returning the label names and the node attribute
RETURN n.name, LABELS(n)
```

| n.name  | labels(n)                        |
| ------- | -------------------------------- |
| "Peter" | ["NodeLabelOne", "NodeLabelTwo"] |

## Attributes

Updating attributes is a frequent occurrence and there are a few different ways to accomplish this.

```cypher
// Updating a attribute value
MATCH (n:Person{name:'Peter Jones'})

// Updating the name
SET n.name = 'Peter Austin'

// Returning the updated name and label
Return n.name, labels(n)
```

| n.name         | labels(n)  |
| -------------- | ---------- |
| "Peter Austin" | ["Person"] |

**_Important Note_:** The order of operations matters here. First we assign the old name to the new attribute _then_ we remove the old attribute name.

```cypher
// Updating a attribute name
MATCH (n:Person{name:'Peter Jones'})

// Updating the attribute name
SET n.full_name = n.name
REMOVE n.name

// Returning the updated name and label
Return n.name, n.full_name, labels(n)
```

| n.name      | n.full_name   | labels(n)  |
| ----------- | ------------- | ---------- |
| "Undefined" | "Peter Jones" | ["Person"] |

## Relationships

As the data exploration process occurs relationships may need to be updated or changed. This is easily accomplished using the built in NEO4J functions.

```cypher
// Re-naming the relationship
MATCH (a)-[r:<RELATIONSHIP_NAME>]->(b)

// Creating the new relationship
CREATE (a)-[:<NEW_RELATIONSHIP_NAME>]->(b)

// Deleting the old relationship
DELETE r
```
