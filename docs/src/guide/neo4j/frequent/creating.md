# Creating

## Nodes

Creating a node with direct assignment using the `JSON` object convention.

```cypher
CREATE (n:<LabelName>{
    <attribute_name_one>: <valueOne>,
    <attribute_name_two>: <valueTwo>,
    <attribute_name_three>: <valueThree>,
    <attribute_name_four>: <valueFour>
})

// Return the newly created node
RETURN n
```

Creating a node with the `SET` method.

```cypher
CREATE (n:<LabelName>)
SET n.<attribute_name_one> = <valueOne>
SET n.<attribute_name_two> = <valueTwo>
SET n.<attribute_name_three> = <valueThree>
SET n.<attribute_name_four> = <valueFour>

// Return the newly created node
Return n
```

An example of this may be the following

```cypher
CREATE (n:Person)
SET n.first_name = 'thomas'
SET n.last_name = 'smith'
SET n.age = 21

// Return the newly created node
Return n
```

## Attributes

In the event you have a node(s) that you would like to add attributes to the following is a template that can be used.

```cypher
// Finding a node type
MATCH (n:<LabelName>)

// Matching conditions
WHERE n.<attribute_name_one> = <some_value_one>
AND n.<attribute_name_two> = <some_value_two>

// Setting the new attributes
SET n.<new_attribute_one> = <new_attribute_value_one>
SET n.<new_attribute_two> = <new_attribute_value_two>

// Returning the node
RETURN n
```

As a continuation of the previous example we are going to update the newly created node with a profession. This can be accomplished using the following code.

```cypher
// Finding a node type
MATCH (n:Person)

// Matching conditions
WHERE n.first_name = 'thomas'
AND n.last_name = 'smith'
AND n.age = 21

// Setting the new attributes
SET n.profession = 'black smith'

// Returning the node
RETURN n
```

## Relationships

Creating relationships with pre-existing nodes.

**_Important Note_:** when creating relationships, the direction of the arrow determines the nature of the relationship.

```cypher
// Matching two nodes
MATCH (n:<LabelName>), (m:<LabelName>)

// Searching for specific node n
WHERE n.<attribute_name_one> = <some_value_one>
AND n.<attribute_name_two> = <some_value_two>

// Searching for specific node m
AND m.<attribute_name_one> = <some_value_one>
AND m.<attribute_name_two> = <some_value_two>

// Creating the relationship between node n and m
CREATE (n)-[r:<RELATIONSHIP_NAME>]->(m)
```

Creating a relationship with between two people with attributes.

```cypher
// Matching two nodes
MATCH (n:Person), (m:Person)

// Searching for person n
WHERE n.first_name = 'bell'
AND n.last_name ='thomas'
AND n.age = 26

// Searching for person m
AND n.first_name = 'michael'
AND n.last_name = 'thomas'
AND n.age = 28

// Creating the relationship
CREATE (n)-[r:RELATED]->(m)

// Reversing the relationship
CREATE (n)<-[p:RELATED]-(m)

// Setting attributes on the relationships
SET r.status = 'married'
SET p.status = 'married'

// Returning the modified nodes
RETURN m,n
```

Alternatively you can create the same type of relationship using `JSON` notation. By doing this we can inline the relationship attributes and omit the relationship variables.

```cypher
// Matching two nodes
MATCH (n:Person), (m:Person)

// Searching for person n
WHERE n.first_name = 'bell'
AND n.last_name ='thomas'
AND n.age = 26

// Searching for person m
AND n.first_name = 'michael'
AND n.last_name = 'thomas'
AND n.age = 28

// Creating the relationship
CREATE (n)-[:RELATED{status:'married'}]->(m)

// Reversing the relationship
CREATE (n)<-[:RELATED{status:'married'}]-(m)

// Returning the modified nodes
RETURN m,n
```

## Labels

All nodes are required to have a label but there isn't a limit on how many a single node can have.
**_Important note_:** Each node label need to be separated by `:` for each label.

```cypher
// Creating a node with one label
CREATE (n:<LabelName>)

// Creating a node with more than one label
CREATE (n:<LabelNameOne>:<LabelNameTwo>)
CREATE (n:<LabelNameOne>:<LabelNameTwo>:<LabelNameThree>)
```
