# About

This page contains queries that answer major questions put forth by Molly.

## How much sugar was sold?

```cypher
// How much sugar was sold
MATCH (t:Transaction)-[:CONTAINS]->(e:Entry{item:"sugar"})
RETURN COUNT(e)
```

---

## What were the different types of sugar?

```cypher
// What were the different types of sugar
MATCH (t:Transaction)-[:CONTAINS]->(e:Entry{item:"sugar"})
RETURN COUNT(e), e.descriptor
```

| count(n) | descriptor        |
| -------- | ----------------- |
| 3        | "single, refined" |
| 126      | null              |
| 11       | "brown            |

---

## How much 'Turlington's Balsam of Life' was sold?

```cypher
// How much 'Turlington's Balsam of Life' was sold
MATCH (t:Transaction)-[:CONTAINS]->(e:Entry)
WHERE e.item contains "turlington"

RETURN COUNT(e), e.descriptor
```

---

## Which customer bought the most rum?

```cypher
MATCH (p:Person)-[:HAS_RECEIPT]->(t:Transaction)-[:CONTAINS]->(e:Entry{item:'rum'})

// Returning the table containing the following information
RETURN p.first_name, p.last_name, COUNT(e) as total_bought , e.descriptor

// Ordering the table by descending order.
ORDER BY total_bought DESC
```

---

## Did a woman ever by snuff or a snuff box?

```cypher
MATCH (p:Person{gender:'female'})-[:HAS_RECEIPT]->(t:Transaction)-[:CONTAINS]->(e:Entry)
// Looking for the word snuff
WHERE e.entry CONTAINS 'snuff'
OR e.item CONTAINS 'snuff'

// Returns a true or false answer
RETURN COUNT(e) > 0 as Purchase
```

| Return |
| ------ |
| FALSE  |

---

### How many captains were customers?

```cypher
// Matching the type of person
MATCH (p:Person)

// Conditions we are looking for
WHERE EXISTS(p.profession) 
AND p.profession CONTAINS 'captain'

// The realationship structure
MATCH a = (p)-[:HAS_RECEIPT]->(t:Transaction)-[:OCCURRED_AT]->(place:Place)

// Returning the relationship.
RETURN a
```