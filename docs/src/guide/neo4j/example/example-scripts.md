# Creating relationships

## Creating relationships between transactions and entries

**_Important Note_:** This query only creates new relations on nodes that do not already have and existing relationship with the node being connected.

- This query will occasionally create two of the same items on the same transaction. Manual review will be required to clarify the relationship.

```cypher
// Matching transactions and entries
MATCH (t:Transaction), (e:Entry)

// Matching all the transactions to their entries
WHERE t.reel_id = e.reel_id
AND t.folio_page = e.folio_page
AND t.entry_id = e.entry_id

// Making the relationship
MERGE (t)-[:CONTAINS]->(e)

// Returning the relationship
RETURN t, e
```

---

## Creating relationships between people and their transactions.

Creates connections between people and their transactions.

**_Important Note_:** This query only creates new relations on nodes that do not already have an existing relationship with the node that is being connected.

- This will occasionally make a relationship between two people to none transaction. This occurs because those two people have the same `first_name` and `last_name` name. Manual review will be required.

```cypher
// Matching people to transactions
MATCH (p:Person), (t:Transaction)

// Matching people to transactions
WHERE p.first_name = t.first_name
AND p.last_name = t.last_name

// Creating the relationships
MERGE (p)-[:HAS_RECEIPT]->(t)

// Returning the created relationships
RETURN p, t
```

---

## Searching for transactions that have more than one relationship

Finds and displays all the relationships that exist where there is more than one match to a single transaction.

```cypher
// Matching person1 to a transaction and any other match to that same transaction
MATCH p= (p1:Person)-[:HAS_RECEIPT]->(t:Transaction)<-[:HAS_RECEIPT]-(p2:Person)

// Returning the transactions that have more than one receipt holder.
RETURN p Limit 50
```

---

## Creating New locations from organizations location data.

This script creates new nodes from organizations location data.
**_Important Note_:** This will only create new nodes if a node doesn't already exist for that instance.

```cypher
Match (org:Organization)
WHERE EXISTS(org.location)

// Creating new nodes if they don't already exist
MERGE (l:Location{name:org.location})

// Returning the newly created nodes
Return l
```

---

## Creating New locations from places location data.

This script creates new nodes from places location data.
**_Important Note_:** This will only create new nodes if a node doesn't already exist for that instance.

```cypher
Match (p:Place)
WHERE EXISTS(p.location)

// Creating new nodes if they don't already exist
MERGE (l:Location{name:p.location})

// Returning the newly created nodes
Return l
```

---

## Creating relationship between a place and a location

This script creates a relationship between places and locations and will only create new relations if there doesn't exist an relationship already.

```cypher
// Matching Nodes
MATCH (l:Location)
MATCH (p:Place)

// Matching the place to the location node
WHERE p.location = l.name

// Creating a singleton relationship to a location
MERGE (p)-[:FOUND_IN]->(l)

// Returning the newly created relationship
RETURN p, l
```

---

## Creating relationship between a organization and a location

This script creates a relationship between organizations and locations and will only create new relations if there doesn't exist an relationship already.

```cypher
// Matching Nodes
MATCH (l:Location)
MATCH (org:Organization)

// Matching the organization to the location node
WHERE org.location = l.name

// Creating a singleton relationship to a location
MERGE (org)-[:FOUND_IN]->(l)

// Returning the newly created relationship
RETURN org, l
```
---

## Creating relationship between a person and a location

This script creates a relationship between people and locations and will only create new relations if there doesn't exist an relationship already.

```cypher
// Matching Nodes
MATCH (l:Location)
MATCH (p:Person)
// Matching the person to the location node
WHERE p.location = l.name

// Creating a singleton relationship to a location
MERGE (p)-[:FOUND_IN]->(l)

// Returning the newly created relationship
RETURN p, l
```
---

## Creating relationship between individual transactions and place
This script creates a relationship between each individual transaction and the store they occurred at.
```cypher
// Matching specific nodes
MATCH (t:Transaction{store_name:'colchester'})
MATCH (p:Place{name:'colchester store'})

// Creating singleton relationships
MERGE (t)-[:OCCURRED_AT]->(p)

// Returning the newly created relationship
RETURN t, p
```

---
## Creating relationship between an individual and the place they shop at
This script creates a shorthand relationship that represents a longer query used to find the customers of a specific store.
```cypher
// The relationship structure
MATCH a = (p)-[:HAS_RECEIPT]->(t:Transaction)-[:OCCURRED_AT]->(place:Place)
RETURN p, place
```
The below query creates a shorthand relationship of the above query with the relationship of customers.
```cypher
// Matching the type of person
MATCH (p:Person)

// The relationship structure
MATCH a = (p)-[:HAS_RECEIPT]->(t:Transaction)-[:OCCURRED_AT]->(place:Place)

// Creating the new relationship
MERGE (p)-[:CUSTOMER_OF]->(place)

// Returning the relationship.
return p, place
```