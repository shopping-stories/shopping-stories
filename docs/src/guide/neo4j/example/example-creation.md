# Example Creation

## Creating new `entry` from challenge `entry`
These scripts create new nodes from `challenge` entry nodes. There are two ways of tacking this process that are dependent on what you encounter.

```cypher


```

## Creating New locations from `place` `location` data.

This script creates new nodes from places location data.
**_Important Note_:** This will only create new nodes if a node doesn't already exist for that instance.

```cypher
Match (p:Place)
WHERE EXISTS(p.location)

// Creating new nodes if they don't already exist
MERGE (l:Location{name:p.location})

// Returning the newly created nodes
Return l
```

## Creating new locations from `organization` `location` data

This script creates new nodes from organizations location data.
**_Important Note_:** This will only create new nodes if a node doesn't already exist for that instance.

```cypher
Match (org:Organization)
WHERE EXISTS(org.location)

// Creating new nodes if they don't already exist
MERGE (l:Location{name:org.location})

// Returning the newly created nodes
Return l
```

## Creating new locations from `person` `location` data

This script creates new nodes from places location data.
**_Important Note_:** This will only create new nodes if a node doesn't already exist for that instance.

```cypher
Match (p:Place)
WHERE EXISTS(p.location)

// Creating new nodes if they don't already exist
MERGE (l:Location{name:p.location})

// Returning the newly created nodes
Return l
```