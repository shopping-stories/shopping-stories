# Example Relationships

## Creating relationships between `transaction` and `entry`

**_Important Note_:** This query only creates new relations on nodes that do not already have and existing relationship with the node being connected.

- This query will occasionally create two of the same items on the same transaction. Manual review will be required to clarify the relationship.

```cypher
// Matching transactions and entries
MATCH (t:Transaction), (e:Entry)

// Matching all the transactions to their entries
WHERE t.reel_id = e.reel_id
AND t.folio_page = e.folio_page
AND t.entry_id = e.entry_id

// Making the relationship
MERGE (t)-[:CONTAINS]->(e)

// Returning the relationship
RETURN t, e
```

## Creating relationships between `person` and `transaction`

Creates connections between people and their transactions.

**_Important Note_:** This query only creates new relations on nodes that do not already have an existing relationship with the node that is being connected.

- This will occasionally make a relationship between two people to none transaction. This occurs because those two people have the same `first_name` and `last_name` name. Manual review will be required.

```cypher
// Matching people to transactions
MATCH (p:Person), (t:Transaction)

// Matching people to transactions
WHERE p.first_name = t.first_name
AND p.last_name = t.last_name

// Creating the relationships
MERGE (p)-[:HAS_RECEIPT]->(t)

// Returning the created relationships
RETURN p, t
```

## Creating relationship between `place` and a `location`

This script creates a relationship between places and locations and will only create new relations if there doesn't exist an relationship already.

```cypher
// Matching Nodes
MATCH (l:Location)
MATCH (p:Place)

// Matching the place to the location node
WHERE p.location = l.name

// Creating a singleton relationship to a location
MERGE (p)-[:FOUND_IN]->(l)

// Returning the newly created relationship
RETURN p, l
```

## Creating relationship between `organization` and `location`

This script creates a relationship between organizations and locations and will only create new relations if there doesn't exist an relationship already.

```cypher
// Matching Nodes
MATCH (l:Location)
MATCH (org:Organization)

// Matching the organization to the location node
WHERE org.location = l.name

// Creating a singleton relationship to a location
MERGE (org)-[:FOUND_IN]->(l)

// Returning the newly created relationship
RETURN org, l
```

## Creating relationship between `person` and `location`

This script creates a relationship between people and locations and will only create new relations if there doesn't exist an relationship already.

```cypher
// Matching Nodes
MATCH (l:Location)
MATCH (p:Person)
// Matching the person to the location node
WHERE p.location = l.name

// Creating a singleton relationship to a location
MERGE (p)-[:FOUND_IN]->(l)

// Returning the newly created relationship
RETURN p, l
```

## Creating relationship between `transactions` and `place`

This script creates a relationship between each individual transaction and the store they occurred at.

```cypher
// Matching specific nodes
MATCH (t:Transaction{store_name:'colchester'})
MATCH (p:Place{name:'colchester store'})

// Creating singleton relationships
MERGE (t)-[:OCCURRED_AT]->(p)

// Returning the newly created relationship
RETURN t, p
```

## Creating relationship between `person` and `place`

This script creates a shorthand relationship that represents a longer query used to find the customers of a specific store.

```cypher
// The relationship structure
MATCH a = (p)-[:HAS_RECEIPT]->(t:Transaction)-[:OCCURRED_AT]->(place:Place)
RETURN p, place
```

The below query creates a shorthand relationship of the above query with the relationship of customers.

```cypher
// Matching the type of person
MATCH (p:Person)

// The relationship structure
MATCH a = (p)-[:HAS_RECEIPT]->(t:Transaction)-[:OCCURRED_AT]->(place:Place)

// Creating the new relationship
MERGE (p)-[:CUSTOMER_OF]->(place)

// Returning the relationship.
return p, place
```
