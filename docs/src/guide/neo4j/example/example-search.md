# Example Search 

## Find the place where items were bought at and who bought them.
This script finds all the purchases that john barry made containing sugar and the store the occurred at.
**_Important Note_:** Make sure that the option connect result nodes is unchecked.
```cypher 
// Matching the relationship
MATCH PathToItem =  (p:Person{first_name:'john',last_name:'barry'})-[:HAS_RECEIPT]->(t:Transaction)-[:CONTAINS]->(e:Entry{item:'sugar'})
MATCH PathToStore = (p)-[:CUSTOMER_OF]->(store:Place)

// Excluding original_entry and challenges
WHERE NOT EXISTS(t.original_entry)
AND NOT EXISTS(t.challenges)

// Returning the relationship paths
Return PathToItem, PathToStore LIMIT 20
```

## Find a place where items were bought and excluding specific people
```cypher
// Matching the relationship
MATCH PathToItem =  (p:Person)-[:HAS_RECEIPT]->(t:Transaction)-[:CONTAINS]->(e:Entry{item:'sugar'})
MATCH PathToStore = (p)-[:CUSTOMER_OF]->(store:Place)

// Excluding original_entry and challenges
WHERE NOT EXISTS(t.original_entry)
AND NOT EXISTS(t.challenges)

// Not including specific people
AND NOT p.last_name = 'lane' 
AND NOT p.last_name='boggiss'
AND NOT p.last_name='halley'

// Returning the relationship paths
Return PathToItem, PathToStore LIMIT 20
```