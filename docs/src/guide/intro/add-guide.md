# How to Use VuePress

This documentation was created with Vuepress (see [here](https://vuepress.vuejs.org/)). This guide can be seen as a 'living document' in the sense that it was meant to be continually updated as more features are added to the project.

Here is a quick review on how to add to this documentation.

## Creating a VuePress Project

This VuePress project currently lives in the Shopping Stories repository (see [here](https://gitlab.com/shopping-stories/shopping-stories)). It can be found in `/docs`.

To create a new VuePress project, you can simply follow this handy [guide](https://vuepress.vuejs.org/guide/getting-started.html).

## Adding Documentation

You can add to this guide by creating a new Markdown `.md` file in a text editor. Here is an example:

```md
// some-file.md

# H1

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a neque ultrices, semper erat sed, malesuada sem.

## H2

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a neque ultrices, semper erat sed, malesuada sem.

### H3 (does not appear on sidebar)

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a neque ultrices, semper erat sed, malesuada sem.

Add [hyperlink](wwww.url.com)

Add `text`

etc...
```

::: tip Note
You can find more information about how you can show content using Markdown with this [cheatsheet](https://vuepress.vuejs.org/guide/markdown.html#header-anchors))
:::

Save the new Markdown file in the `/docs/src/guide` directory. Make sure to also add in the path in the `config.js` file, found at `docs/src/.vuepress`. Here is an example of adding the `some-file.md` file to the sidebar of the VuePress application:

```js
// config.js
~~~~~~~~~~~~~~~~~~~

themeConfig: {
  repo: '',
  editLinks: false,
  docsDir: '',
  editLinkText: '',
  lastUpdated: false,
  nav: [
    {
      text: 'Guide',
      link: '/guide/',
    }
  ],
  sidebar: {
    '/guide/': [
      {
        title: 'Introduction',
        collapsable: true,
        children: [
          'some-file',
        ]
      }
    }
  }

~~~~~~~~~~~~~~~~~~~
```

## Running VuePress

To view your VuePress Project on your local computer, make sure you are in the `/docs` directory with the `package.json` file. Type the following commands:

```sh
$ npm install
$ npm run dev
```

This will start up the project locally at the specified port (8080 by default). Simply type `localhost:8080` in your browser of choice to view the VuePress Application.

## Configuring VuePress

To learn more about all the neat things you can do with this VuePress guide, make sure to check out the official [documentation](https://vuepress.vuejs.org/).
