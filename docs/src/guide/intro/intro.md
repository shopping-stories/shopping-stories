# Introduction

This guide was created as a way to document the Shopping Stories project. (insert photo)

The project was initially proposed by Molly Kerr, founding director of History Revealed, Inc., to the UCF Senior Design I course in Summer 2020. The original idea was to create a centralized database solution for Molly's data. The original team, Group 7, comprised of the following folks:

```
Cameron Garretson,
Randy Glasgow,
Joshua Romero,
Matthew Stone,
& Nick Plympton
```
