# How To Use This Guide

This guide was designed around the idea that another team will extend the work already done by Group 7. The goal was to create something intuitive for future teams to understand. This guide is meant to be exhaustive without being cumbersome. Which is to say that we do our best here to help you understand what you are looking at without spelling it out line by line.

The best approach to using this guide effectively is to first understand the components that make up this project.

### Neo4j

The foundation of the project is designed around the Neo4j Graph Database. You can read more about it on Neo4's [website](https://neo4j.com/neo4j-graph-database/).

We enrolled in the Neo4j [Startup Program](https://neo4j.com/startup-program/), which must be renewed annually. This program allows us to utilize Neo4j Enterprise and everything that comes with it, with the exception of customer support.

The reason we decided to work with Neo4j's Graph Database was due to the amount of data and the way in which we could manipulate it. We turned every entry in the ledgers into nodes and built relationships that connected them. This, we hope, will allow Molly to better traverse her datasets and discover new insights from the data.

### The Stack

The website is composed of Express.js, Vue.js, and Node.js. Combined with the Neo4j database, we call it the 'NEVN' stack. We also utilized Amazon Cognito for the user login/register portion of the project.

This guide broke the website into two parts: Front-end and Back-end. In the Front-end section, we break down the website and it's components, as well as the overall layout. In the Back-end section, we review the API and the other miscellaneous guts that make the Front-end work.

### AWS

The project's website and database are both hosted on Amazon Web Services, particularly implemented on an EC2 instance running Ubuntu. The AWS section of this guide will review the setup process, AWS management, and a host of other useful tidbits related to the EC2 instance.
