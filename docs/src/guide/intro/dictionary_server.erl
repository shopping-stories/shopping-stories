-module(dictionary_server).
-export([start/0, stop/0, insert/2, remove/1, lookup/1, clear/0, size/0, dictionary/1]).

dictionary(Book) ->
  receive
    stop -> ok;
    {in, Key, Value} ->
      dictionary(lists:keystore(Key, 1, Book, {Key,Value}));
    {rm, Key} ->
      dictionary(lists:keydelete(Key, 1, Book));
    {lu, Key} ->
      io:format("Key: ~w, Value: ~w~n", [Key, element(2,element(2,lists:keysearch(Key, 1, Book)))]),
      dictionary(Book);
    clear -> dictionary([]);
    size ->
      io:format("The size of the dictionary is: ~w.~n", [length(Book)]),
      dictionary(Book)
  end.



start() ->
  Existence = lists:member(dict, registered()),
  if
    not Existence ->
      register(dict, spawn(dictionary_server, dictionary, [[]])),
      ok;
    true -> {error, dictionary_already_exists}
  end.





stop() ->
  Existence = lists:member(dict, registered()),
  if
    Existence ->
      dict ! stop,
      ok;
    true -> {error, dictionary_doesnt_exist}
  end.





insert(Key, Value) ->
  Existence = lists:member(dict, registered()),
  if
    Existence ->
      dict ! {in, Key, Value},
      ok;
    true -> {error, dictionary_doesnt_exist}
  end.





remove(Key) ->
  Existence = lists:member(dict, registered()),
  if
    Existence ->
      dict ! {rm, Key},
      ok;
    true -> {error, dictionary_doesnt_exist}
  end.





lookup(Key) ->
  Existence = lists:member(dict, registered()),
  if
    Existence ->
      dict ! {lu, Key},
      ok;
    true -> {error, dictionary_doesnt_exist}
  end.





clear() ->
  Existence = lists:member(dict, registered()),
  if
    Existence ->
      dict ! clear,
      ok;
    true -> {error, dictionary_doesnt_exist}
  end.





size() ->
  Existence = lists:member(dict, registered()),
  if
    Existence ->
      dict ! size,
      ok;
    true -> {error, dictionary_doesnt_exist}
  end.
