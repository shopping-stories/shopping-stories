# Updating the Server

Updates should be ideally performed weekly, but no less frequently than once a month. Updates help to patch security flaws; generally constitute a server reboot, which is good for the health of the server and its services.

*Ensure you reboot the server after you install updates.*

Updating packages
---
To start the update process, copy+paste the following command into the terminal:
```sh
sudo apt-get update && sudo apt-get dist-upgrade
```
If you wish to see what will be updated prior to initiated the upgrade process, you can split the commands up.
![update_cmd](./images/update/update-login.png)

During an upgrade, certain configuration files may temporarily halt the upgrade process as the contents of the new version may differ from the currently installed one.  

*In nearly all of these cases, you* **DO NOT** *want to overwrite the existing configuration*, as the existing configuration files have changes that differ from the default. Simply entering "y" for all queries may break some of the programs installed on the server.

You can inspect the differences by responding with "d"; if you notice any configuration options that are different, it is probably wise to reject the new file.
![details](./images/update/update-journal.png)  

When prompted, reject the new file (most commonly done by typing "n" and pressing the enter key).
![details](./images/update/update-neo4j-conf.png)  

When prompted to remove obsolete packages, respond with "y". This helps to reduce the bloat of unused files on the server and preserve storage capacity.
![cleanup](./images/update/update-obsolete.png)

Updating Ubuntu
---
![ubuntu](./images/update/update-ubuntu.png)  
When you SSH in and see a message similar to this, it's time to update Ubuntu. Copy+paste the following command into the terminal:
```sh
sudo do-release upgrade
```

---
## Strange/Unexpected Occurrences Resulting From Updates

Should the update process catastrophically fail or services break in a manner that leaves them unrecoverable, you should [restore the server](../aws/backup.md) from the latest backup.
