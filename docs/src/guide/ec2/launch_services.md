# Launching Services
*If you're visiting this page after doing a backup restoration, note that all of these services should auto-start.*   
*If they fail to start for whatever reason, proceed to* [Manual Launching the Website](#Manually-Launching-the-Website).
## Automatic Startups

All of the system's services are launched at server startup via Cron.

To view the crontab, enter the following command in any directory:  
```sh
crontab -e
```
The current startup script is as follows:  
```sh
@reboot sh -c 'sudo journalctl --vacuum-size=50M && cd /home/ubuntu/shopping-stories/web && sudo pm2 start ecosystem.config.js && cd ../server && sudo pm2 start ecosystem.config.js && sudo neo4j start'
```
### What's going on in that script?
  The first command ```sudo journalctl --vacuum-size=50M``` is cleaning up old system journal entries. These are a pretty major source of system bloat and accumulate at about 1.5GB/month without any intervention.  
	The following command(s) ```cd /home/ubuntu/shopping-stories/web && sudo pm2 start ecosystem.config.js && cd ../server && sudo pm2 start ecosystem.config.js``` launch the website and its supporting proxy server.  
Finally, ```sudo neo4j start``` begins a new instance of the Neo4j database. This needs to run so that the proxy server can route all subdomain calls (browser.shoppingstories.org) to the proper port internally while still keeping the encrypted HTTPS SSL/TLS channel.



## Manually Launching the Website

```
Cam knows what's up
```

## Booting Neo4j
Neo4j should automatically start on system boot. The guide for setting this up can be [found here](https://neo4j.com/developer/kb/how-to-setup-neo4j-to-startup-on-linux-server-reboot/).  
The file/script which launches Neo4j on startup is as follows:
![init_d](./images/launch/neo4j.png)


If you need to manually launch Neo4j, enter the following command:

```sh
sudo neo4j start
```
