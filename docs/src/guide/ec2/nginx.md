# NGINX Proxy Server

The NGINX Proxy Server exists in the server and waits for requests to ports 80 and 443. Depending on the requests, it will then direct the client to the proper internal channels where the website functionality exists. This allows all of the functionality to exist under secured HTTPS TLS/SSL encryption.

To learn more about NGINX, click [here](https://nginx.org/en/).

---

To edit the proxy server in relation to `shoppingstories.org` and any subdomains, make sure to log into the EC2 server. Then type the following command:

```bash
$ sudo nano /etc/nginx/conf.d/shoppingstories.conf
```

::: danger
Any changes to nginx requires a restart!
```bash
$ sudo systemctl restart nginx
```
:::

Here is `/etc/nginx/conf.d/shoppingstories.conf` in its entirety:

```conf
server {
    listen 443 ssl;

    server_name api.shoppingstories.org;

    ssl_certificate /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/shoppingstories.org-0001/privkey.pem;

    location / {
        proxy_pass http://localhost:8443;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
     }
}

server {
    listen 80;

    server_name www.shoppingstories.org shoppingstories.org;

    return 301 https://$host$request_uri;
}
server {
    listen 80;

    server_name browser.shoppingstories.org

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;

    server_name www.shoppingstories.org shoppingstories.org;

    ssl_certificate /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/shoppingstories.org-0001/privkey.pem;

    location / {
        proxy_pass http://localhost:8080;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}

server {
  listen 443 ssl;

  server_name browser.shoppingstories.org;

  ssl_certificate /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/shoppingstories.org-0001/privkey.pem;

  location / {
      proxy_pass https://localhost:7473/browser/;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection 'upgrade';
      proxy_set_header Host $host;
      proxy_cache_bypass $http_upgrade;
   }
}
```

## api.shoppingstories.org

The proxy server monitors all requests for the API endpoints on port 443, which is the secure port for HTTPS connections. All HTTP requests will be directed to a standard NGINX 404 webpage.

![HTTP Example](./images/nginx/nginx-http.png)

The `.conf` file will monitor the 443 port and use the symlink certificate `.pem` files that were created by certbot to verify the HTTPS certificate. It then redirects all activity to the internal port where the Express App is running.

```conf
server {
    listen 443 ssl;

    server_name api.shoppingstories.org;

    ssl_certificate /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/shoppingstories.org-0001/privkey.pem;

    location / {
        proxy_pass http://localhost:8443;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
     }
}
```

## shoppingstories.org

The proxy server will monitor all HTTP requests for `shoppingstories.org` and `www.shoppingstories.org` at the standard port 80. It will then redirect these requests to the encrypted channel, `https://shoppingstories.org` and `https://www.shoppingstories.org` respectfully.


```conf
server {
    listen 80;

    server_name www.shoppingstories.org shoppingstories.org;

    return 301 https://$host$request_uri;
}
```

Similarly to `api.shoppingstories.org`, the proxy server will take all HTTPS requests and redirect the traffic to the internal port where the Vue App is hosted. It again uses the certificates produced by certbot.

```conf
server {
    listen 443 ssl;

    server_name www.shoppingstories.org shoppingstories.org;

    ssl_certificate /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/shoppingstories.org-0001/privkey.pem;

    location / {
        proxy_pass http://localhost:8080;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```

## browser.shoppingstories.org

The proxy server will monitor any HTTP requests for `browser.shoppingstories.org` and redirect that traffic to the encrypted HTTPS channel.

```conf
server {
    listen 80;

    server_name browser.shoppingstories.org

    return 301 https://$host$request_uri;
}
```

An HTTPS request will result in the proxy server talking to the internal port where the Neo4j browser is currently running on the EC2 machine. All communication will be encrypted in HTTPS for the client.

```conf
server {
  listen 443 ssl;

  server_name browser.shoppingstories.org;

  ssl_certificate /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/shoppingstories.org-0001/privkey.pem;

  location / {
      proxy_pass https://localhost:7473/browser/;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection 'upgrade';
      proxy_set_header Host $host;
      proxy_cache_bypass $http_upgrade;
   }
}
```
