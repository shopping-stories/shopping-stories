# Connecting to Backend/Server (SSH)

In order to perform administrative duties effectively on the EC2 instance, you need to connect to the server via a Linux/Mac machine. Trying to connect via the Windows Command Line can have adverse effects.  
*If you already have a Linux/Mac machine available, skip to [Key File Installation](#key-file-installation)*

**Windows users** will need to setup a Ubuntu shell, which can be done in just a few minutes by following [this guide (Steps 1-7).](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

You do not need to complete **'Install Windows Terminal (optional)'** or anything beyond it.  

#### When you first launch the Ubuntu shell, you'll need to create a local user account.  
**user** is a valid username, but make it whatever you'd like.  
*Keep in mind you'll be typing whatever password you choose every time you connect to the instance.*
![create_user](./images/connect/1st-launch.png)

#### Optionally, you can change the colors of the terminal by right-clicking the top of the window and selecting "Properties".
![pretty_colors](./images/connect/terminal-colors.png)  


## Key File Installation
* **Linux/Mac** users will need to place the following key file somewhere safe and reference its file_path when performing the ssh command.  
* **Windows** users can create this file in the default directory.

### 1. Begin by opening your terminal and typing the following command:  
where ***key_file_name*** can be any name of your choosing. You will type this file name when connecting to the server.
```sh
nano key_file_name.pem
```
![init_key_file](./images/connect/initialize-keyfile.png)

### 2. You will then need to copy + paste in the private key string. (Simply right-click to paste inside the terminal)  
Consult Molly for this information, as it is protected and cannot be posted here.  

![fill_key_file](./images/connect/fill-keyfile.png)

Press **"Ctrl+O -> Enter"** to save the file.  

Your key to the instance has successfully been installed!

## How to SSH into the instance

### 1. Open your terminal if you haven't already.  
### 2. Type the following command in, where key_file_name is the name you gave your key file and ELASTIC_IP is the IP address of the server.
```sh
sudo ssh -i key_file_name.pem ubuntu@ELASTIC_IP
```  

The first time you connect to the instance, you'll need to confirm the fingerprint... *Simply type 'yes' and press enter.*
![confirm_key](./images/connect/1st-connect.png)

### 3. You should now be connected to the instance! Congratulations!
*You should be able to press the 'up arrow' in subsequent launches of the terminal in order to recover this command, so you don't need to retype it every time you want to SSH in.*
![confirm_key](./images/connect/connected.png)


## After a Instance Restoration

You may notice this message when attempting to ssh into the server after a instance restoration.  

![confirm_key](./images/connect/post-restore.png)

This is normal - simply run the command it issues you with **sudo** to reset the keyfile.
```sh
sudo ssh-keygen -f "/root/.ssh/known_hosts" -R "ELASTIC_IP"
```
