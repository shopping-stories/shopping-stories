# Helpful Linux Commands

Perhaps most important of all is the manual command:

```bash
$ man
```

Learn more about the `man` command [here](https://www.computerhope.com/unix/uman.htm).

::: tip
Most commands on the EC2 server require root level access. Instead of becoming root, aka 'super user':
```bash
$ su -
```
... it is more secure to use sudo instead:
```bash
$ sudo git pull
```
:::

---

## Creation

Create a directory: *ignore <>*
```bash
$ mkdir <new-directory-name>
```

Create a new file: *ignore <>*
```bash
$ touch <filename>
```

Direct all output to a text file: *ignore <>*
```bash
$ <process> >> <filename>
```
Example with tree command:

```bash
$ tree >> output.txt
```

## Copying and Moving Files

Copy files: *ignore <>*
```bash
$ cp <filename> <newfilename>
```

Example of copying a file to a directory:
```bash
$ cp file.txt somedirectory/newfile.txt
```

Moving a file to elsewhere
```bash
$ mv file.txt somedirectory
```

## Deletion
removing a file: *ignore <>*
```sh
$ rm <filename> <optional_additional_file> <optional_additional_file>
```
removing an empty directory: *ignore <>*
```sh
$ rm -r <directory>
```  
or  
```sh
$ rmdir <directory>
```
removing all files and subdirectories: *ignore <>*
```sh
$ rm -rf <directory>
```  


## Navigation
Show current directory's subdirectories and files:
```sh
$ ls
```

Show current directory, including hidden files:
```bash
$ ls -a
```

### Change directory
Go to subdirectory: *ignore <>*
```sh
$ cd <directoryname>
```

Back out one directory level
```sh
$ cd ..
```

Back out two directories
```sh
$ cd ../..
```
et cetera...  

You can string these directory commands together. For instance, if you want to back out two directories and into another, you can do this:
```sh
$ cd ../../target/directories
```

## File/Directory Information
Show subdirectory & file size: *recommend running this as root*
```sh
$ du -hs * | sort -rh | head -50
```

## Transfer Local File to Server
```sh
$ scp -i keyname.pem filename.ext ubuntu@serverip:/destination/path/etc/etc
```

## Port commands
Listens to traffic on #PortNum:
```sh
$ ls | nc -l -p #PortNum
```  

List all active ports:
```sh
$ ss -lntu
```
OR  
```sh
$ sudo netstat -tulpn | grep LISTEN
```

OR

```bash
$ sudo netstat -plnt
```

To stop any processes by PID:

```bash
$ kill <PID>
```

Open port to outbound traffic on #PortNum (must use sudo or be root)
```sh
$ ufw allow #PortNum
```  

## System Restarts

It is sometimes necessary to restart processes, such as Neo4j and NGINX. Anytime you edit the configuration files of either, it is required to restart these processes.

For NGINX:

```bash
$ sudo systemctl restart NGINX
```

For Neo4j
```bash
$ sudo neo4j start
```
