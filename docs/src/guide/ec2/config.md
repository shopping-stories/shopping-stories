# Configuration Files

There are a handful of notable configuration files of interest for Shopping Stories.  

Any changes to these files should be commented so it is easy to both reverse changes if need be and to see what is no longer a default setting.

## Neo4j
#### neo4j.conf & neo4j.template  
These are the primary configuration files for Neo4j. In order for a change to be persistent, you must edit the .template file. And changes made to the template file will subsequently change the .conf file.  

**TL;DR:** *just edit the .template file.*  

***It would behoove you*** to follow the convention found below when you make any changes to this file, so that you may more easily track and restore these changes should you need to.  

![neo4j.template](./images/config/neo4j-template.png)  

## Website
#### package.json  
![package.json](./images/config/package-json.png)  
This file contains parameters for setting the port and domain name of the website. Port 443 indicates HTTPS.

#### vue.config.js
![vue.config.js](./images/config/vue-config-js.png)  
This file includes some configurations for the website as well as the location of the SSL certificates.
