# Storage Maintenance

Over time, various logs within the system can slowly start to add up or *bloat* the storage. This page documents some of the largest data-hoarding parts of the system.

Every time you SSH into the server, you should get a report of the current disk usage. If you notice this creep up over time, you should consider proceeding with some cleanup.  
  
![disk usage](./images/maintenance/sign-in.png)

A quick one-stop command to free up some space. ***NOTE:*** **The server will restart**:
```sh
sudo apt-get clean && sudo apt-get autoremove --purge && sudo reboot
```

## System Journal  
*The system journal is set to clean itself up whenever the server reboots. Refer to [Launching Services](./launch_services) for more information on this.*

Check the size of journal entries with:
```sh
journalctl --disk-usage
```  

The journal entries are stored in the following directory:
```/var/log/journal/406105618d684a858d074aeced845a27```  

Configuring the parameters of the journal is managed through the file:
```/etc/systemd/journald.conf```

## Neo4j logs

Neo4j logs virtually every query and call made to the database. These logs can be found at ```/var/log/neo4j```  
The size of these files is negligible, considering the storage capacity of the server. If storage space is critically low, or these logs grow too large, consider deleting the oldest logs. Instructions for how to remove files can be [found here.](./linuxcmds.md#deletion)
![Neo4j_logs](./images/maintenance/neo4j-logs.png)


## APT Cache

Ubuntu holds onto all of the old installers in case you need them again. It's generally safe to clean these up.  

To see how large the cache is, run:
```sh
du -sh /var/cache/apt/archives
```

To clean that cache out, run:
```sh
sudo apt-get clean
```

## Old Kernels
These are less likely to plague the system, but after a significant amount of time and several update cycles, old kernels can be found hidden under the couch.  

To clean these:
```sh
sudo apt-get autoremove --purge
```
