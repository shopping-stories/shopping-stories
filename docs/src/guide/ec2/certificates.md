
# Relevant Certificates

All of the certificates for the server should automatically update themselves when the time comes.

A tree for all of the relevant certificates of the server is shown below.

You can also pull this tree up with the command:
```sh
tree /var/lib/neo4j/certificates
```

```sh
/var/lib/neo4j/certificates
├── bak
│   ├── bolt
│   │   ├── private.key
│   │   ├── public.crt
│   │   ├── revoked
│   │   └── trusted
│   ├── cluster
│   │   ├── private.key
│   │   ├── public.crt
│   │   ├── revoked
│   │   └── trusted
│   │       └── public.crt
│   └── https
│       ├── private.key
│       ├── public.crt
│       ├── revoked
│       └── trusted
├── bolt
│   ├── neo4j.cert -> /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem
│   ├── neo4j.key -> /etc/letsencrypt/live/shoppingstories.org-0001/privkey.pem
│   ├── public.crt -> /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem
│   └── trusted
│       └── neo4j.cert -> /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem
├── cluster
│   ├── neo4j.cert -> /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem
│   ├── neo4j.key -> /etc/letsencrypt/live/shoppingstories.org-0001/privkey.pem
│   ├── public.crt -> /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem
│   └── trusted
│       └── neo4j.cert -> /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem
└── https
    ├── neo4j.cert -> /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem
    ├── neo4j.key -> /etc/letsencrypt/live/shoppingstories.org-0001/privkey.pem
    ├── public.crt -> /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem
    └── trusted
        └── neo4j.cert -> /etc/letsencrypt/live/shoppingstories.org-0001/fullchain.pem
```
