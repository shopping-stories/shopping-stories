# Vue App Overview

The Vue website application lives in the `web/` directory. Here are the current contents of that directory:

```bash
web
├── babel.config.js
├── ecosystem.config.js
├── package.json
├── package-lock.json
├── public
│   ├── favicon.ico
│   └── index.html
├── README.md
├── src
│   ├── App.vue
│   ├── assets
│   │   ├── bezos.jpg
│   │   ├── favicon.ico
│   │   ├── HRI-Logo.png
│   │   ├── ledger.jpg
│   │   ├── neo4jBrowser.svg
│   │   └── Painting.png
│   ├── components
│   │   ├── AppBar.vue
│   │   ├── Browser.vue
│   │   ├── CheckImports.vue
│   │   ├── Docs.vue
│   │   ├── Footer.vue
│   │   ├── ImportData.vue
│   │   ├── Snackbar.vue
│   │   ├── UserManagement.vue
│   │   └── UserSettings.vue
│   ├── images
│   │   ├── Books.jpg
│   │   ├── C_1760_023_ElizabethConnell.jpg
│   │   ├── ColchesterToday.jpg
│   │   ├── ElizabethConnell2.jpg
│   │   ├── ElizabethConnell.jpg
│   │   ├── esther.jpg
│   │   ├── HighLifeBelowStairs.jpg
│   │   ├── Image Credits.docx
│   │   ├── JohnGlassford.jpg
│   │   ├── Map.png
│   │   ├── molly.jpg
│   │   ├── Money.jpg
│   │   ├── Other
│   │   │   ├── colonial_money.jpg
│   │   │   ├── floor1.jpg
│   │   │   ├── HR_logo.png
│   │   │   ├── map1.jpg
│   │   │   ├── map2.PNG
│   │   │   ├── map3.jpg
│   │   │   ├── map4.jpg
│   │   │   ├── quill1.jpg
│   │   │   ├── quill2.jpg
│   │   │   ├── quill3.jpg
│   │   │   ├── quill4.jpg
│   │   │   ├── ship.PNG
│   │   │   └── trade.jpg
│   │   ├── parchment.jpg
│   │   ├── Quill01.jpg
│   │   ├── quill1.jpg
│   │   ├── quill3.jpg
│   │   ├── Research01.jpg
│   │   ├── Research02.jpg
│   │   ├── Researching.jpg
│   │   ├── Tobacco.jpg
│   │   └── wallpaper1.jpg
│   ├── main.js
│   ├── plugins
│   │   └── vuetify.js
│   ├── router
│   │   └── index.js
│   ├── services
│   │   └── http-common.js
│   ├── store
│   │   └── index.js
│   └── views
│       ├── About.vue
│       ├── Acknowledgements.vue
│       ├── Dashboard.vue
│       ├── Files.vue
│       ├── ForgotPassword.vue
│       ├── Home.vue
│       ├── Ledgers.vue
│       ├── Profile.vue
│       ├── Project.vue
│       ├── Search.vue
│       ├── Signin.vue
│       ├── Signup.vue
│       ├── Team.vue
│       ├── Transcription.vue
│       └── Verify.vue
└── vue.config.js
```

## Top Level System Files

There are a few top level system files that help run the Vue web app.

### web/package.json & web/package-lock.json

`web/package.json` is the starting point for the Vue App. It houses the start scripts and the version of the npm packages the team has installed for use in the app. `web/package-lock.json` is auto-maintained by npm and maintains the specifics of the packages so that the app runs smooth on every computer.

```json
// web/package.json

{
  "name": "web-app",
  "version": "0.1.0",
  "private": true,
  "scripts": {
    "serve": "vue-cli-service serve",
    "build": "vue-cli-service build",
    "lint": "vue-cli-service lint"
  },
...
```

### web/ecosystem.config.js

`web/ecosystem.config.js` is a start script used by PM2, which is a Node manager program. The team highly recommends utilizing this tool as it can simplify web development. More information can be found [here](https://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/).

```js
// web/ecosystem.config.js

module.exports = {
  apps : [{
    name: "shoppingstories.org",
    script: 'npm',
    args: 'run serve',
    watch: '.'
  }],

  deploy : {
    production : {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
```

### web/vue.config.js

This file was used primarily during the development phase of the project. The vue client can do a variety of actions that help the development process, more information can be found [here](https://cli.vuejs.org/config/).

```js
// web/vue.config.js

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    disableHostCheck: true
  }
}
```

## Environmental Variables

When cloning this repository, the `web/.env` file will not appear even though it is needed to run the application both locally and in the production build. Here is a breakdown of what that file would look like:

```.env
# .env

# Build
VUE_APP_NODE_ENV="development"

# Vue Port
VUE_APP_HTTP_PORT="XXXX"

# Cognito Credentials
VUE_APP_COGNITO_USERPOOLID="XXXX"
VUE_APP_COGNITO_IDENTITYPOOLID="XXXX"
VUE_APP_COGNITO_WEBCLIENTID="XXXX"
VUE_APP_COGNITO_REGION="XXXX"

# API
VUE_APP_API_URL="XXXX"
VUE_APP_API_TOKEN="XXXX"
```

### Build

The Build environmental variable will just let the Vue application know what build to run the program on. This is set for 'development' for testing and configuring, however the 'production' build is the proper format for the live website.

```.env
# Build
VUE_APP_NODE_ENV="development"
```

### Vue Port

The Port variable tells the Vue application what port to run on in the server or locally. Group 7 used port 8080 primarily during development.

```.env
# Vue Port
VUE_APP_HTTP_PORT="XXXX"
```

### Cognito Credentials

This portion of the `.env` file will tell the Express App what credentials to use when connecting to AWS Cognito. Learn more about where to find these here.

*doublechecklink*

```.env
# Cognito Credentials
VUE_APP_COGNITO_USERPOOLID="XXXX"
VUE_APP_COGNITO_IDENTITYPOOLID="XXXX"
VUE_APP_COGNITO_WEBCLIENTID="XXXX"
VUE_APP_COGNITO_REGION="XXXX"
```

### API

These variables in the `.env` file indicate the base URL of the Express API Endpoints. You can read more about the base URL here. *doublechecklink*

```.env
# API
VUE_APP_API_URL="XXXX"
VUE_APP_API_TOKEN="XXXX"
```

## src Directory

The `src` directory houses most of the main components that make up the Vue web application. This is where all of the `.vue` files live, as well as any images, assets, and front-end javascript files.

```bash
web
├── src
│   ├── App.vue
│   ├── assets
│   │   ├── bezos.jpg
│   │   ├── favicon.ico
│   │   ├── HRI-Logo.png
│   │   ├── ledger.jpg
│   │   ├── neo4jBrowser.svg
│   │   └── Painting.png
│   ├── components
│   │   ├── AppBar.vue
│   │   ├── Browser.vue
│   │   ├── CheckImports.vue
│   │   ├── Docs.vue
│   │   ├── Footer.vue
│   │   ├── ImportData.vue
│   │   ├── Snackbar.vue
│   │   ├── UserManagement.vue
│   │   └── UserSettings.vue
│   ├── images
│   │   ├── Books.jpg
│   │   ├── C_1760_023_ElizabethConnell.jpg
│   │   ├── ColchesterToday.jpg
│   │   ├── ElizabethConnell2.jpg
│   │   ├── ElizabethConnell.jpg
│   │   ├── esther.jpg
│   │   ├── HighLifeBelowStairs.jpg
│   │   ├── Image Credits.docx
│   │   ├── JohnGlassford.jpg
│   │   ├── Map.png
│   │   ├── molly.jpg
│   │   ├── Money.jpg
│   │   ├── Other
│   │   │   ├── colonial_money.jpg
│   │   │   ├── floor1.jpg
│   │   │   ├── HR_logo.png
│   │   │   ├── map1.jpg
│   │   │   ├── map2.PNG
│   │   │   ├── map3.jpg
│   │   │   ├── map4.jpg
│   │   │   ├── quill1.jpg
│   │   │   ├── quill2.jpg
│   │   │   ├── quill3.jpg
│   │   │   ├── quill4.jpg
│   │   │   ├── ship.PNG
│   │   │   └── trade.jpg
│   │   ├── parchment.jpg
│   │   ├── Quill01.jpg
│   │   ├── quill1.jpg
│   │   ├── quill3.jpg
│   │   ├── Research01.jpg
│   │   ├── Research02.jpg
│   │   ├── Researching.jpg
│   │   ├── Tobacco.jpg
│   │   └── wallpaper1.jpg
│   ├── main.js
│   ├── plugins
│   │   └── vuetify.js
│   ├── router
│   │   └── index.js
│   ├── services
│   │   └── http-common.js
│   ├── store
│   │   └── index.js
│   └── views
│       ├── About.vue
│       ├── Acknowledgements.vue
│       ├── Dashboard.vue
│       ├── Files.vue
│       ├── ForgotPassword.vue
│       ├── Home.vue
│       ├── Ledgers.vue
│       ├── Profile.vue
│       ├── Project.vue
│       ├── Search.vue
│       ├── Signin.vue
│       ├── Signup.vue
│       ├── Team.vue
│       ├── Transcription.vue
│       └── Verify.vue
```

## src/main.js

This file creates the Vue application, hence the name. The Vue app is mounted here.

```js
// web/src/main.js

...
new Vue({
  router,
  store,
  vuetify,
  components: {
    ValidationProvider
  },
  render: h => h(App)
}).$mount('#app')
```

## src/App.vue

This is the initial view of the website. The `AppBar.vue` component is called before any other components so that it is stationary on the website in all views/routes/pages. Then there is the `v-main` component that houses all of the routes inside the Vue application. Finally, the `Footer.vue` component is always visible similar to `AppBar.vue`. Lastly, the `Snackbar.vue` component is outside of the routes as well so that dynamic messages can be seen on every page.

```html
<!-- web/src/App.vue -->

<template>
  <v-app v-show="isReady">
    <AppBar> </AppBar>

    <v-main class="fill-height backgroundColor">
      <router-view></router-view>
    </v-main>
    <Footer />
    <Snackbar />
  </v-app>
</template>
...
```

## src/views Directory

The `src/views` directory houses all of the unique pages that clients visit when using the Vue web application. These Vue components are registered in the router and have unique routes, such as `https://shoppingstories.org/about` for `About.vue`.

```bash
web  
├── src
│   └── views
│       ├── About.vue
│       ├── Acknowledgements.vue
│       ├── Dashboard.vue
│       ├── Files.vue
│       ├── ForgotPassword.vue
│       ├── Home.vue
│       ├── Ledgers.vue
│       ├── Profile.vue
│       ├── Project.vue
│       ├── Search.vue
│       ├── Signin.vue
│       ├── Signup.vue
│       ├── Team.vue
│       ├── Transcription.vue
│       └── Verify.vue
```

## src/components Directory

The `src/components` directory houses all of the unique and often reusable components for the Vue web application. Most of these components are tabs that live in `Dashboard.vue` and show on `https://shoppingstories.org/dashboard`.

```bash
web  
├── src
│   ├── components
│   │   ├── AppBar.vue
│   │   ├── Browser.vue
│   │   ├── CheckImports.vue
│   │   ├── Docs.vue
│   │   ├── Footer.vue
│   │   ├── ImportData.vue
│   │   ├── Snackbar.vue
│   │   ├── UserManagement.vue
│   │   └── UserSettings.vue
```

## src/router Directory

The `src/router` directory holds the router `index.js` file, which is the glue that holds the website together. All of the Vue components in `src/views` are registered in the router and act as webpages.

```bash
web  
├── src
│   ├── router
│   │   └── index.js
```

## src/services Directory

The `src/services` directory houses the `http-common.js` file, which activates the Axios HTTP module. This allows the Vue application to talk to the API endpoints that live in the Express App.

```bash
web  
├── src
│   ├── services
│   │   └── http-common.js
```

## src/store Directory

The `src/store` directory holds the setup information for AWS Cognito. It also creates 'global' variables and functions that can be used on multiple components.

```bash
web  
├── src
│   ├── store
│   │   └── index.js
```

## src/assets & src/images

As the name probably implies, the `src/assets` and the `src/images` directories house the images used in the Vue web application.

```bash
web  
├── src
│   ├── assets
│   │   ├── bezos.jpg
│   │   ├── favicon.ico
│   │   ├── HRI-Logo.png
│   │   ├── ledger.jpg
│   │   ├── neo4jBrowser.svg
│   │   └── Painting.png
│   ├── images
│   │   ├── Books.jpg
│   │   ├── C_1760_023_ElizabethConnell.jpg
│   │   ├── ColchesterToday.jpg
│   │   ├── ElizabethConnell2.jpg
│   │   ├── ElizabethConnell.jpg
│   │   ├── esther.jpg
│   │   ├── HighLifeBelowStairs.jpg
│   │   ├── Image Credits.docx
│   │   ├── JohnGlassford.jpg
│   │   ├── Map.png
│   │   ├── molly.jpg
│   │   ├── Money.jpg
│   │   ├── Other
│   │   │   ├── colonial_money.jpg
│   │   │   ├── floor1.jpg
│   │   │   ├── HR_logo.png
│   │   │   ├── map1.jpg
│   │   │   ├── map2.PNG
│   │   │   ├── map3.jpg
│   │   │   ├── map4.jpg
│   │   │   ├── quill1.jpg
│   │   │   ├── quill2.jpg
│   │   │   ├── quill3.jpg
│   │   │   ├── quill4.jpg
│   │   │   ├── ship.PNG
│   │   │   └── trade.jpg
│   │   ├── parchment.jpg
│   │   ├── Quill01.jpg
│   │   ├── quill1.jpg
│   │   ├── quill3.jpg
│   │   ├── Research01.jpg
│   │   ├── Research02.jpg
│   │   ├── Researching.jpg
│   │   ├── Tobacco.jpg
│   │   └── wallpaper1.jpg
```
