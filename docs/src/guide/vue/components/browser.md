# Browser.vue

## Description

A simple component that lives in the user's Dashboard. It is only accessible by Molly's team and herself. Users can access the Neo4j Browser through the link on this component.

![Browser](../images/browser.png)

## HTML

The main component is the `<v-card>` where the component title is and the button that takes users to the Neo4j browser.

```html
<v-card
  max-width="45vw"
  class="mx-auto"
  outlined
  shaped
  elevation="4"
>
  <v-card-text class="justify-center"
    elevation="2"
  >
  <h1> Access the Neo4j Browser! </h1>
  <br>
  <p> The Neo4j Browser is where users can view and interact with the Neo4j Database. It houses many unique features and functionalities.
  It uses the Cypher query language for all actions inside the browser. Click <a href="https://neo4j.com/developer/cypher/">here</a> to learn more
  about Cypher. </p>
  <p> In order to use the Neo4j Browser, users must sign into the proper account. This is provided by the Admin. The browser currently lives
  in the subdomain <a href="https://browser.shoppingstories.org">browser.shoppingstories.org</a>. </p>
  <br>
  <v-card-actions  class="justify-center mx-auto mt-5">
  <v-btn
    @click="submit()"
    elevation="2"
    color="secondary"
  > Click Here!
  </v-btn>
  </v-card-actions>

  </v-card-text>
</v-card>
```

## Methods

### submit()

When users click the button, the function opens a new window to `browser.shoppingstories.org`.

```js
submit() {
  window.open('https://browser.shoppingstories.org/?connectURL=browser.shoppingstories.org')
}
```
