# Docs.vue

## Description

A simple component that lives in the user's Dashboard. It is only accessible by Molly's team and herself. Users can access the live documentation website through the link on this component.

::: tip
You are already here!
:::

![Docs](../images/docs.png)

## HTML

The main component is the `<v-card>` where the component title is and the button that takes users to the documentation website.

```html
<v-card
  max-width="45vw"
  class="mx-auto"
  outlined
  shaped
  elevation="4"
>
  <v-card-text class="justify-center"
    elevation="2"
  >
  <h1> View the Documentation! </h1>
  <br>

  <p> The documentation lives in and is hosted by AWS S3. This ensures the documentation stays live if anything ever happens to the
  website or the server. The link for the documentation is <a href="http://docs.shoppingstories.org">docs.shoppingstories.org</a>. </p>
  <br>

  <v-card-actions  class="justify-center mx-auto mt-5">
  <v-btn
    @click="submit()"
    elevation="2"
    color="secondary"
  > View Docs!
  </v-btn>
  </v-card-actions>

  </v-card-text>
</v-card>
```

## Methods

### submit()

When users click the button, the function opens a new window to `docs.shoppingstories.org`.

```js
submit() {
  window.open('http://docs.shoppingstories.org/')
}
```
