# AppBar.vue

## Description

AppBar.vue is a component used on the website that is always visible on every single webpage/view. It serves as the primary navigation tool for the user to easily jump from any page of the website to another as well as login or logout of their user account.

## HTML

The components used in this view are `<v-app-bar>`,` <v-btn>`, `<v-menu>`, `<v-icon>`, and `<v-list>`. Everything is contained within the `<v-app-bar>` component. The `<v-app-bar>` component contains props that define the color of the bar, its height, and an elevate on scroll characteristic. The website logo is displayed through the `<img> `API and placed next to a `<v-toolbar-title>`. This is all contained within a `<div>` that is made clickable so the user can go to the homepage simply by clicking on the website logo/name.

 On normal desktop viewport sizes, the buttons on the App Bar will be "About History Revealed" and a dropdown menu named "The Shopping Stories Project". When the user is logged in, they may also see a button named "Dashboard". The dropdown menu uses a data array in the `<script>` section of the view to display a list of the other webpages a user can access. To the right of these buttons is a "Login/Sign-Up" button that will change to a "Logout" button if the user has already logged into their account.

<img height="2000" src="https://i.imgur.com/TdRdpys.png" alt="Screen Shot 2020-11-13 at 11.26.03 PM" style="zoom:25%;" />

On narrow-width or mobile viewport sizes, the main buttons of the App Bar are removed and a new menu button appears that condenses the previous available buttons into a single menu list.

`<v-list>` and `<v-list-item>` are used to traverse data arrays and populate these menu lists.

The other main level APIs used in this view are `<img>` for the website logo and `<v-spacer>` to evenly space the items in the App Bar.

## Imports

## Data

The data for this view are two data arrays that list out the options for the `<v-btn>` dropdown menu on desktop viewports and the mobile menu for smaller viewports. These arrays simply contain the icon to be used, the name of each page, and the router link that will be used if the option is clicked.

## Methods

```
methods: {
  clickMethod() {
    this.$router.push({ name: "Home" }).catch(err => {console.log("Redundant Navigation, error: " + err)})
  },
  pushAbout() {
    this.$router.push({ name: "About" }).catch(err => {console.log("Redundant Navigation, error: " + err)})
  },
  pushDashboard() {
    this.$router.push({ name: "Dashboard" }).catch(err => {console.log("Redundant Navigation, error: " + err)})
  },
  pushSignIn() {
    this.$router.push({ name: "Signin" }).catch(err => {console.log("Redundant Navigation, error: " + err)})
  },
  openTwitter() {
    window.open("https://twitter.com/History_Reveal", "_blank");
  },
  openFacebook() {
    window.open("https://www.facebook.com/HistoryReveal/", "_blank");
  },
  openInstagram() {
    window.open("https://www.instagram.com/history_reveal/", "_blank");
  },
```

Included in the methods section are methods for each of the buttons in the App Bar that push the various router links when clicked.