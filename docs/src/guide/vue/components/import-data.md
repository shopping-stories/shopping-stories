# ImportData.vue

## Description

`ImportData.vue` looks deceptively simple but there is a lot of functionality going on under the hood. This is probably one of the most important components on the website as it relates to the original premise of the Group 7 design project.

It is here that users will upload Excel files that will be parsed into the database. The user will first select a file to upload, we verify that it is a valid file, then we send it off to the Express App to strip the contents.

If we run into any errors with the file, which usually relate to syntax, we alert the user. Otherwise, we alert the user that the file has been successfully uploaded.

![Import Data](../images/importdata.png)

## HTML

First we allows users to grab a file in their system through the use of the `v-file-input` component. Currently, users can only upload one file at a time. We also specify which files we accept. This has only been tested using the `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet` file type as that is what we have used.

::: tip
The `accept` attribute below will initially hide everything that is not the specified file types. However, users can easily bypass this. Therefore, there are further guardrails later in the code.
:::

We also use the `v-chip` component to visualize the selection the user has made.

```html
<v-file-input
  show-size
  placeholder="Select a file to upload"
  solo
  accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
  type="file"
  v-model="file"
>
  <template v-slot:selection="{ text }">
    <v-chip
      medium
      label
      color="secondary"
    >
      {{ text }}
    </v-chip>
  </template>
</v-file-input>
```

The user has an option of selecting one of two document types, `Master` and `Transactions`. This will inform the functions in the Javascript of where to send the file.

```html
<v-select
  v-model="selectVal"
  :items="items"
  solo
  placeholder="Select an upload type"
  :prepend-icon="selectVal ? 'mdi-check' : 'mdi-select'"
></v-select>
```
Finally, we have the submit button that confirms the selections and starts the upload process.

```html
<v-card-actions class="ma-5 mt-0 justify-center">
  <v-btn
    :loading="loading"
    :disabled="loading"
    @click="uploadFile()"
  >
    Upload {{ selectVal }}
    <v-icon
      right
      color="secondary"
    >
      mdi-cloud-upload
    </v-icon>
  </v-btn>
</v-card-actions>
```

## Imports

```js
import { HTTP } from '../services/http-common';
import { mapMutations } from "vuex";
```

## Methods

Handy Snackbar!

```js
...mapMutations(["setSnackbar"]),
```

The `checkFile()` function confirms that the selected file is not already in the S3 bucket, and therefore already uploaded and parsed. If so, we reject that file and tell the user to try again with a different one.

This helps avoid redundancy in the database. As of this writing, there simply is not a clean and efficient way to scrub the database from duplicate data. As such, we monitor closely what is uploaded into the database.

```js
async checkFile() {
  await HTTP.post('import/filecheck', {name: this.filename, bucket: 'shoppingstories'})
  .then((response) => {
    this.exists = response.data.value;
  })
  .catch((err) => {
    console.log("filecheck error: ", err)
  })
},
```

Here is where we access the file, ensure it has not already been uploaded, is the right file type, and that a selection has been made regarding the type (ie. `Master` or `Transactions`).

We then send that file to the Express App in the form of a `FormData` object. Since we can't send raw Excel files through HTTP API calls, we have to create this object to hold the file in place as it goes through the wires.

After the API call, we let the user know if this attempt was successful. Common errors are almost entirely syntax errors. These are caught by Neo4j when we attempt to run the built query into the database. 

```js
// used to upload the users file to the server,
// parse it, and import it into the database.
async uploadFile() {

  // check file
  if (!this.file) {
    this.setSnackbar({
      type: "warning",
      msg: "Please select file to upload",
      timeout: 5000,
    });
    return;
  }

  // check upload type selection
  else if (!this.selectVal) {
    this.setSnackbar({
      type: "warning",
      msg: "Please select upload type",
      timeout: 5000,
    });
    return;
  }

  // checking if file type is correct
  else if (this.file['type'] !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && this.file['type'] !== '.csv' && this.file['type'] !== 'application/vnd.ms-excel') {
    this.setSnackbar({
      type: "warning",
      msg: "That file type is not supported (.xlsx, .xls, .csv files only)",
      timeout: 5000,
    });
    return;
  }

  // set loading
  this.loader = 'loading'

  // check if file already exists
  this.filename = this.file.name
  await this.checkFile();

  if (this.exists) {
   this.loader = null
   this.loading = false
   this.setSnackbar({
     type: "error",
     msg: "Error: that file has already been uploaded. Try another.",
     timeout: 5000,
   });
   return;
  }

  // creates new form for API request
  let formData = new FormData();
  formData.set(name, this.file.name)
  formData.append('file', this.file);
  const selection = this.selectVal.toLowerCase()

  // axios call to proper endpoint
  HTTP.put('/import/' + selection, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
  .then((response) => {

    if (response.status == 200) {
      this.setSnackbar({
        type: "success",
        msg: "File successfully uploaded!",
      })
    }
    else if (response.status == 201) {
      this.setSnackbar({
        type: "error",
        msg: "Unable to parse file! Syntax Error: " + response.data,
        timeout: 20000,
      })
    }
    else if (response.status == 202) {
      this.setSnackbar({
        type: "error",
        msg: "Unable to parse file: " + response.data,
        timeout: 20000,
      })
    }
  })
  .catch(() => {
    this.error = true;
    this.setSnackbar({
      type: "error",
      msg: this.snackbarMSG(),
      timeout: 10000,
    });
  })
  .finally(() => {
    this.file = null;
    this.selectVal = null;
    this.loading = false
    this.loader = null
  });
},
```

## Data

```js
data: () => ({
  // Determines the parser to use
  selectVal: undefined,

  // Controls when to show the snackbar
  displaySnackbar: false,

  // Controls the loading icon
  upload: false,

  // Controls what color to show for the snackbar
  error: false,

  // list of different parsers available.
  items: ["Master", "Transactions"],

  file: null,
  filename: null,

  loader: null,
  loading: false,
}),
```

## Watch

```js
watch: {
  loader () {
    const l = this.loader
    this.[l] = !this.[l]
  },
},
```
