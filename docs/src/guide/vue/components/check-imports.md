# CheckImports.vue

## Description

This component is where Molly and her team and review the contents of the S3 buckets: `shoppingstories` and `ss-documents`. More information on S3's [here](http://docs.shoppingstories.org/guide/aws/s3.html).

The component allows basic CRUD operations on the contents of each bucket.

![CheckImports](../images/checkimports.png)

## HTML

There are many components in this file. In the future, it may be easier to separate this component into several different components.

Breaking it down, first we have the `<v-tabs>` which separate the two buckets and their respective data tables into tabs. This part sets it up:

```html
<!-- Tabs -->
<v-tabs
  v-model="tab"
  align-with-title
  background-color="secondary"
  dark
>
  <v-tabs-slider color="primary"></v-tabs-slider>
  <v-tab
    v-for="item in items"
    :key="item"
    @click="reveal = false"
  >
    {{ item }}
  </v-tab>
</v-tabs>
```

Then we have the `<v-tabs-items` component that houses the two separate tabs and their contents. First is the tab for the `shoppingstories` bucket. This is where it may be beneficial to break this component up into smaller components in separate files:

```html
<!-- shoppingstories Bucket -->
<v-tab-item>
  <v-card>
    <v-card-title>
    File Management

    <v-btn
      @click="uploadItem"
      text
    >
      <v-icon color="secondary" style="pb-1" medium> {{ uploadIcon }} </v-icon>
    </v-btn>
    <v-spacer></v-spacer>
      <v-text-field
        v-model="search"
        label="Search (UPPER CASE ONLY)"
        class="mx-4"
        color="secondary"
        single-line
        hide-details
        append-icon="mdi-magnify"
      ></v-text-field>
      <v-btn
        @click="refresh"
        text
        :loading="loadingButton"
      >
        <v-icon color="secondary" style="pb-1" medium> {{ refreshIcon }} </v-icon>
      </v-btn>
    </v-card-title>
    <v-card-subtitle> Manage S3 Bucket Content </v-card-subtitle>
      <v-data-table
        :headers="headers"
        :items="payload"
        item-key="name"
        class="elevation-1"
        :loading="isLoading"
        :search="search"
        :custom-filter="filterOnlyCapsText"
      >
      <template v-slot:top>
      <!-- Delete Dialog -->
      <v-dialog v-model="dialogDelete" max-width="500px">
        <v-card>
          <v-card-title class="headline">Are you sure you want to delete this item?</v-card-title>
          <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn color="secondary " text @click="closeDelete">Cancel</v-btn>
            <v-btn color="red darken-1" text @click="deleteItemConfirm">Delete</v-btn>
            <v-spacer></v-spacer>
          </v-card-actions>
        </v-card>
      </v-dialog>
      <!-- Upload Dialog -->
      <v-dialog v-model="dialogUpload" max-width="700px">
        <v-card>
          <v-card-title class="headline">Select the file you wish to upload.</v-card-title>
          <v-card-subtitle> This file will not be parsed and will upload straight to the S3 bucket.</v-card-subtitle>
          <v-container>
          <v-file-input
            show-size
            placeholder="Select a file to upload"
            solo
            accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
            type="file"
            v-model="file"
          >
            <template v-slot:selection="{ text }">
              <v-chip
                medium
                label
                color="secondary"
              >
                {{ text }}
              </v-chip>
            </template>
          </v-file-input>
          </v-container>
          <v-card-actions>
            <v-spacer></v-spacer>

            <v-btn @click="uploadFile">
              Upload
              <v-icon
              right
              color="secondary"
            >
              mdi-cloud-upload
            </v-icon>
          </v-btn>
            <v-spacer></v-spacer>
          </v-card-actions>
        </v-card>
      </v-dialog>
      </template>

      <v-progress-linear
        v-show="isLoading"
        slot="progress"
        color="secondary"
        indeterminate
      >
      </v-progress-linear>
      <template v-slot:item.actions="{ item }">
      <v-row>
      <v-col>
        <v-icon
          small
          color="secondary"
          @click="deleteItem(item)"
        >
          mdi-delete
        </v-icon>
        </v-col>
        <v-col>
        <v-icon
          small
          color="secondary"
          @click="downloadItem(item)"
        >
          mdi-cloud-download-outline
        </v-icon>
        </v-col>
        </v-row>
      </template>
    </v-data-table>
  </v-card>
</v-tab-item>
```

And here is the second tab for the `ss-documents` bucket:
```html
<!-- ss-documents Bucket -->
<v-tab-item>
  <v-card>

    <v-card-title>
    File Management

    <v-btn
      @click="uploadItemDocuments"
      text
    >
      <v-icon color="secondary" style="pb-1" medium> {{ uploadIcon }} </v-icon>
    </v-btn>
    <v-spacer></v-spacer>
      <v-text-field
        v-model="search"
        label="Search (UPPER CASE ONLY)"
        class="mx-4"
        color="secondary"
        single-line
        hide-details
        append-icon="mdi-magnify"
      ></v-text-field>
      <v-btn
        @click="refresh"
        text
        :loading="loadingButton"
      >
        <v-icon color="secondary" style="pb-1" medium> {{ refreshIcon }} </v-icon>
      </v-btn>
    </v-card-title>
    <v-card-subtitle> Manage S3 Bucket Content </v-card-subtitle>
      <v-data-table
        :headers="headers"
        :items="documentPayload"
        item-key="name"
        class="elevation-1"
        :loading="isLoading"
        :search="search"
        :custom-filter="filterOnlyCapsText"
      >
      <template v-slot:top>
      <!-- Delete Dialog -->
      <v-dialog v-model="dialogDeleteDocuments" max-width="500px">
        <v-card>
          <v-card-title class="headline">Are you sure you want to delete this item?</v-card-title>
          <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn color="secondary " text @click="closeDeleteDocuments">Cancel</v-btn>
            <v-btn color="red darken-1" text @click="deleteItemConfirmDocuments">Delete</v-btn>
            <v-spacer></v-spacer>
          </v-card-actions>
        </v-card>
      </v-dialog>
      <!-- Upload Dialog -->
      <v-dialog v-model="dialogUploadDocuments" max-width="700px">
        <v-card>
          <v-card-title class="headline">Select the file you wish to upload.</v-card-title>
          <v-card-subtitle> This file will not be parsed and will upload straight to the S3 bucket.</v-card-subtitle>
          <v-container>
          <v-file-input
            show-size
            placeholder="Select a file to upload"
            solo
            accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
            type="file"
            v-model="file"
          >
            <template v-slot:selection="{ text }">
              <v-chip
                medium
                label
                color="secondary"
              >
                {{ text }}
              </v-chip>
            </template>
          </v-file-input>
          </v-container>
          <v-card-actions>
            <v-spacer></v-spacer>

            <v-btn @click="uploadFileDocuments">
              Upload
              <v-icon
              right
              color="secondary"
            >
              mdi-cloud-upload
            </v-icon>
          </v-btn>
            <v-spacer></v-spacer>
          </v-card-actions>
        </v-card>
      </v-dialog>
      </template>

      <v-progress-linear
        v-show="isLoading"
        slot="progress"
        color="secondary"
        indeterminate
      >
      </v-progress-linear>
      <template v-slot:item.actions="{ item }">
      <v-row>
      <v-col>
        <v-icon
          small
          color="secondary"
          @click="deleteItemDocuments(item)"
        >
          mdi-delete
        </v-icon>
        </v-col>
        <v-col>
        <v-icon
          small
          color="secondary"
          @click="downloadItemDocuments(item)"
        >
          mdi-cloud-download-outline
        </v-icon>
        </v-col>
        </v-row>
      </template>
    </v-data-table>
  </v-card>
</v-tab-item>
```

## Imports

```js
import { HTTP } from '../services/http-common'
import { mapMutations } from 'vuex'
```

## Methods

### Snackbar

```js
...mapMutations(["setSnackbar"]),
```

### getDocumentsPayload()

Grabs the payload JSON to fill in the headers for the `ss-documents` bucket and corresponding tab. Finishes by stopping the loading animation.

```js
getDocumentsPayload() {
  const bucket = 'ss-documents'
  HTTP.get('import/files', {params: { bucket: bucket }})
  .then((response) => {
    // grab message payload
    this.documentPayload = response.data.message
  })
  .catch((e) => {
    console.log(e)
    // alert user of error
    this.setSnackbar({
      type: "error",
      msg: 'Error: Unable to retrieve files',
      timeout: 20000,
    })
  })
  .finally(() => {
    this.isLoading = false;
  })
},
```

### getPayload()

Grabs the payload JSON to fill in the headers for the `shoppingstories` bucket and corresponding tab. Finishes by stopping the loading animation.

```js
getPayload() {
  const bucket = 'shoppingstories'
  HTTP.get('import/files', {params: { bucket: bucket }})
  .then((response) => {
    // grab message payload
    this.payload = response.data.message
  })
  .catch((e) => {
    console.log(e)
    // alert user of error
    this.setSnackbar({
      type: "error",
      msg: 'Error: Unable to retrieve files',
      timeout: 20000,
    })
  })
  .finally(() => {
    this.isLoading = false;
  })

},
```

### filterOnlyCapsText()

For both tabs, allows the user to search through the contents of the S3 buckets. All caps but can be all undercase as well.

```js
filterOnlyCapsText (value, search) {
  return value != null &&
  search != null &&
  typeof value === 'string' &&
  value.toString().toLocaleUpperCase().indexOf(search) !== -1
},
```

### refresh()

Refreshes the contents of both tabs from the corresponding S3 buckets. The timeout is there simply to alert the user of a button action, especially useful when there is no change to the contents of the data tables.

```js
refresh() {
  this.loadingButton = true;
  this.getPayload()
  this.getDocumentsPayload()

  // to alert user of action
  setTimeout(() => {
    this.loadingButton = false;
  }, 500)
},
```

### deleteItem()

Grabs the item that was selected in the `shoppingstories` tab, and opens the corresponding dialog box for user input.

```js
deleteItem(item) {
  this.editedIndex = this.payload.indexOf(item)
  this.editedItem = Object.assign({}, item)
  this.dialogDelete = true
},
```

### deleteItemDocuments()

Grabs the item that was selected in the `shoppingstories` tab, and opens the corresponding dialog box for user input.

```js
deleteItemDocuments(item) {
  this.editedIndex = this.payload.indexOf(item)
  this.editedItem = Object.assign({}, item)
  this.dialogDeleteDocuments = true
},
```

### deleteItemConfirm()

Confirms with the user that the item that was grabbed should be deleted. Makes an API call to the Express App, which will then delete the item from the `shoppingstories` bucket.

```js
deleteItemConfirm () {
  this.payload.splice(this.editedIndex, 1)

  HTTP.delete('import/file', {params: {filename: this.editedItem.file}})
  .then((response) => {
    if (response.status == '200') {
      this.setSnackbar({
        type: "success",
        msg: 'Successfully deleted ' + this.editedItem.file,
        timeout: 5000,
      })
    }

    else if (response.status == '300') {
      this.setSnackbar({
        type: "warning",
        msg: 'File ' + this.editedItem.file + ' does not exist',
        timeout: 5000,
      })
    }
  })
  .catch((err) => {
    console.log('Failed deleteItemConfirm: ', err)
    this.setSnackbar({
      type: "error",
      msg: 'Unable to delete file',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.closeDelete()
    this.refresh()
  })
},
```

### deleteItemConfirmDocuments()

Confirms with the user that the item that was grabbed should be deleted. Makes an API call to the Express App, which will then delete the item from the `ss-documents` bucket.

```js
deleteItemConfirmDocuments () {
  this.payload.splice(this.editedIndex, 1)

  HTTP.delete('import/file/documents', {params: {filename: this.editedItem.file}})
  .then((response) => {
    if (response.status == '200') {
      this.setSnackbar({
        type: "success",
        msg: 'Successfully deleted ' + this.editedItem.file,
        timeout: 5000,
      })
    }

    else if (response.status == '300') {
      this.setSnackbar({
        type: "warning",
        msg: 'File ' + this.editedItem.file + ' does not exist',
        timeout: 5000,
      })
    }
  })
  .catch((err) => {
    console.log('Failed deleteItemConfirmDocuments: ', err)
    this.setSnackbar({
      type: "error",
      msg: 'Unable to delete file',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.closeDeleteDocuments()
    this.refresh()
  })
},
```

### closeDelete()

Closes the dialog message box for the `shoppingstories` tab.

```js
closeDelete () {
  this.dialogDelete = false
  this.$nextTick(() => {
    this.editedItem = Object.assign({}, this.defaultItem)
    this.editedIndex = -1
  })
},
```

### closeDeleteDocuments()

Closes the dialog message box for the `ss-documents` tab.

```js
closeDeleteDocuments () {
  this.dialogDeleteDocuments = false
  this.$nextTick(() => {
    this.editedItem = Object.assign({}, this.defaultItem)
    this.editedIndex = -1
  })
},
```

### downloadItem()

Allows users to download items from the `shoppingstories` bucket. First calls the API and requests a `blob` object to be returned. The Express App responds with the requested excel file in `blob` form. The method then converts the blob into a proper excel file and creates a URL link for which the client is able to automatically download the file.

```js
downloadItem(item) {
  this.editedIndex = this.payload.indexOf(item)
  this.editedItem = Object.assign({}, item)
  HTTP.get('import/file/download', {responseType: 'blob',  params: {filename: this.editedItem.file}} )
  .then((response) => {
    const url = URL.createObjectURL(new Blob([response.data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    }))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', this.editedItem.file)
    document.body.appendChild(link)
    link.click()
    if (response.status == '200') {
      this.setSnackbar({
        type: "success",
        msg: 'Downloading ' + this.editedItem.file,
        timeout: 5000,
      })
    }

    else if (response.status == '300') {
      this.setSnackbar({
        type: "warning",
        msg: 'File ' + this.editedItem.file + ' does not exist',
        timeout: 5000,
      })
    }
  })
  .catch((err) => {
    console.log('Failed downloadItemConfirm: ', err)
    this.setSnackbar({
      type: "error",
      msg: 'Unable to download file',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.dialogUpload = false
    this.refresh()
  })
},
```

### downloadItemDocuments()

Allows users to download items from the `ss-documents` bucket. First calls the API and requests a `blob` object to be returned. The Express App responds with the requested excel file in `blob` form. The method then converts the blob into a proper excel file and creates a URL link for which the client is able to automatically download the file.


```js
downloadItemDocuments(item) {
  this.editedIndex = this.payload.indexOf(item)
  this.editedItem = Object.assign({}, item)
  HTTP.get('import/file/download/documents', {responseType: 'blob',  params: {filename: this.editedItem.file}} )
  .then((response) => {
    const url = URL.createObjectURL(new Blob([response.data], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    }))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', this.editedItem.file)
    document.body.appendChild(link)
    link.click()
    if (response.status == '200') {
      this.setSnackbar({
        type: "success",
        msg: 'Downloading ' + this.editedItem.file,
        timeout: 5000,
      })
    }

    else if (response.status == '300') {
      this.setSnackbar({
        type: "warning",
        msg: 'File ' + this.editedItem.file + ' does not exist',
        timeout: 5000,
      })
    }
  })
  .catch((err) => {
    console.log('Failed downloadItemConfirmDocuments: ', err)
    this.setSnackbar({
      type: "error",
      msg: 'Unable to download file',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.dialogUploadDocuments = false
    this.refresh()
  })
},
```

### checkFile()

Checks if the file that is about to be uploaded is not already in the requested bucket.

```js
async checkFile() {
  await HTTP.post('import/filecheck', {name: this.filename, bucket: this.bucket})
  .then((response) => {
    this.exists = response.data.value;
  })
  .catch((err) => {
    console.log("filecheck error: ", err)
  })
},
```

### uploadFile()

User uploads the intended file into the `shoppingstories` bucket. However, it first double checks that the user has selected a file to upload, then checks that the file is of the proper type (excel), before finally checking if the file does not already exist in the bucket.

Once all of the above is checked, the file is then uploaded into the `shoppingstories` bucket without first going through the parser and therefore the database.

```js
async uploadFile() {

  // check file
  if (!this.file) {
    this.setSnackbar({
      type: "warning",
      msg: "Please select file to upload",
      timeout: 5000,
    });
    return;
  }

  // checking if file type is correct
  else if (this.file['type'] !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && this.file['type'] !== '.csv' && this.file['type'] !== 'application/vnd.ms-excel') {
    this.setSnackbar({
      type: "warning",
      msg: "That file type is not supported (.xlsx, .xls, .csv files only)",
      timeout: 5000,
    });
    return;
  }

  this.bucket = 'shoppingstories'

  // check if file already exists
  this.filename = this.file.name
  await this.checkFile();

  if (this.exists) {
   this.loader = null
   this.loading = false
   this.setSnackbar({
     type: "error",
     msg: "Error: that file has already been uploaded. Try another.",
     timeout: 5000,
   });
   return;
  }

  // creates new form for API request
  let formData = new FormData();
  formData.set(name, this.file.name)
  formData.append('file', this.file);

  // axios call to proper endpoint
  HTTP.put('/import/file/upload', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
  .then((response) => {
    if (response.status == 200) {
      this.setSnackbar({
        type: "success",
        msg: "File successfully uploaded!",
      })
    }
  })
  .catch(() => {
    this.error = true;
    this.setSnackbar({
      type: "error",
      msg: 'Error! Could not upload file, try again.',
      timeout: 10000,
    });
  })
  .finally(() => {
    this.file = null;
  });
},
```

### uploadFileDocuments()

User uploads the intended file into the `ss-documents` bucket. However, it first double checks that the user has selected a file to upload before finally checking if the file does not already exist in the bucket.

Once all of the above is checked, the file is then uploaded into the `ss-documents` bucket where all users and website visitors will have access to it.

```js
async uploadFileDocuments() {

  // check file
  if (!this.file) {
    this.setSnackbar({
      type: "warning",
      msg: "Please select file to upload",
      timeout: 5000,
    });
    return;
  }

  // check if file already exists
  this.filename = this.file.name
  this.bucket = 'ss-documents'
  await this.checkFile();

  if (this.exists) {
   this.loader = null
   this.loading = false
   this.setSnackbar({
     type: "error",
     msg: "Error: that file has already been uploaded. Try another.",
     timeout: 5000,
   });
   return;
  }

  // creates new form for API request
  let formData = new FormData();
  formData.set(name, this.file.name)
  formData.append('file', this.file);

  // axios call to proper endpoint
  HTTP.put('/import/upload/documents', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
  .then((response) => {
    if (response.status == 200) {
      this.setSnackbar({
        type: "success",
        msg: "File successfully uploaded!",
      })
    }
  })
  .catch(() => {
    this.error = true;
    this.setSnackbar({
      type: "error",
      msg: 'Error! Could not upload file, try again.',
      timeout: 10000,
    });
  })
  .finally(() => {
    this.file = null;
  });
},
```

### closeUpload()

Closes the upload dialog box and refreshes the `shoppingstories` tab.

```js
closeUpload() {
  this.dialogUpload = false
  this.refresh()
},
```

### closeUploadDocuments()

Closes the upload dialog box and refreshes the `ss-documents` tab.

```js
closeUploadDocuments() {
  this.dialogUploadDocuments = false
  this.refresh()
},
```

### uploadItem()

Opens the upload dialog box for the `shoppingstories` tab.

```js
uploadItem() {
  this.dialogUpload = true
},
```

### uploadItemDocuments()

Opens the upload dialog box for the `ss-documents` tab.

```js
uploadItemDocuments() {
  this.dialogUploadDocuments = true
},
```

## Data

```js
data() {
    return {

    // For populating data tables
    tab: null,
    items: [
      'Data Files', 'User Files',
    ],

    bucket: '',
    uploadIcon: 'mdi-cloud-upload-outline',
    file: null,
    value: '',
    isLoading: false,
    loader: null,

    documentPayload: [],
    payload: [],

    search: '',
    loadingButton: false,
    refreshIcon: 'mdi-refresh',

    dialogDelete: false,
    dialogUpload: false,
    dialogDeleteDocuments: false,
    dialogUploadDocuments: false,
    editedItem: {
      file: '',
      time: '',
    },
    defaultItem: {
      file: '',
      time: '',
    },
    editedIndex: -1,
  };
},
```

## Computed

This computed method, `headers()`, will automatically fill in the data table with the contents of the JSON package.

```js
computed: {
  headers() {
    return [
      {
        text: "File Name",
        align: "start",
        sortable: false,
        value: 'file',
      },
      { text: "Uploaded", value: "time" },
      { text: 'Actions', value: 'actions', sortable: false },
    ]
  },
},
```

## Watch

This is where we watch for changes in the dialog boxes. These are needed for when a user clicks anywhere in the screen to get rid of a dialog box.

```js
watch: {
  loader () {
    const l = this.loader
    this.[l] = !this.[l]
    setTimeout(() => (this[l] = false), 1500)
    this.loader = null
  },

  dialogDelete (val) {
    val || this.closeDelete()
  },

  dialogDeleteDocuments (val) {
    val || this.closeDeleteDocuments
  },

  dialogUpload (val) {
    val || this.closeUpload()
  },

  dialogUploadDocuments (val) {
    val || this.closeUploadDocuments()
  },
},
```

## Created

On page load, grab all the information in the buckets for the user.

```js
created() {
  this.isLoading = true;
  this.getPayload()
  this.getDocumentsPayload()
},
```
