# Snackbar.vue

## Description

The snackbar is a popup message that lets users know different messages, such as success messages and errors. It is dynamic to reflect the specific action the user is performing.

## HTML

Makes use of the `<v-snackbar>` component and adds a v-model to make it dynamic.

```html
<v-snackbar
  v-model="model"
  :color="snackbar.type"
  :timeout="snackbar.timeout"
>
  <v-icon
    :dark="snackbar.type !== 'warning'"
    class="mr-2"
    v-text="icon"
  />
  <span
    :class="snackbar.type === 'warning' ? 'black--text' : 'white--text'"
    v-text="snackbar.msg"
  />
</v-snackbar>
```

## Imports

```js
import {
  mapState
} from 'vuex'
```

## Data

```js
data: () => ({
  model: false
}),
```

## Computed

```js
computed: {
  ...mapState(['snackbar']),
  icon () {
    return ICON_MAP[this.snackbar.type] || 'mdi-playlist-check'
  }
},
```

## Watch

Part of what makes this component dynamic, it waits and watches for when it is needed.

```js
watch: {
  snackbar () {
    this.model = true
  }
}
```

## ICON_MAP

List of all the different icons that are used for the varying messages. 

```js
const ICON_MAP = {
  error: 'mdi-alert-octagon',
  info: 'mdi-information',
  success: 'mdi-check-circle',
  warning: 'mdi-alert-circle'
}
```
