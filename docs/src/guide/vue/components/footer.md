# Footer.vue

## Description

Footer.vue is the footer component that is at the base of all views/webpages of this website. It contains links to History Revealed's  social media pages, a method to contact the organization, a link to the About page on the website, a link to a "Meet the Team" page on the website, and general rights reserved text.

## HTML

The footer simply uses `<v-btn>` components that send the user to other pages if clicked. The social media links call methods that will open each social media page in a new tab/window. 

## Methods 

```
methods: {
  openTwitter() {
    window.open("https://twitter.com/History_Reveal", "_blank");
  },
  openFacebook() {
    window.open("https://www.facebook.com/HistoryReveal/", "_blank");

  },
  openInstagram() {
    window.open("https://www.instagram.com/history_reveal/", "_blank");

  },
},
```

Three similar methods are used in this component relating to the social media buttons on the footer. The methods `openTwitter()`, `openFacebook()`, and `openInstagram()` all will open links to their repective website in a new window/tab.