# UserSettings.vue

## Description

`UserSettings.vue` is where users are able to edit their own attributes. All users have access to this page, as it was designed to be a fairly standard user settings page with the same functionality one would expect.

![User Settings](../images/usersettings.png)

## HTML

There are many different components that work together in `UserSettings.vue`. For the visual aspect, most of these are grouped in rows. The first row contains the user's name and acts as a page header.

```html
<!-- Name -->
<v-row class="justify-center mx-auto">
  <p class="display-2 text--primary">
    {{ name }}
  </p>
</v-row>
```
The second row is essentially a row of buttons that allow the user to edit certain attributes, send confirmation emails, and delete their account.

The first button is the `Update Email` button, which allows users to change their emails. Once clicked, users are shown a popup via the `v-dialog` component, where they can input the new email address.

```html
<!-- Update Email -->
<v-dialog
 v-model="clickUpdateEmail"
 max-width="500"
>
 <template v-slot:activator="{ on, attrs }">
  <v-btn
    text
    color="secondary"
    v-bind="attrs"
    v-on="on"
  >
    Update Email
  </v-btn>
 </template>
 <v-card>
   <v-card-title class="headline">
     Update {{ name }}'s Email
   </v-card-title>
   <v-card-text>Please confirm the new email to update with:
   </v-card-text>
   <v-text-field
     v-model="newEmail"
     class="pa-5"
     color="secondary"
     label="Update Email"
     :rules="rules.email"
     required
   >
   </v-text-field>
   <v-card-actions>
     <v-spacer></v-spacer>
     <v-btn
       color="grey darken-1"
       text
       @click="clickUpdateEmail = false"
     >
       Cancel
     </v-btn>
     <v-btn
       text
       color="secondary accent-1"
       @click="changeEmail"
     >
       Change Email
     </v-btn>
   </v-card-actions>
 </v-card>
</v-dialog>
```

When a user creates an account, their email address is already verified via the confirmation link. However, when a user updates their email to a new one, this email then needs to be verified. This `v-dialog` button monitors the status of `verified` and is only visible when the user is not verified.

Once clicked, the user is brought to a form where they can input the verification code sent to them. The code is automatically sent when the user updates their email address. The user also has an option to resend a confirmation email, which invalidates any prior codes the user may have received.

```html
<!-- Verify Email -->
<v-dialog
 v-if="verified === 'false'"
 v-model="clickVerifyEmail"
 max-width="500"
>
 <template v-slot:activator="{ on, attrs }">
  <v-btn
    text
    color="secondary"
    v-bind="attrs"
    v-on="on"
  >
    Verify Email
  </v-btn>
 </template>
 <v-card>
   <v-card-title class="headline">
     Verify {{ name }}'s Email
   </v-card-title>
   <v-card-text>Please enter the verification code sent to your email address on file.
   </v-card-text>
   <v-text-field
     v-model="verificationCode"
     class="pa-5"
     color="secondary"
     label="Enter Verification Code"
     required
   >
   </v-text-field>
   <v-card-actions>
     <v-spacer></v-spacer>
     <v-btn
       color="grey darken-1"
       text
       @click="clickVerifyEmail = false"
     >
       Cancel
     </v-btn>
     <v-btn
      color="grey darken-1"
      text
      @click="resendCode"
     >
      Resend Code
     </v-btn>

     <v-btn
       text
       color="secondary accent-1"
       @click="verifyEmail"
     >
       Verify Email
     </v-btn>
   </v-card-actions>
 </v-card>
</v-dialog>
```

Next, the user is able to update their password. This brings the user to a form via the `v-dialog` component, where the user will input their desired password twice. These passwords must follow the same rules as account creation.

```html
<!-- Change password -->
<v-dialog
 v-model="clickUpdatePassword"
 max-width="500"
>
 <template v-slot:activator="{ on, attrs }">
  <v-btn
    text
    color="secondary"
    v-bind="attrs"
    v-on="on"
  >
    Update Password
  </v-btn>
 </template>
 <v-card>
   <v-card-title class="headline">
     Update {{ name }}'s Password
   </v-card-title>
   <v-card-text>Please confirm your current password:
   </v-card-text>
   <v-text-field
     v-model="oldPassword"
     :type="viewPassword ? 'password' : 'text'"
     :append-icon="viewPassword ? 'mdi-eye-off' : 'mdi-eye'"
     @click:append="viewPassword = !viewPassword"
     class="pa-5"
     color="secondary"
     label="Current Password"
     :rules="rules.password"
     required
     counter
   >
   </v-text-field>

   <v-card-text>Please confirm the new password to update with:
   </v-card-text>
   <v-text-field
     v-model="newPassword"
     :type="viewPassword ? 'password' : 'text'"
     :append-icon="viewPassword ? 'mdi-eye-off' : 'mdi-eye'"
     @click:append="viewPassword = !viewPassword"
     class="pa-5"
     color="secondary"
     label="New Password"
     :rules="rules.password"
     required
     counter
   >
   </v-text-field>
   <v-text-field
     v-model="confirmPassword"
     :type="viewPassword ? 'password' : 'text'"
     :append-icon="viewPassword ? 'mdi-eye-off' : 'mdi-eye'"
     @click:append="viewPassword = !viewPassword"
     class="pa-5"
     color="secondary"
     label="Confirm Password"
     :rules="[newPassword === confirmPassword || 'Password must match']"
     required
     counter
   >
   </v-text-field>
   <v-card-actions>
     <v-spacer></v-spacer>
     <v-btn
       color="grey darken-1"
       text
       @click="clickUpdatePassword = false"
     >
       Cancel
     </v-btn>
     <v-btn
       text
       color="secondary accent-1"
       @click="changePassword"
     >
       Change Password
     </v-btn>
   </v-card-actions>
 </v-card>
</v-dialog>
```

Finally, users have the option to delete their account. This is permanent and cannot be undone. There will be a popup via the `v-dialog` component that will alert the user of this and confirm that they want to delete their account.

```html
<!-- Delete Account -->
<v-dialog
 v-model="dialog"
 max-width="290"
>
 <template v-slot:activator="{ on, attrs }">
  <v-btn
    text
    color="red"

    v-bind="attrs"
    v-on="on"
  >
    Delete My Account
  </v-btn>
 </template>
 <v-card>
   <v-card-title class="headline">
     Delete Account
   </v-card-title>
   <v-card-text>Are you sure you want to delete your account? This action is permanent.
   </v-card-text>
   <v-card-actions>
     <v-spacer></v-spacer>
     <v-btn
       color="secondary darken-1"
       text
       @click="dialog = false"
     >
       Close
     </v-btn>
     <v-btn
       color="red darken-1"
       text
       @click="deleteUser"
     >
       Delete
     </v-btn>
   </v-card-actions>
 </v-card>
</v-dialog>
```

Each row after the buttons will show a user attribute. Here is the user's username.

```html
<!-- Username -->
<v-row class="justify-center pt-3">
 <p><b>Username: </b>{{ username}} </p>
</v-row>
```

Next is the user's email address. This is followed by an icon whose type and color will change depending on the verification status of the email address connected to the user.

```html
<!-- Email & Verified -->
<v-row v-if="verified === 'true'" class="justify-center">
  <v-col class="justify-center pb-0 pt-0" md="auto" pb="0">
    <p><b>Email: </b>{{ email }}</p>
  </v-col>
  <v-col class="justify-center pb-0 pt-0" md="auto" pb="0">
    <p style="color:green;">Verified
    <v-icon color="green" style="pb-1" medium>{{ icon }} </v-icon>
    </p>
  </v-col>
</v-row>
<v-row v-else class="justify-center">
  <v-col class="justify-center pb-0 pt-0" md="auto" pb="0">
   <p><b>Email: </b>{{ email }}</p>
  </v-col>
  <v-col class="justify-center pb-0 pt-0" md="auto" pb="0">
   <p style="color:red"> Not Verified
   <v-icon color="red" style="pb-1" medium>{{ icon }} </v-icon>
   </p>
  </v-col>
</v-row>
```

Finally, we grab the user's groups. Currently, the only groups a user should belong to are 'Researcher'. Admin, of course, has her own group.

```html
<!-- Groups -->
<v-row v-if="showGroup" class="justify-center">
 <p><b>Groups: </b>{{ groups }} </p>
</v-row>
```

## Imports

```js
import { HTTP } from '../services/http-common'
import { mapActions, mapMutations, mapState } from "vuex";
```

## Methods

First we grab some necessary functions and variables from the store.

```js
...mapActions("cognito", ["changePassword", 'resendConfirmation'], ["signOut"], ["resendConfirmation"]),
...mapMutations(["setSnackbar", "setEmail", "setPassword", "setUsername",]),
...mapState(['email','username']),
```

`fetchUserInfo()` is not a misnomer and actually fetches the user information. This is done through a series of API calls, the first to the Express App. The Express API will then call AWS Cognito's API. As such, the needed information is the token associated with the user and their current session.

Once the information is obtained, we update the values for the data field.

```js
async fetchUserInfo() {
  // grab the group from Cognito
  const getToken = await this.$store.dispatch('cognito/fetchJwtToken')
  await HTTP.patch('cognito/user/self', {token: getToken.toString()})
  .then((response) => {
    this.username = response.data.Username;
    this.status = response.data.UserStatus;
    this.verified = response.data.UserAttributes[1].Value
    this.email = response.data.UserAttributes[3].Value
    this.name = response.data.UserAttributes[2].Value

    // set verified
    if (this.verified == 'true') {
      this.icon = 'mdi-check'
    }
    else {
      this.icon = 'mdi-alert-circle'
    }

    const groupID = this.$store.getters['cognito/userGroups'];
    if (groupID[0]) {
      this.showGroup = true;
      this.groups = groupID[0]
    }
  })
  .catch((err) => {
    console.log(err)
  })
},
```

The function `changeEmail()` also grabs the user's token associated with their current session and throws that into an API call to the Express App. It also grabs the new email from the user input. The function `fetchUserInfo()` is called afterwards to update all user information in the tab.

```js
// Updates the email of the selected user
// to the new email provided by Admin
async changeEmail() {
  const getToken = await this.$store.dispatch('cognito/fetchJwtToken')
  HTTP.patch('cognito/user/attributes',
  {
    username: this.selectedUsername,
    token: getToken.toString(),
    attributes: [
    {
      Name: 'email',
      Value: this.newEmail
    }]
  })
  .then(() => {
    this.email = this.newEmail
    this.setSnackbar({
      type: "success",
      msg: 'Successfully updated email to ' + this.newEmail,
      timeout: 5000,
    })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not update email. Try again later',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.clickUpdateEmail = false
    this.fetchUserInfo();
  })
},
```

The `changePassword()` is similar to `changeEmail()` in that we first grab the token associated with the user's session, then call the Express App with the updated information. In this case, that would be the new password for the user.

```js
// Updates password
async changePassword() {
  const getToken = await this.$store.dispatch('cognito/fetchJwtToken')
  HTTP.patch('cognito/user/password',
  {
    previous: this.oldPassword,
    token: getToken.toString(),
    proposed: this.newPassword
  })
  .then(() => {
    this.setSnackbar({
      type: "success",
      msg: 'Successfully changed password!',
      timeout: 5000,
    })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not update password. Try again.',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.clickUpdatePassword = false
    this.fetchUserInfo();
  })
},
```

`verifyEmail()` grabs the user's token and their email address, as well as the code from the user input, and ships that information off to the Express App.

```js
async verifyEmail() {
  const token = await this.$store.dispatch('cognito/fetchJwtToken')
  const email = this.email
  const code = this.verificationCode

  HTTP.patch('cognito/verify/email', {token: token.toString(), email: email, code: code})
  .then(() => {
    this.setSnackbar({
      type: "success",
      msg: 'Successfully verified email!',
      timeout: 5000,
    })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not verify code. Try again.',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.clickVerifyEmail = false
    this.fetchUserInfo();
  })
},
```

Sometimes an email verification code may be lost or expired. As such, users can request a new one. Here, we grab the user's token and then send that request to the Express App.

```js
async resendCode() {
  const token = await this.$store.dispatch('cognito/fetchJwtToken')
  HTTP.patch('cognito/resend', {token: token.toString()})
  .then(() => {
    this.setSnackbar({
      type: "success",
      msg: 'Successfully resent email verification code!',
      timeout: 5000,
    })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not send code. Try again.',
      timeout: 5000,
    })
  })
},
```

## Data

```js
data: () => ({
  user: "",
  name: '',
  status: '',
  verified: '',
  groups: '',
  createDate: '',
  icon: '',
  refreshIcon: 'mdi-refresh',
  loadingButton: false,
  dialog: false,
  newEmail: '',
  clickUpdateEmail: false,
  clickVerifyEmail: false,
  verificationCode: '',
  showGroup: false,
  clickUpdatePassword: false,
  confirmPassword: '',
  newPassword: '',
  oldPassword: '',
  viewPassword: true,
  confirmEmail: '',
  isConfirmed: true,
  reveal: '',

  // Rules for each element
  rules: {
    username: [
      (v) => !!v || "Username is required",
      (v) =>
        /^[A-Za-z]+$/.test(v) ||
        "Name can't contain numbers or special chars.", // numbers are actually okay but this is fine for now
    ],
    email: [
      (v) => !!v || "E-mail is required",
      (v) => /.+@.+/.test(v) || "E-mail must be valid",
    ],
    password: [
      (v) => !!v || "Password is required",
      (v) => /(?=.*[0-9])/.test(v) || "Password must contain a number", // Checking for at least one number
      (v) =>
        /(?=.*[A-Z])/.test(v) || "Password must contain a uppercase letter", // Checking for at least one uppercase letter
      (v) =>
        /(?=.*[a-z])/.test(v) || "Password must contain a lowercase letter", // Checking for at least one lowercase letter
      (v) =>
        /(?=.*[@#$%^&*!])/.test(v) ||
        "Password must contain a special character", // Checking for at least one special character
      (v) => v.length > 7 || "Password must have 8 or more characters",
    ],
  },
}),
```

## Created

```js
created() {
  this.fetchUserInfo();
}
```
