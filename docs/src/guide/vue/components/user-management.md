# UserManagement.vue

## Description

`UserManagement.vue` is another component that could potentially be split up into several other components.

This component is built for the Admin to monitor, update, and view all of the users that have created an account in the website. It is essentially an AWS Cognito Management console that lives on the website.

The Admin is able to see all Users and all Researchers via the tabs at the top. The Admin can also click a user and a new card will appear with all of that user's information. This card will also have functionality to change certain attributes about the user, such as adding or removing the user from the Researcher group.

::: tip
The Admin, or anyone besides the user, will not have access to any user password. This is done for the safety and integrity of the user's account.
:::

![User Management](../images/usermanagement.png)

## HTML

The HTML for this component is admittedly bloated. However, we've tried to break it down into smaller chunks.

First up is the `v-tabs` component, which houses all of the other components for the Admin to use. Currently, the only two tabs are `Users` and `Groups`.

```html
<!-- Tabs -->
<v-tabs
  v-model="tab"
  align-with-title
  background-color="secondary"
  dark
>
  <v-tabs-slider color="primary"></v-tabs-slider>
  <v-tab
    v-for="item in items"
    :key="item"
    @click="reveal = false"
  >
    {{ item }}
  </v-tab>
</v-tabs>
```

Here is a brief except of what holds the individual tab items together.

```html
<v-tabs-items v-model="tab">
```

Each `v-tab-item` component holds a `v-data-table` component that forms the majority of the tab. This is filled with user info pulled from the Express App.

Included is the `v-text-field` component which allows the Admin to search for individual users.

```html
<v-tab-item>
  <v-card class="mt-0 mx-auto pa-10" width="75vw" >
  <v-card-title> All Users
  <v-btn
    @click="refreshUser"
    :loading=loadingButton
    text
  >
    <v-icon color="secondary"  style="pb-1" medium>{{ refreshIcon }} </v-icon>
  </v-btn>
  <v-spacer></v-spacer>
  <v-text-field
    v-model="search"
    label="Search (UPPER CASE ONLY)"
    class="mx-4"
    color="secondary"
    single-line
    hide-details
    append-icon="mdi-magnify"
  ></v-text-field>
  </v-card-title>
  <v-card-subtitle> Edit User Attributes and Groups </v-card-subtitle>
```

The second section of the tab item is the actual `v-data-table` where the user information is shown in table format. The first one uses the `headersUsers` template and the `userPayload` for the contents. It also has customizable filter options. Currently it is set to `filterOnlyCapsText()`.

Notably, there is a function `showUser()` that activates the card that holds all of the selected user's information.

Lastly, there's the `v-progress-linear` component that helps indicate to the user that the page is loading.

```html
<!-- All users data table -->
    <v-container class="pa-1" width="75vw">
      <v-data-table
        :headers="headersUser"
        :items="userPayload"
        item-key="name"
        class="elevation-1"
        :search="search"
        :custom-filter="filterOnlyCapsText"
        loading="isLoading"
        loading-text="Loading... Please wait"
        @click:row="showUser"
      >
      <v-progress-linear
        v-show="isLoading"
        slot="progress"
        color="secondary"
        indeterminate
      ></v-progress-linear>
      </v-data-table>
    </v-container>
  </v-card>
</v-tab-item>
```

The other `v-tab-item` component is very similar to the previous but is specific to the Researcher group in Cognito.

```html
<v-tab-item>
  <!-- Researcher Group -->
  <v-card class="mt-0 mx-auto pa-10" width="75vw">
  <v-card-title> Researcher Group
    <v-btn
      @click="refreshResearcher"
      text
      :loading="loadingButton"
    >
      <v-icon color="secondary" style="pb-1" medium> {{ refreshIcon }} </v-icon>
    </v-btn>
    <v-spacer></v-spacer>
    <!-- Researcher Search Field -->
    <v-text-field
      v-model="search"
      label="Search (UPPER CASE ONLY)"
      class="mx-4"
      single-line
      hide-details
      append-icon="mdi-magnify"
    ></v-text-field>
  </v-card-title>
  <v-card-subtitle> Edit User Attributes and Groups </v-card-subtitle>
    <!-- Researcher Data Table -->
    <v-container class="pa-1" width="75vw">
      <v-data-table
        :headers="headersResearcher"
        :items="researcherPayload"
        item-key="name"
        class="elevation-1"
        :search="search"
        :custom-filter="filterOnlyCapsText"
        loading="isLoading"
        loading-text="Loading... Please wait"
        @click:row="showResearcher"
      >
      <v-progress-linear
        v-show="isLoading"
        slot="progress"
        color="secondary"
        indeterminate
      ></v-progress-linear>
      </v-data-table>
    </v-container>
  </v-card>
</v-tab-item>
```

The `v-expand-transition` is an important piece that adds the needed functionality to the `UserManagement.vue` component.

When the Admin clicks on an individual user on either of the tabs' data tables, this component expands out a `v-card` component that holds all that functionality.

This includes changing users' emails, adding and removing users to the Researcher group, and deleting users from Cognito. It also shows the user's attributes similar to `UserSettings.vue` but with more information such as the date the user joined and so on.

However, this component is quite bloated and is the most likely candidate to be separated into its own `.vue` file.

```html
<!-- Click User Transition Card -->
<v-expand-transition>
  <v-card
    v-if="reveal"
    class="transition-fast-in-fast-out v-card--reveal"
    style="height:100%"
  >
    <v-card-text class="pb-3" >
     <v-row class="justify-center mx-auto">
       <p class="display-2 text--primary">
         {{ selectedName }}
       </p>
     </v-row>
     <v-row class="justify-center mx-auto">
       <v-dialog
        v-model="clickUpdateEmail"
        max-width="500"
       >
        <template v-slot:activator="{ on, attrs }">
         <v-btn
           text
           color="secondary"
           v-bind="attrs"
           v-on="on"
         >
           Update Email
         </v-btn>
        </template>
        <v-card>
          <v-card-title class="headline">
            Update {{ selectedName }}'s Email
          </v-card-title>
          <v-card-text>Please confirm the new email to update with:
          </v-card-text>
          <v-text-field
            v-model="newEmail"
            class="pa-5"
            color="secondary"
            label="Update Email"
            :rules="rules.email"
            required
          >
          </v-text-field>
          <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn
              color="grey darken-1"
              text
              @click="clickUpdateEmail = false"
            >
              Cancel
            </v-btn>
            <v-btn
              text
              color="secondary accent-1"
              @click="changeEmail"
            >
              Change Email
            </v-btn>
          </v-card-actions>
        </v-card>
       </v-dialog>
       <v-btn
         v-if="selectedStatusBoolean === 'false'"
         text
         color="secondary accent-1"
         @click="confirmUser"
       >
         Confirm User
       </v-btn>
       <v-btn
         v-if="selectedGroups == 'Researcher'"
         text
         color="secondary accent-1"
         @click="removeResearcher"
       >
         Remove Researcher
       </v-btn>
       <v-btn
         v-else
         text
         color="secondary accent-1"
         @click="addResearcher"
       >
         Add Researcher
       </v-btn>
       <v-dialog
        v-model="dialog"

        max-width="290"
       >
        <template v-slot:activator="{ on, attrs }">
         <v-btn
           text
           color="red"

           v-bind="attrs"
           v-on="on"
         >
           Delete User
         </v-btn>
        </template>
        <v-card>
          <v-card-title class="headline">
            Delete Account
          </v-card-title>
          <v-card-text>Are you sure you want to delete this user's account? This action is permanent.
          </v-card-text>
          <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn
              color="secondary darken-1"
              text
              @click="dialog = false"
            >
              Close
            </v-btn>
            <v-btn
              color="red darken-1"
              text
              @click="deleteUser"
            >
              Delete
            </v-btn>
          </v-card-actions>
        </v-card>
       </v-dialog>
     </v-row>
     <v-row class="justify-center pt-3">
      <p><b>Username: </b>{{ selectedUsername}} </p>
     </v-row>
     <v-row v-if="selectedVerified === 'true'" class="justify-center">
       <v-col class="justify-center pb-0 pt-0" md="auto" pb="0">
         <p><b>Email: </b>{{ selectedEmail }}</p>
       </v-col>
       <v-col class="justify-center pb-0 pt-0" md="auto" pb="0">
         <p style="color:green;">Verified
         <v-icon color="green" style="pb-1" medium>{{ icon }} </v-icon>
         </p>
       </v-col>
     </v-row>
     <v-row v-else class="justify-center">
       <v-col class="justify-center pb-0 pt-0" md="auto" pb="0">
        <p><b>Email: </b>{{ selectedEmail }}</p>
       </v-col>
       <v-col class="justify-center pb-0 pt-0" md="auto" pb="0">
        <p style="color:red"> Not Verified
        <v-icon color="red" style="pb-1" medium>{{ icon }} </v-icon>
        </p>
       </v-col>
     </v-row>
     <v-row class="justify-center">
      <p><b>Joined: </b>{{ selectedCreateDate }} </p>
     </v-row>
     <v-row class="justify-center">
      <p><b>Confirmation Status: </b>{{ selectedStatus }} </p>
     </v-row>
     <v-row class="justify-center">
      <p><b>Groups: </b>{{ selectedGroups }} </p>
     </v-row>
    </v-card-text>
    <v-card-actions class="pt-0 justify-center">
     <v-btn
       text
       color="secondary accent-1"
       @click="reveal = false"
     >
       Close
     </v-btn>
    </v-card-actions>
  </v-card>
</v-expand-transition>
```

## Style

```css
.v-card--reveal {
justify-content: center;
bottom: 0;
opacity: 1 !important;
position: absolute;
width: 100%;
}
```

## Imports

```js
import { HTTP } from '../services/http-common'
import { mapMutations } from "vuex";
```

## Methods

### Snackbar
Handy Snackbar component!

```js
...mapMutations(["setSnackbar"]),
```

### deleteUser()

The `deleteUser()` function does exactly what the name suggests. It is initiated when the Admin clicks the confirm button on the corresponding `v-dialog` component. This will call the Express App and tell it to delete the selected user.

If successful, the expanded card will disappear and the data tables will be reloaded with all the current users and their information.

```js
// deletes selected user from Cognito user pool
deleteUser() {
  HTTP.delete('cognito/user/delete', {params: {username: this.selectedUsername}})
  .then(() => {
    this.setSnackbar({
      type: "success",
      msg: 'Successfully deleted ' + this.selectedName,
      timeout: 5000,
    })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: Could not delete ' + this.selectedName,
      timeout: 5000,
    })
  })
  .finally(() => {
    this.dialog = false;
    this.getUserPayload()
    this.reveal = false
  })
},
```

### confirmUser()

Similarly, `confirmUser()` will grab the user's username and tell the Express App to confirm this user in Cognito. Afterwards, it will refresh both pages to reflect any changes.

```js
// Confirms user account in Cognito
confirmUser() {
  HTTP.patch('cognito/user/confirm', {username: this.selectedUsername})
  .then(() => {
    this.selectedStatus = 'CONFIRMED'
    this.selectedStatusBoolean = 'true'
    this.setSnackbar({
      type: "success",
      msg: 'Successfully confirmed ' + this.selectedName,
      timeout: 5000,
    })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not confirm ' + this.selectedName,
      timeout: 5000,
    })
  })
  .finally(() => {
    this.refreshUser()
    this.refreshResearcher()
  })
},
```

### removeResearcher()

`removeResearcher()` will take the selected user as well as the group `Researcher` parameters and send that data off to the Express App. Once the request is processed, the data tables will be refreshed to update the new values.

It will also close the information card as that user will no longer be in the `Groups` tab.

```js
// Removes selected user from Researcher group
removeResearcher() {
  HTTP.patch('cognito/user/remove', {username: this.selectedUsername, group: 'Researcher'})
  .then(() => {
    this.setSnackbar({
      type: "success",
      msg: 'Successfully removed ' + this.selectedName + ' from Research Group',
      timeout: 5000,
    })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not removed ' + this.selectedName + ' from Research Group',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.getUserPayload()
    this.getResearcherPayload()
    this.reveal = false
  })
},
```

### changeEmail()

The `changeEmail()` function grabs the username of the selected user as well as the new email that was provided by the Admin. It will then call the Express App with that necessary information. After the request is processed, the function will update the data tables to reflect any changes.

```js
// Updates the email of the selected user
// to the new email provided by Admin
changeEmail() {
  HTTP.patch('cognito/user/edit',
  {
    username: this.selectedUsername,
    attributes: [
    {
      Name: 'email',
      Value: this.newEmail
    }]
  })
  .then(() => {
    this.setSnackbar({
      type: "success",
      msg: 'Successfully updated email for ' + this.selectedName,
      timeout: 5000,
    })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not update email for ' + this.selectedName,
      timeout: 5000,
    })
  })
  .finally(() => {
    this.clickUpdateEmail = false
    this.refreshUser()
    this.refreshResearcher()
  })
},
```

### addResearcher()

This function adds users to the Researcher group. The user will then have access to all of the features available for Molly's team, this component notwithstanding.

::: tip
Users will need to log out and log back in after being added to the Researcher group in order to see any changes to their Dashboard.
:::

Similar to other functions here, `addResearcher()` grabs the username and the group `Researcher` and sends this information off to the Express App. After the request is processed, the data tables will be refreshed.

```js
// Adds user to Researcher group
// eventually more groups can be added
addResearcher() {
  HTTP.patch('cognito/user/join', {username: this.selectedUsername, group: 'Researcher'})
  .then(() => {
    this.selectedGroups = 'Researcher'
    this.setSnackbar({
      type: "success",
      msg: 'Successfully added ' + this.selectedName + ' to the Reseacher group',
      timeout: 5000,
    })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not add ' + this.selectedName + ' to Research Group',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.getUserPayload()
    this.getResearcherPayload()
  })
},
```

### showResearcher()

The `showResearcher()` function alerts the expansion component that a user in the `Groups` tab has been clicked on. It will then populate the related data fields with with the selected user's information.

```js
// Shows card with user details
// (Researcher group only)
showResearcher(value) {

  // get values
  this.selectedName = value.Attributes[2].Value
  this.selectedUsername = value.Username
  this.selectedEmail = value.Attributes[3].Value
  this.selectedStatus = value.UserStatus
  this.selectedVerified = value.Attributes[1].Value
  this.selectedCreateDate = value.UserCreateDate

  // grab confirmed status
  if (this.selectedStatus == 'UNCONFIRMED') {
    this.selectedStatusBoolean = false;
  }

  else {
    this.selectedStatusBoolean = true;
  }

  // set verified
  if (this.selectedVerified == 'true') {
    this.icon = 'mdi-check'
  }
  else {
    this.icon = 'alert-circle'
  }

  // GETs the user's groups
  HTTP.get('cognito/user/groups', {params: {username: this.selectedUsername}})
  .then((response) => {
    if (response.data.Groups.length > 0) {
      this.selectedGroups = response.data.Groups[0].GroupName
    }
    else {
      this.selectedGroups = 'None'
    }
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not retrieve groups for ' + this.selectedName,
      timeout: 5000,
    })
  })
  .finally(() => {
    // reveal card
    this.reveal = true;
  })
},
```

### showUser()

Similarly, the `showUser()` function alerts the expansion component that a user in the `Users` tab has been clicked on. It will then populate the related data fields with with the selected user's information.

```js
// Shows card with user details
// (All Users only)
showUser(value) {
  this.selectedName = value.Attributes[0].Value
  this.selectedEmail = value.Attributes[1].Value
  this.selectedStatus = value.UserStatus
  this.selectedUsername = value.Username
  this.selectedCreateDate = value.UserCreateDate

  // grab confirmed status
  if (this.selectedStatus == 'UNCONFIRMED') {
    this.selectedStatusBoolean = false;
  }

  else {
    this.selectedStatusBoolean = true;
  }

  // GETs the value for email verification of selected user
  HTTP.get('cognito/user', {params: {username: this.selectedUsername}})
  .then((response) => {
    this.selectedVerified = response.data.UserAttributes[1].Value
    if (this.selectedVerified == 'true') {
      this.icon = 'mdi-check'
    }
    else {
      this.icon = 'mdi-alert-circle'
    }
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not retrieve email verification status for ' + this.selectedName,
      timeout: 5000,
    })
  })

  // GETs the user's groups
  HTTP.get('cognito/user/groups', {params: {username: this.selectedUsername}})
  .then((response) => {
    if (response.data.Groups.length > 0) {
      this.selectedGroups = response.data.Groups[0].GroupName
    }
    else {
      this.selectedGroups = 'None'
    }
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not retrieve groups for ' + this.selectedName,
      timeout: 5000,
    })
  })
  .finally(() => {
    this.reveal = true;
  })
},
```

### getUserPayload()

`getUserPayload()` function simply grabs all of the users currently in Cognito and fills the corresponding data table component with their information.

```js
// Grabs all users and info for
// populating Users data table
getUserPayload() {
  HTTP.get('cognito/users')
  .then((response) => {
    this.userPayload = response.data.Users
    this.userPayload.forEach(el => {
      const timestamp = new Date(el.UserCreateDate)
      el.UserCreateDate = timestamp.toLocaleString()
      })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not retrieve user data',
      timeout: 5000,
    })
  })
},
```

### getResearcherPayload()

`getResearcherPayload()` function simply grabs all of the users in the `Researcher` group in Cognito and fills the corresponding data table component with their information.

```js
// Grabs all users in the Researcher group
// for populating Researcher data table
getResearcherPayload() {
  HTTP.get('cognito/groups/researcher')
  .then((response) => {
    this.researcherPayload = response.data.Users
    this.researcherPayload.forEach(el => {
      const timestamp = new Date(el.UserCreateDate)
      el.UserCreateDate = timestamp.toLocaleString()
      })
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: 'Error: could not user data',
      timeout: 5000,
    })
  })
  .finally(() => {
    this.isLoading = false;
  })
},
```

### refreshUser()

`refreshUser()` simply calls the other related fetch functions, which updates all of the user information seen on the screen.

```js
// Refreshes data table for user
// called by refresh button
refreshUser() {
  this.loadingButton = true;
  this.getUserPayload()

  // to alert user of action
  setTimeout(() => {
    this.loadingButton = false;
  }, 500)
},
```

### refreshResearcher()

`refreshResearcher()` simply calls the other related fetch functions, which updates all of the user information seen on the screen.

```js
// Refreshes data table for researcher
// called by refresh button
refreshResearcher() {
  this.loadingButton = true;
  this.getResearcherPayload()

  // to alert user of action
  setTimeout(() => {
    this.loadingButton = false;
  }, 500)
},
```

### filterOnlyCapsText()

`filterOnlyCapsText` is one example of a myriad of ways one could filter through the data tables. All caps was chosen as otherwise users would have to input a name or a data field exactly as it is stored. Ex.) typing `John Doe` instead of simply `john doe` or `JOHN DOE`.

All caps was an arbitrary choice and can be changed to lowercase search or any other filter of choice.

```js
// Filter for results, set to ALL CAPS
filterOnlyCapsText (value, search) {
  return value != null &&
  search != null &&
  typeof value === 'string' &&
  value.toString().toLocaleUpperCase().indexOf(search) !== -1
},
```

## Data

```js
data: () => ({
  selectedName: '',
  selectedUsername: '',
  selectedEmail: '',
  selectedStatus: '',
  selectedVerified: '',
  selectedCreateDate: '',
  selectedStatusBoolean: 'false',
  selectedGroups: '',
  icon: '',
  reveal: false,
  refreshIcon: 'mdi-refresh',
  loadingButton: false,
  dialog: false,
  newEmail: '',
  clickUpdateEmail: false,

  // Rules for each element
  rules: {
    username: [
      (v) => !!v || "Username is required",
      (v) =>
        /^[A-Za-z]+$/.test(v) ||
        "Name can't contain numbers or special chars.", // numbers are actually okay but this is fine for now
    ],
    email: [
      (v) => !!v || "E-mail is required",
      (v) => /.+@.+/.test(v) || "E-mail must be valid",
    ],
    password: [
      (v) => !!v || "Password is required",
      (v) => /(?=.*[0-9])/.test(v) || "Password must contain a number", // Checking for at least one number
      (v) =>
        /(?=.*[A-Z])/.test(v) || "Password must contain a uppercase letter", // Checking for at least one uppercase letter
      (v) =>
        /(?=.*[a-z])/.test(v) || "Password must contain a lowercase letter", // Checking for at least one lowercase letter
      (v) =>
        /(?=.*[@#$%^&*!])/.test(v) ||
        "Password must contain a special character", // Checking for at least one special character
      (v) => v.length > 7 || "Password must have 8 or more characters",
    ],
  },

  // For populating data tables
  tab: null,
  items: [
    'users', 'groups',
  ],

  // Determines the parser to use
  selectVal: undefined,

  // For retrieving data
  userPayload: [],
  researcherPayload: [],
  isLoading: false,

  // for search filter
  search: '',
}),
```

## Computed

Computed functions are similar to Methods but with the addition of caching values. Here, we create headers for our data tables, which have a name and a value. These values are pulled from the respective payloads, one for User and one for Researcher.

You can read more about computed functions [here](https://vuejs.org/v2/guide/computed.html).

```js
// Headers for data tables
computed: {
  headersUser () {

    return [
      {
        // First name Last Name
        text: 'Name',
        align: 'start',
        sortable: true,
        value: 'Attributes[0].Value',
      },
      { text: 'Username', value: 'Username' },
      { text: 'Email', value: 'Attributes[1].Value' },
      { text: 'Status', value: 'UserStatus' },
      { text: 'Created', value: 'UserCreateDate'}
    ]
  },

  headersResearcher () {
    return [
      {
        // Name
        text: 'Name',
        align: 'start',
        sortable: true,
        value: 'Attributes[2].Value'
      },
      { text: 'Username', value: 'Username' },
      { text: 'Email', value: 'Attributes[3].Value' },
      { text: 'Verified', value: 'Attributes[1].Value' },
      { text: 'Status', value: 'UserStatus' },
      { text: 'Created', value: 'UserCreateDate'}
    ]
  }
},
```

## Created

When this component is first initialized, we populate the data tables with the respective user information. While this is processing, we start the loading progress bar component, which finishes once both function calls are complete.

```js
created() {
  this.isLoading = true;
  this.getUserPayload();
  this.getResearcherPayload();
}
```
