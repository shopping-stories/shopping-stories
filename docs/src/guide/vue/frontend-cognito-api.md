# Cognito API

For a complete list of Actions/Getters that are included in this module, check out the official [guide](https://cognito.vuetifyjs.com/). We will not cover everything here, just what is currently used in the project.

## User Sign-in

Users can sign into the website, and therefore sign into Cognito, but simply clicking on the sign-in link on the AppBar (TODO: include link here for AppBar documentation).

### Signin.vue

In the code, that involves pushing users to the `Signin.vue`, found in `/web/src/views`. The `Signin.vue` uses two crucial components: `Email.vue` & `Password.vue`. These can be found in `/web/src/components`. This is reflective of the fact that the Cognito User Pool currently only captures users email and users passwords in order to authenticate.

Users can be directed from `Signin.vue` to `Signup.vue`, where they can complete the register form to create an account.

### Email.vue

This component first grabs the user inputted email address in the template portion

```
<v-text-field
  v-model="internalValue"
  autofocus
  label="Email or phone"
  name="email"
/>
```

Then the component imports `mapMutations` from Vuex in the script portion

```js
import {
  mapMutations
} from 'vuex'
```

In the export section of the script, we set the email value

```js
export default {
  computed: {
    internalValue: {
      get() {
        return this.$store.state.email
      },
      set (val) {
        this.setEmail(val)
      }
    }
  },

  methods {
    ...mapMutations(['setEmail'])
  }
}
```

### Password.vue

This component grabs the user-input password. Showing the password can be toggled

```
<v-text-field
  v-model="internalValue"
  :append-icon="show ? 'mdi-eye' : 'mdi-eye-off'"
  :type="show ? 'text' : 'password'"
  class="mb-3"
  label="Password"
  name="password"
  @click:append="show = !show"
/>
```

The user can then click the submit action button, which will then authenticate the credentials

```
<v-btn
  :disabled="!internalValue"
  :loading="isLoading"
  class="text-capitalize ma-0"
  color="primary"
  depressed
  @click="submit"
>
  Next
</v-btn>
```

The script portion utilizes mapActions, mapMutations, and mapState from Vuex

```js
import {
  mapActions,
  mapMutations,
  mapState
} from 'vuex'
```

The script then grabs the user inputs and updates values

```js
computed: {
  ...mapState([
    'email',
    'password',
    'isLoading'
  ]),
  internalValue: {
    get () {
      return this.$store.state.password
    },
    set (val) {
      this.setPassword(val)
    }
  }
},
```

Then the script performs the submit action

```js
methods: {
  ...mapActions('cognito', ['signInUser']),
  ...mapMutations([
    'setEmail',
    'setPassword',
    'setIsLoading',
    'setSnackbar'
  ]),
  submit () {
    this.hasError = false
    this.setIsLoading(true)
    this.signInUser({
      username: this.email,
      password: this.password
    })
      .then(() => {
        this.setSnackbar({
          type: 'success',
          msg: `Successfully signed in user ${this.email}`
        })

        this.setEmail('')
        this.$router.push('/authenticated')
      })
      .catch(res => {
        this.setSnackbar({
          type: 'error',
          msg: res
        })
      })
      .finally(() => {
        this.setIsLoading(false)
        this.setPassword('')
      })
  }
}
```

`Password.vue` updates the `Snackbar.vue` component, which pushes needed messages to the user based on the `mapState`

### Authenticated.vue

After users sign in, they are directed to `Authenticated.vue`, found in `/web/src/views`. Here, we verify that the user has successfully been authenticated in Cognito

The action is in the script section. Here, we utilize `mapState` and `mapActions` from Vuex

```js
import {
  mapState,
  mapActions
} from 'vuex'
```

The authentication happens in the `export default {}` section. Here we utilize the `beforeRouteEnter` function, and grab the user session. If there is an error and the user has not been successfully signed in, the user is redirected to the sign in page before given access to the authenticated page.

```js
export default {
  beforeRouteEnter (to, from, next) {
    next(vm => {
      if (!vm.$store.getters['cognito/isLoggedIn']) {
        vm.$router.push('/auth/signin')
      }
    })
  },
}
```

## User Signup

`Signup.vue` is more intensive than `Signin.vue`, which is to say that there is more going on under the hood. It grabs all the necessary information from the user and sends it to the designated Cognito User Pool.

Currently, users are required to enter:

first & last names
```
<VTextField
  v-model="firstName"
  :rules="[rules.required('Enter first name')]"
  label="First name"
/>
```

```
<VTextField
  v-model="lastName"
  :rules="[rules.required('Enter last name')]"
  label="Last name"
/>
```

email address (will use for authentication & verification)
```
<VTextField
  v-model="email"
  :rules="[rules.required('Enter your email address')]"
  label="Email"
  hint="You can use letters, numbers & periods"
  name="email"
  persistent-hint
/>
```

and password, which must satisfy the security requirements (users must also confirm their passwords)
```
<VTextField
  v-model="password"
  :append-icon="show ? 'mdi-eye' : 'mdi-eye-off'"
  :rules="[rules.required('Enter a password')]"
  :type="show ? 'text' : 'password'"
  label="Password"
  @click:append="show = !show"
/>
```

```
<VTextField
  v-model="confirmPassword"
  :append-icon="showConfirm ? 'mdi-eye' : 'mdi-eye-off'"
  :rules="[rules.required('Confirm you password'), rules.confirm]"
  :type="showConfirm ? 'text' : 'password'"
  label="Confirm password"
  @click:append="showConfirm = !showConfirm"
/>
```

Once the necessary information is filled out, users can click the `Next` button to move forward
```
<BaseBtn
  :disabled="!form"
  :loading="isLoading"
  prominent
  @click="submit"
>
  Next
</BaseBtn>
```

The script portion of this component utilizes `mapActions` and `mapMutations` from Vuex

```js
import {
  mapActions,
  mapMutations
} from 'vuex'
```

In the `export default {}` section of the script, we review the data and set the rules for the password, as well as verifying the two input passwords match

```js
data () {
  const data = {
    isLoading: false,
    form: false,
    firstName: undefined,
    lastName: undefined,
    email: undefined,
    password: undefined,
    confirmPassword: undefined,
    rules: {
      required: msg => v => !!v || msg,
      confirm: v => v ? v === this.password : 'Passwords do not match'
    },
    show: false,
    showConfirm: false
  }
  return data
},
```

Next, we specify the methods used

```js
methods: {
  ...mapActions('cognito', ['registerUser']),
  ...mapMutations(['setSnackbar']),
  submit () {
    if (!this.$refs.form.validate()) return

    this.isLoading = true

    this.registerUser({
      username: this.email,
      password: this.password,
      attributes: {
        name: `${this.firstName} ${this.lastName}`,
        email: this.email
      }
    })
      .then(() => {
        this.setSnackbar({
          type: 'success',
          msg: 'Account created. Check your email for verification'
        })

        this.$router.push('/authenticated')
      })
      .catch(err => {
        this.setSnackbar({
          type: 'error',
          msg: err.message,
          timeout: 10000
        })
      })
      .finally(() => (this.isLoading = false))
  }
}
```

## User Logout

The `AppBar.vue` component, found in `/web/src/components`, is set up to react to the status of a user session. If a user is logged in, the component will render a 'Logout' button. The opposite is true as well: if there is not a user session, then a 'Sign-in/Register' button appears instead.

This is created by using an if-else statement in the template of the `AppBar.vue` component

```
<div v-if="!this.$store.getters['cognito/isLoggedIn']">
  <v-btn color="blue darken-3"
           class="mr-3"
           @click="$router.push({name:'Signin'})"
           outlined
           small
    >
        <span style="font-family: 'Iowan Old Style'" class="font-weight-bold">
          Login/Sign-Up
        </span>

    </v-btn>
  </div>
```

```
<div v-else>
  <v-btn color="blue darken-3"
           class="mr-3"
           @click="submit()"
           outlined
           small
    >
        <span style="font-family: 'Iowan Old Style'" class="font-weight-bold">
          Logout
        </span>

    </v-btn>
  </div>
```

The `if` portion is immediately taken care of with the injected script, which sees if there is a user session currently
```js
!this.$store.getters['cognito/isLoggedIn']
```

The `else` portion utilizes the `submit()` function
```js
...mapActions('cognito', ['signOut']),
submit () {
  this.isLoading = true

  this.signOut().finally(() => {
    this.isLoading = false
    this.$router.push('/auth/signin')
  })
}
```
