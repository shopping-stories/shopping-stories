# Services

The `web/src/services` directory houses the `http-common.js` file, which activates the Axios HTTP module. This allows the Vue application to talk to the API endpoints that live in the Express App.

```bash
web  
├── src
│   ├── services
│   │   └── http-common.js
```

## http-common.js

`web/src/services/http-common.js` utilizes the `axios` module installed via `web/package.json`. To learn more about the useful functionality of Axios, click [here](https://www.npmjs.com/package/axios).

The entirety of the file is initializing an `HTTP` object that makes use of the variables found in `web/.env`. The `baseURL` attribute is the part of the API endpoint url that is always the same. For the live website, it is `https://api.shoppingstories.org/api/v1/`. When using the `HTTP` object during, for example, a GET request, it looks like the following:

```js
HTTP.get('import/count') {
```

The `import/count` string tells the `HTTP` object to reach the full API endpoint: `https://api.shoppingstories.org/api/v1/import/count`.

The `headers` attribute dictates the token and what type of token that it is. This token is then interpreted by the Express App when the request reaches it. The Express App will automatically reject any requests that do not have a valid token.

Here is `web/src/services/http-common.js` in it's entirety:

```js
// web/src/services/http-common.js

import axios from 'axios';

// grab API base url & set header
export const HTTP = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  headers: {
    Authorization: 'Bearer ' + process.env.VUE_APP_API_TOKEN
  }
})
```
