# Router

The `web/src/router` directory holds the router `index.js` file, which is the glue that holds the website together. All of the Vue components in `web/src/views` are registered in the router and act as webpages.

```bash
web  
├── src
│   ├── router
│   │   └── index.js
```

## index.js

As mentioned above, `web/src/router/index.js` is the glue that holds the Vue application together. It creates and exports the `routes` object, which stores all of the routes to the different pages. These pages are generally the Vue components found in `web/src/views`.

For more information on a Vue router, click [here](https://router.vuejs.org/).


```js
// web/src/router/index.js

import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'


Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: "Shopping Stories",
    }
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search.vue'),
    meta: {
      title: 'Search'
    }
  },

...
  { path: '*', redirect: '/' }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
```

## const routes

The `routes` object registers all of the components that create the Vue application and saves them as routes that can be appended to the base URL of the website. For instance, to get to the `Home.vue` component, simply type in `https://shoppingstories/` in the browser.

The main attributes of a route are:

`path:` the additional string that follows the base URL
`name:` the name of the component and page
`component:` the component to use for the route
`meta: {title: }` the title of the page

There are really two ways to register a route, as seen below. The first registers just by the name of the component, which is hardcoded in the `<Script>` portion of the `.vue` file. The other grabs the component from the location in the `src` directory in relation to the `web/src/router/index.js` file.

```js
// web/src/router/index.js

...
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: "Shopping Stories",
    }
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search.vue'),
    meta: {
      title: 'Search'
    }
  },
...
```

To catch all traffic that goes 'out of bounds' of the website, i.e. to a page that is not registered in the `routes` object, this line is included. It redirects the client back to the Home page.

```js
// web/src/router/index.js

...

{ path: '*', redirect: '/' }

...
```

## const router

Finally, we create a new `router` object that contains all the routes we just dictated to the `routes` object. We set the mode to `history` to avoid reloading the page every time the URL changes. Read more about that [here](https://router.vuejs.org/guide/essentials/history-mode.html).

Finally, we export the router to be used in the Vue App.

```js
// web/src/router/index.js

...

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
```
