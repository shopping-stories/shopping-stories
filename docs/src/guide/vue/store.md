# Store

The `web/src/store` directory holds the setup information for AWS Cognito. It also creates 'global' variables and functions that can be used on multiple components.

```bash
web  
├── src
│   ├── store
│   │   └── index.js
```

## index.js

`web/src/store/index.js` primarily uses Vuex to set global variables in the 'store'. These variables are then accessible by the entire Vue App that is spawned by the client. To learn more about Vuex, click [here.](https://vuex.vuejs.org/#what-is-a-state-management-pattern)

Here's a look at `web/src/store/index.js`:

```js
// web/src/store/index.js

import Vue from 'vue'
import Vuex from 'vuex'
import attachCognitoModule from '@vuetify/vuex-cognito-module'
Vue.use(Vuex)

const set = property => (store, payload) => (store[property] = payload)

// initialize values
const store = new Vuex.Store({
  state: {
    username: '',
    email: '',
    isLoading: false,
    isReady: false,
    password: '',
    snackbar: {}
  },
  mutations: {
    setUsername: set('username'),
    setEmail: set('email'),
    setIsReady: set('isReady'),
    setIsLoading: set('isLoading'),
    setPassword: set('password'),
    setUser: set('user'),
    setSnackbar: set('snackbar')
  }
})

// shoppingstories
attachCognitoModule(store, {
  userPoolId: process.env.VUE_APP_COGNITO_USERPOOLID,
  identityPoolId: process.env.VUE_APP_COGNITO_IDENTITYPOOLID,
  userPoolWebClientId: process.env.VUE_APP_COGNITO_WEBCLIENTID,
  region: process.env.VUE_APP_COGNITO_REGION
}, 'cognito')


store.dispatch('cognito/fetchSession')
  .catch(err => console.log("thisisanerror", err))
  .finally(() => store.commit('setIsReady', true))

export default store
```

### const store

The `store` object holds the Vuex store information used throughout the Vue App. This primarily builds off of the `vuex-cognito-module` that is installed via `web/package.json`. Learn more about this module and all of its functionality [here](https://cognito.vuetifyjs.com/).

Here `store` object is set different `state` attributes that will be used throughout the Vue App. The `mutations` object utilizes the `set` object. This makes more sense when looking at examples in `Signin.vue` (found [here](http://docs.shoppingstories.org/guide/vue/views/signin.html#description)) and `Signup.vue` (found [here](http://docs.shoppingstories.org/guide/vue/views/signup.html#description)).

```js
// web/src/store/index.js

...
// initialize values
const store = new Vuex.Store({
  state: {
    username: '',
    email: '',
    isLoading: false,
    isReady: false,
    password: '',
    snackbar: {}
  },
  mutations: {
    setUsername: set('username'),
    setEmail: set('email'),
    setIsReady: set('isReady'),
    setIsLoading: set('isLoading'),
    setPassword: set('password'),
    setUser: set('user'),
    setSnackbar: set('snackbar')
  }
})
...
```

## Attaching Cognito Module

This attaches the values found in the `store` object to the AWS Cognito User Pool and Identity Pool, as well as the corresponding Cognito App Client via ID. The module will handle most of the Cognito related API functionality regarding credentials, especially when clients sign into the website or create an account.

```js
// web/src/store/index.js

...
attachCognitoModule(store, {
  userPoolId: process.env.VUE_APP_COGNITO_USERPOOLID,
  identityPoolId: process.env.VUE_APP_COGNITO_IDENTITYPOOLID,
  userPoolWebClientId: process.env.VUE_APP_COGNITO_WEBCLIENTID,
  region: process.env.VUE_APP_COGNITO_REGION
}, 'cognito')
...
```

## Dispatch and Export

When the Vue App initally loads, a call is made to fetch the session ID should it exist. If it does, that means the client is already signed in and does not have to go through the sign-in portal. If it does not, it catches the promise in a `console.log`, which serves no real purpose other than avoiding ugly red errors in the browser console.

Removing everything besides the `store.commit('setIsReady', true)` part will get rid of the promise errors but the client will have to log into the website every time the page is refreshed or the tab is closed.

Cognito's timeout length for user sessions is 30 days. Group 7 could not find a way to change that, but ideally it should be a few hours to a few days. In a perfect world, though, users will always signout before leaving the browser.

```js

// web/src/store/index.js

...
store.dispatch('cognito/fetchSession')
  .catch(err => console.log("thisisanerror", err))
  .finally(() => store.commit('setIsReady', true))

export default store
```
