# Verify.vue

## Description

Page that users are able to input the verification codes given to them by the Cognito email service. This only happens when users go through the process of forgetting their passwords.

![Verify](../images/verify.png)

## HTML

The main components are the `<v-card>` that houses the `<v-form>` for the Verification code, new Password, and confirm New Password.

```html
<v-card class="mx-auto pa-10" width="500" elevation="6">
  <v-card-title>Email Verification</v-card-title>
  <v-card-subtitle
  >
    Input the code sent to you through the email address provided
  </v-card-subtitle
  >
  <!-- Input Code -->
  <v-form ref="form" class="mx-auto">
    <v-row>
      <v-text-field v-model="verificationCode" class="ml-7 mr-7" color="secondary" label="Verification Code" outlined>
      </v-text-field>
    </v-row>


    <v-row>
      <!-- Password -->
      <v-text-field
        v-model="password"
        :type="viewPassword ? 'password' : 'text'"
        :append-icon="viewPassword ? 'mdi-eye-off' : 'mdi-eye'"
        @click:append="viewPassword = !viewPassword"
        class="pa-5"
        color="secondary"
        label="Create Password"
        :rules="rules.password"
        required
        outlined
        counter
      >
      </v-text-field>

      <!-- Confirm Password -->
      <v-text-field
        v-model="confirmPassword"
        :type="viewPassword ? 'password' : 'text'"
        :append-icon="viewPassword ? 'mdi-eye-off' : 'mdi-eye'"
        @click:append="viewPassword = !viewPassword"
        class="pa-5"
        color="secondary"
        label="Confirm Password"
        :rules="[password === confirmPassword || 'Password must match']"
        required
        outlined
        counter
      >
      </v-text-field>
    </v-row>

    <!-- Submit Button -->
    <v-card-actions class="justify-center mt-5">
      <v-btn @click="submit()" color="secondary" class="white--text">
        Submit
        <v-icon class="pl-2"> mdi-login </v-icon>
      </v-btn>
    </v-card-actions>
  </v-form>
</v-card>
```

## Imports

```js
import { mapActions, mapMutations, mapState } from "vuex";
```

## Methods

### Submit()

Function is called when users click the `Submit` button on the webpage. This makes an API call through the vuex cognito module, and updates the user's password if successful. Otherwise, it alerts the user that there has been an error.

```js
submit() {
  this.changePassword({
    username: this.email,
    code: this.verificationCode,
    newPassword: this.password,
  })
  .then(() => {
    this.setSnackbar({
      type: "success",
      msg: "Success! Your password has been changed",
    });

    // Push user to sign in page
    this.$router.push("/auth/signin");
  })
  .catch((err) => {
    console.error(err);
    this.setSnackbar({
      type: "error",
      msg: 'Unable to process request. Please verify information is accurate',
      timeout: 10000,
    });
  })
  .finally(() => {
    this.isLoading = false;
  });
},

```

## Data

```js
data: () => ({

  viewPassword: true,
  password: "",
  confirmPassword: "",

  rules: {
    password: [
      (v) => !!v || "Password is required",
      (v) => /(?=.*[0-9])/.test(v) || "Password must contain a number", // Checking for at least one number
      (v) =>
        /(?=.*[A-Z])/.test(v) || "Password must contain a uppercase letter", // Checking for at least one uppercase letter
      (v) =>
        /(?=.*[a-z])/.test(v) || "Password must contain a lowercase letter", // Checking for at least one lowercase letter
      (v) =>
        /(?=.*[@#$%^&*!])/.test(v) ||
        "Password must contain a special character", // Checking for at least one special character
      (v) => v.length > 7 || "Password must have 8 or more characters",
      ],
  },

  // Attributes
  verificationCode: "",
}),
```

## Created

```js
created() {
  document.title = "Verify Email | Shopping Stories";
},
```

## Computed

```js
computed: {
  ...mapState([
    'email',
  ]),
}
```
