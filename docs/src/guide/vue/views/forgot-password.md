# ForgotPassword.vue

## Description

`ForgotPassword.vue` is where users end up after clicking the `Forgot Password` button on the signin page, `Signin.vue`. The purpose of this page is for users to input their email that is registered in AWS Cognito, where a verification code will be sent. This page also leads users to the `Verify.vue` page, wherein users will input their code they just sent and reset their password.

![Forgot Password](../images/forgotpassword.png)

## HTML

As with similar pages such as `Signin.vue` and `Signup.vue`, this page makes use of the `v-form` component. We grab the user's email that they want the email verification code to be sent to.

```html
<!-- Sign-In -->
<v-form ref="form" class="mx-auto">
  <v-row>
    <v-text-field v-model="internalValue" name="email" class="ml-7 mr-7" color="secondary" label="Email" outlined>
    </v-text-field>
  </v-row>
  <v-card-actions class="justify-center mt-5">
    <v-btn @click="submit()" color="secondary" class="white--text">
      Send Code
      <v-icon class="pl-2"> mdi-email </v-icon>
    </v-btn>
  </v-card-actions>
</v-form>
```

## Imports

```js
import { mapActions, mapMutations, mapState } from "vuex";
```

## Methods

First, we grab some necessary functions from the Vuex Cognito module.

```js
...mapActions("cognito", ["forgotPassword"]),
...mapMutations(["setSnackbar"]),
...mapMutations(['setEmail']),
```

Then we put those to use in `submit()`, which will be called when users click the `Send Code` button on the page.

This function grabs the user input email address, loads it into the Cognito module, who then ships it to the AWS Cognito API. If successful, AWS will send the user a verification code and we send the user to the `Verify.vue` page to input that code.

```js
// Creates new password from user input
submit() {
  this.forgotPassword({
    username: this.email,
  })
  .then(() => {
    this.setSnackbar({
      type: "success",
      msg: "Success! Check your email for instructions",
    });
    this.$router.push("/auth/verify");
  })
  .catch((err) => {
    console.error(err);
    this.setSnackbar({
      type: "error",
      msg: err,
      timeout: 10000,
    });
  })
  .finally(() => {
    this.isLoading = false;
  });
},
```

## Data

```js
data: () => ({

  // Attributes
  emailAddress: "",
}),
```

## Created

```js
created() {
  document.title = "Forgot Password | Shopping Stories";
},
```
