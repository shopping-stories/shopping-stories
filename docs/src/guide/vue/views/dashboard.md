# Dashboard.vue

## Description

The Dashboard is essentially a user hub for many useful features.

![Dashboard](../images/dashboard.png)

Most of these features are locked for the average user. Here's a breakdown of who has access to what:

```
> User/No Group:
Settings

> Researcher Group:
Settings
Import
Files
Browser
Docs

> Admin (Molly)
Settings
Import
Files
Browser
Users
```

## HTML

`Dashboard.vue` serves as the access point to all of the sidebar tabs. That is all contained in the Navbar, which utilizes `v-navigation-drawer`.
```html
<!-- Navbar -->
<v-navigation-drawer
  v-model="drawer"
  height="90vh"
>
...
```

The User Info `v-card` component puts the user's name on the top of the NavBar and lists their role, should the user have one.

```html
<!-- User Info -->
<v-card fill-width flat tile>
  <v-sheet
    v-if="this.$store.getters['cognito/isLoggedIn']"
    color="primary"
    height="75"
  >
    <v-card-title> {{ this.$store.getters['cognito/userAttributes'].name }} </v-card-title>
    <v-card-subtitle v-if="this.$store.getters['cognito/userGroups']">
      Role: {{ this.group }}
    </v-card-subtitle>
  </v-sheet>
</v-card>
```

Here is where the list of tabs is displayed. It utilizes the `v-list` component.

```html
<!-- User Options -->
<v-list id="userOptions" shaped>
  <v-list-item-group v-model="selectedItem" color="secondary">
    <!-- Item Template -->
    <v-list-item
      v-for="item in userOptionsList"
      :key="item.title"
      class="mt-2 mb-2"
    >
      <v-list-item-action>
        <v-list-item-title class="text-h6">
          <v-icon class="pb-1 pr-2" medium>{{ item.icon }} </v-icon>
          {{ item.title }}
        </v-list-item-title>
      </v-list-item-action>
    </v-list-item>
    <!-- Item Template -->
  </v-list-item-group>
</v-list>
```

## Imports

```js
import UserSettings from "@/components/UserSettings.vue";
import ImportData from "@/components/ImportData.vue";
import UserManagement from "@/components/UserManagement.vue";
import CheckImports from '@/components/CheckImports.vue';
import Docs from '@/components/Docs.vue';
import Browser from '@/components/Browser.vue';
```
## Methods

One simple function called `CheckUser()` that simply grabs the components that the users are authorized to see. Array starts from `0`.

```js
checkUser() {
  if (this.group == 'Researcher') {
    this.userOptionsList[1] = { title: "Import", icon: "mdi-compass", }
    this.userOptionsList[2] = { title: "Files", icon: "mdi-file-find-outline",}
    this.userOptionsList[3] = { title: "Browser", icon: "mdi-database-search",}
    this.userOptionsList[4] = { title: "Docs", icon: "mdi-file-question",}
  }
  else if (this.group == 'Admin') {
    this.userOptionsList[1] = { title: "Import", icon: "mdi-compass", }
    this.userOptionsList[2] = { title: "Files", icon: "mdi-file-find-outline",}
    this.userOptionsList[3] = { title: "Browser", icon: "mdi-database-search",}
    this.userOptionsList[4] = { title: "Docs", icon: "mdi-file-question",}
    this.userOptionsList[5] = { title: "Users", icon: "mdi-account-group",}
  }
}
```

## Data

*Note:* The `selectedItem` object is set to `0` initially so that when the Dashboard loads, the first thing a user sees is their personal settings. This component is available for all users.

```js
data: () => ({

  username: null,
  drawer: true,
  group: '',
  selectedItem: 0,
  userOptionsList: [
    { title: "Settings", icon: "mdi-cog" },
  ],
}),
```

## Created

When the component is loaded, we grab all of the necessary information and update the values in the different components. We also set the page name, as well as check if the user is logged in at all. If not, send them off to the home page as they are not authorized to see the Dashboard (and shouldn't be there anyway).

```js
created() {

  // Only accessible by users who are signed in
  if (!this.$store.getters['cognito/isLoggedIn'])
    this.$router.push("/");

  // Name of page, as seen in the tab
  document.title = "Dashboard | Shopping Stories";

  // grab the group from Cognito
  const groupID = this.$store.getters['cognito/userGroups'];

  // display if exists
  if (groupID == 'Researcher')
    this.group = 'Researcher';

  else if (groupID == 'Admin')
    this.group = 'Admin'

  this.checkUser();
},
```
