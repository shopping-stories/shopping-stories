# Project.vue

## Description

Project.vue details what The Shopping Stories Project is about. It gives a brief history of the ledgers used in this first phase of the project.

## HTML

The APIs used in this view are `<v-container>`, `<v-row>`, and `<v-img>`.  `d-flex` is used throughout the HTML to wrap content on screen depending on various viewport sizes. `<v-img>` uses a custom `<div>` style to overlay on the image.

The components used in this view are `<v-sheet>` and `<v-card>`. The `<v-sheet>` component is sized appropriately to best fit the overlayed image. `<v-card>` is used on the same `<v-row>` and holds the content text of this page. It also sized wider than other similar pages to ensure the original aspect ratio of the used ledger image is maintained. Some text is made clickable through `<a href="">`.

## Style

```
<style scoped>
#caption {
  border-left: 5px double #BBDEFB;
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
  font-family: "Iowan Old Style";
  font-size: 14px;
}

p {
  font-family: "Iowan Old Style";
  font-size: 18px;
}

#title {
  font-family: "Iowan Old Style";
  font-size: 28px;
}

#subtitle {
  font-family: "Iowan Old Style";
  font-size: 20px;
}

.caption {
  position: absolute; /* Position the background text */
  bottom: 0; /* At the bottom. Use top:0 to append it to the top */
  background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.65); /* Black background with 0.5 opacity */
  color: #f1f1f1; /* Grey text */
  width: 100%; /* Full width */
  padding: 5px; /* Some padding */
  padding-left: 10px;
  height: 35px;
}

</style>
```

The styled components of this view are`p`, `#title`,`#subtitle` `.caption`, and `#caption`. Components `p`, `title`, and `#subtitle` are only styled to change font-family and font-size. The `#caption`component is styled to a smaller font-size with a blue double border to the right. The `.caption` component is styled to to overlay a slightly opaque black background with white text to form a caption at the bottom of the used image.

## Created

Simple method to set the title of the webpage that a browser displays.