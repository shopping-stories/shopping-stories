# Team.vue

## Description

Team.vue is a page that details the founding directors from History Revealed.

## HTML

The APIs used in this view are `<v-container>`, `<v-row>`, `<v-col>`, and `<v-img>`. 

The components used are `<v-card>` and `<v-avater>`. The `<v-card>` component contains elevations and tile props. An avatar is simply placed towads the top left of each card with a local image used as the source. `<v-list-item-title>` and `<v-item-list-subtitle>` are used for each team members name and work title. 

## Style

```
<style scoped>

#title {
  font-family: 'Iowan Old Style';
  font-size: 30px;
  font-weight: bold
}

#subtitle {
  font-family: 'Iowan Old Style';
  font-size: 20px;
}

#bio {
  font-family: 'Iowan Old Style';
  font-size: 17px;

}

h1 {
  font-size: 40px;
}

</style>
```

The styled components are `#title`, `#subtitle`, `#bio`, and `h1`. These are all styled to only change font-size and/or font-family.

## Created

Simple method to set the title of the webpage that a browser displays.