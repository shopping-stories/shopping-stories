# Signup.vue

## Description

`Signup.vue` is where users can create an account. This page can be reached from the `Signin.vue` page. Here we grab all the user inputs, where every field is required, and process it with AWS Cognito. If everything works, the user should receive a confirmation email. This confirmation email will include a link that users can click, which then confirms their account.

::: warning
Users must confirm their accounts before being able to sign into the website.
:::

![Sign Up](../images/signup.png)

## HTML

This page consists of a form that users fill out. As such, most of the HTML is in relation to the form and the requirements we have for users. Notice the `:rules` for each field.

```html
<!-- Form body for sign-up -->
<v-form ref="form">
  <!-- Username & First & Last name row  -->
  <v-row class="d-flex flex-wrap">
    <v-text-field
      v-model="firstname"
      class="pa-5"
      color="secondary"
      label="First Name"
      :rules="rules.name"
      required
    >
    </v-text-field>
    <v-text-field
      v-model="lastname"
      class="pa-5"
      color="secondary"
      label="Last Name"
      :rules="rules.name"
      required
    >
    </v-text-field>
  </v-row>
  <v-row>
    <!-- Username & Email -->
    <v-text-field
      v-model="username"
      class="pa-5"
      color="secondary"
      label="Username"
      :rules="rules.username"
      required
    >
    </v-text-field>
    <!-- Email -->
    <v-text-field
      v-model="emailAddress"
      class="pa-5"
      color="secondary"
      label="Email"
      :rules="rules.email"
      required
    >
    </v-text-field>
  </v-row>
  <v-row>
    <!-- Password -->
    <v-text-field
      v-model="password"
      :type="viewPassword ? 'password' : 'text'"
      :append-icon="viewPassword ? 'mdi-eye-off' : 'mdi-eye'"
      @click:append="viewPassword = !viewPassword"
      class="pa-5"
      color="secondary"
      label="Create Password"
      :rules="rules.password"
      required
      counter
    >
    </v-text-field>

    <!-- Confirm Password -->
    <v-text-field
      v-model="confirmPassword"
      :type="viewPassword ? 'password' : 'text'"
      :append-icon="viewPassword ? 'mdi-eye-off' : 'mdi-eye'"
      @click:append="viewPassword = !viewPassword"
      class="pa-5"
      color="secondary"
      label="Confirm Password"
      :rules="[password === confirmPassword || 'Password must match']"
      required
      counter
    >
    </v-text-field>
  </v-row>

  <!-- Submit & Validate -->
  <v-card-actions class="justify-center mt-10">
    <v-btn @click="submit()" color="secondary" class="white--text"> Create Account </v-btn>
  </v-card-actions>
</v-form>
```

## Imports

```js
import { mapActions, mapMutations } from "vuex";
```

## Methods

First, we grab some valuable functions from the store dealing with the Vuex Cognito module.

```js
...mapActions("cognito", ["registerUser"]),
...mapMutations(["setSnackbar"]),
```

Then we validate the form via a built-in function.

```js
// Form Validation
validateForm() {
  if (!this.$refs.form.validate()) {
    console.error("Not Valid Form");
    return;
  }
},
```

Finally, `submit()` is called when a user clicks the submit button on the page. This will take all of the user inputs and throw it into the Vuex Cognito module, which will then process it with the AWS API. I

There are a few things we watch out for. For instance, if the username has already been taken, we alert the user that they must create a different one.

```js
// Form submit to cognito
submit() {
  // Validating the components meet the rules criteria
  this.validateForm();

  this.isLoading = true;
  this.registerUser({
    username: this.username,
    password: this.password,
    attributes: {
      name: `${this.firstname} ${this.lastname}`,
      email: this.emailAddress,
    },
  })
    .then(() => {
      this.setSnackbar({
        type: "success",
        msg: "Account created. Check your email for verification",
      });

      this.$router.push("/auth/signin");
    })
    .catch((err) => {

      if (err.code == 'UsernameExistsException') {
        this.setSnackbar({
          type: 'error',
          msg: 'Error: Username already exists!',
          timeout: 5000,
        })
      }

      else {
        console.error(err);
        this.setSnackbar({
          type: "error",
          msg: err,
          timeout: 10000,
        });
      }

    })
    .finally(() => {
      this.isLoading = false;
    });
},
```


## Data

The data objects are broken up into two distinct parts. The first are the standard objects one would expect for a Sign Up page.

```js
data: () => ({
  // Control flow
  isLoading: false,
  viewPassword: true,
  // New signup attributes
  username: "",
  firstname: "",
  lastname: "",
  emailAddress: "",
  password: "",
  confirmPassword: "",
...
```

The `rules` object is where we house all of the rules that pertain to user inputs. Refer to the Cognito documentation for Password rules. For the rest, we are simply watching for anything funny. It would be easy to trick it but it is at least a safeguard.

```js
// Rules for each element
rules: {
  username: [
    (v) => !!v || "Username is required",
    (v) =>
      /^[A-Za-z]+$/.test(v) ||
      "Name can't contain numbers or special chars.", // numbers are actually okay but this is fine for now
  ],
  email: [
    (v) => !!v || "E-mail is required",
    (v) => /.+@.+/.test(v) || "E-mail must be valid",
  ],
  name: [
    (v) => !!v || "Name is required",
    (v) =>
      /^[A-Za-z]+$/.test(v) ||
      "Name can't contain numbers or special chars.",
  ],
  password: [
    (v) => !!v || "Password is required",
    (v) => /(?=.*[0-9])/.test(v) || "Password must contain a number", // Checking for at least one number
    (v) =>
      /(?=.*[A-Z])/.test(v) || "Password must contain a uppercase letter", // Checking for at least one uppercase letter
    (v) =>
      /(?=.*[a-z])/.test(v) || "Password must contain a lowercase letter", // Checking for at least one lowercase letter
    (v) =>
      /(?=.*[@#$%^&*!])/.test(v) ||
      "Password must contain a special character", // Checking for at least one special character
    (v) => v.length > 7 || "Password must have 8 or more characters",
  ],
},
}),
```

## Created

```js
created() {
  if (this.$store.getters['cognito/isLoggedIn'])
    this.$router.push("/");

  document.title = "Sign Up | Shopping Stories";
},
```
