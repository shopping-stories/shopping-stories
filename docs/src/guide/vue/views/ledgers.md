# Ledgers.vue

## Description

Ledgers.vue details what ledgers are in the context of The Shopping Stories Project. It gives a brief history of ledgers and the type of historical information that can be learned from them. It also displays an example of a ledger from the time period that is being researched in this project.

## HTML

The APIs used in this view are `<v-container>`, `<v-row>`, and `<v-img>`.  `d-flex` is used throughout the HTML to wrap content on screen depending on various viewport sizes. `<v-img>` uses a custom `<div>` style to overlay on the image.

The components used in this view are `<v-sheet>` and `<v-card>`. The `<v-sheet>` component is sized appropriately to best fit the overlayed image. The `<v-card>` component is used on the same `<v-row>` and holds the content text of this page. The `<v-sheet>` components use props to set elevation and rounded corner characteristics. It also sized wider than other similar pages to ensure the original aspect ratio of the used ledger image is maintained.

## Style

```
<style scoped>
p {
  font-family: "Iowan Old Style";
  font-size: 18px;
}

#title {
  font-family: "Iowan Old Style";
  font-size: 30px;
}

.caption {
  position: absolute; /* Position the background text */
  bottom: 0; /* At the bottom. Use top:0 to append it to the top */
  background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.65); /* Black background with 0.5 opacity */
  color: #f1f1f1; /* Grey text */
  width: 100%; /* Full width */
  padding: 5px; /* Some padding */
  padding-left: 10px;
  height: 55px;
}


.caption2 {
  position: absolute; /* Position the background text */
  bottom: 0; /* At the bottom. Use top:0 to append it to the top */
  background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.65); /* Black background with 0.5 opacity */
  color: #f1f1f1; /* Grey text */
  width: 100%; /* Full width */
  padding: 5px; /* Some padding */
  padding-left: 10px;
  height: 40px;
}


</style>
```

The styled components of this view are`p`, `#title`, and two `.caption` styles. Components `p` and `#title` are only styled to change font-family and font-size. The `.caption` components are styled to to overlay a slightly opaque black background with white text to form a caption at the bottom of the used image. There are two of these caption styles as they required different heights to better match the caption length each image displayed.



## Created

Simple method to set the title of the webpage that a browser displays.