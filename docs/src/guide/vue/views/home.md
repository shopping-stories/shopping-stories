# Home.vue

## Description

Home.vue is the homepage of https://www.shoppingstories.org. It displays a carousel of images that link to various other pages of the website.

## HTML

The APIs used in this view are `<v-container>`, `<v-layout>`, and `<v-spacer>`.

The components used in this view are `<v-carousel>` and `<v-btn>`. The `<v-carousel>` component contains props to change the interval time between cycling images, the carousel height, and whether arrows the user can click to manually cycle images are shown when hovering a cursor on an image. Each image has a `<v-btn>` component overlayed on it to let the user know what page on the website the image links to. Standard `<div>` and `<h1>` components are used under the carousel to display the title and subtitle of the website.

## Data

```
export default {
  name: "Home",

  data() {
    return {
      items: [
        {
          src: require('../images/HighLifeBelowStairs.jpg'),
          title: "Painting",
          text: "Search the Database",
          link: "Search",
        },
        {
          src: require('../images/Map.png'),
          title: "Map",
          text: "About the Shopping Stories Project",
          link: "Project",
        },
        {
          src: require('../images/ElizabethConnell.jpg'),
          title: "Folio",
          text: "Transcribing and the Database",
          link: "Transcription",
        },

        {
          src: require('../images/Money.jpg'),
          title: "Money",
          text: "Ledger Basics",
          link: "Ledgers",
        },
        {
          src: require('../images/Quill01.jpg'),
          title: "Book and Quill",
          text: "Acknowledgements",
          link: "Acknowledgements",
        },
      ],
    };
  },
```

The data portion on this view is simply a data array of various items needed to display on the carousel. Each carousel item has a corresponding index in this data aray that contains an image source, image title, `<v-btn>` text label, and link which corresponds to the name of a webpage in the Vue router.

## Created

Simple method to set the title of the webpage that a browser displays.

## Style

The styled components of this view are`h1`, and `h2`. These Components are only styled to change font-family and font-size.