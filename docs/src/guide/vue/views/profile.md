# Profile.vue

## Description

Profile.vue is a template page that is not in official use at the time of this writing. It is simply a template design for how a potential profile view for users and/or History Revealed employees or volunteers could look.

## HTML

The APIs used in this view are `<v-container>`, `<v-row>`, `<v-col>`, and `<v-img>`. Multiple `<v-row>` APIs are used to organize content on the profile card. All text and images have a center justification.

The components used in this view is `<v-card>`, `<v-icon>`, and `<v-avatar>`. The `<v-avatar>` component would be used here to display the profile image of a user's own choosing. `<v-icon>` is used to display various different icons on this profile page.

## Style

The styled `.bgimg` component is used to source the image that is at the top of the `<v-card>` and behind the `<v-avatar>` component.

## Created

Simple method to set the title of the webpage that a browser displays.