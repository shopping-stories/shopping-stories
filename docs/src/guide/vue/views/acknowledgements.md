# Acknowledgements.vue

## Description

Acknowlegements.view displays credits for the various images used on the website. This page also credits the many people who have been involved with History Revealed and this Shopping Stories Project.

## HTML

The APIs used in this view are `<v-container>`, `<v-row>`, and `<v-img>`.  `d-flex` is used throughout the HTML to wrap content on screen depending on various viewport sizes. `<v-img>` uses a custom `<div>` style to overlay on the image.

The components used in this view are `<v-sheet>` and `<v-card>`. `<v-sheet>` is sized appropriately to best fit the overlayed image. `<v-card>` is used on the same `<v-row>` and holds the text of this page that credits the contributors of the project. Another `<v-row>` is placed below the initial one to hold citation text for various images used on the website. It is sized wider than the other `<v-card>`. Links in this citation page are made clickable  through `<a href="">`.

## Style

```
<style scoped>

p {
  font-family: "Iowan Old Style";
  font-size: 18px;
}

#title {
  font-family: "Iowan Old Style";
  font-size: 24px;
}

.caption {
  position: absolute; /* Position the background text */
  bottom: 0; /* At the bottom. Use top:0 to append it to the top */
  background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.5); /* Black background with 0.5 opacity */
  color: #f1f1f1; /* Grey text */
  width: 100%; /* Full width */
  padding: 5px; /* Some padding */
  padding-left: 10px;
  height: 55px;
}



</style>
```

The styled components of this view are`p`, `#title`, and `.caption`. Components `p` and `title` are only styled to change font-family and font-size. `.caption` is styled to to overlay a slightly opaque black background with white text to form a caption at the bottom of the used image.

## Created

Simple method to set the title of the webpage that a browser displays.