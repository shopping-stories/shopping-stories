# Signin.vue

## Description

As the name implies, this is where users go to sign into the website. When the user clicks the button, we process the inputs with AWS Cognito and confirm that the inputs are valid, otherwise we clean the form and the user tries again.

![Sign In](../images/signin.png)

## HTML

The bulk of the action for this page happens in a `v-card` component. This first part sets the visual icon that grabs the user's eyes.

```html
<!-- Icon -->
<v-container class="mx-auto d-flex flex-wrap">
  <v-icon class="main-icon mx-auto" color="secondary">
    mdi-account-circle
  </v-icon>
</v-container>
```

The real action, however, was in the `v-form` component, where we grab user input and send it off to process.

```html
<!-- Sign-In -->
<v-form ref="form" class="mt-6 mx-auto" v-on:keypress.enter="login">
  <v-row>
    <v-text-field
      class="ml-15 mr-15"
      color="secondary"
      label="Username/Email"
      v-model="username"
      outlined
      v-on:keypress.enter="login"
    >
    </v-text-field>
  </v-row>
  <v-row>
    <v-text-field
      class="ml-15 mr-15"
      color="secondary"
      label="Password"
      type="password"
      v-model="password"
      outlined
      v-on:keypress.enter="login"
    >
    </v-text-field>
  </v-row>
  <v-card-actions class="justify-center mt-5">
    <v-btn @click="login()" color="secondary" class="white--text">
      Login
      <v-icon class="pl-2"> mdi-login </v-icon>
    </v-btn>
  </v-card-actions>
</v-form>
```

Finally, we give users a change to create a new account or run through the process of retrieving their account.

```html
<v-card-actions class="mt-10">
  <v-btn text to="/auth/forgotpassword">Forgot Password</v-btn>
  <v-spacer></v-spacer>
  <v-btn text to="/auth/signup"> Create Account</v-btn>
</v-card-actions>
```
## CSS

```css
.main-icon {
  font-size: 9rem !important ;
  text-shadow: 0px 10px 10px #00000042;
}
```
## Imports

```js
import { mapActions, mapMutations } from "vuex";
```

## Methods

First, we grab some important functions from the store.

```js
...mapActions("cognito", ["signInUser"]),
...mapMutations(["setEmail", "setPassword", "setIsLoading", "setSnackbar"]),
```

For the actual act of signing into the website, we run the user inputs through the vuex cognito module, which talks to the official AWS API endpoints. We also set the Snackbar to reflect on the success of that attempt. After a bad attempt, we go ahead and flush out all user inputs from the form.

```js
// Login to cognito
login() {
  // Attempting login
  this.signInUser({
    username: this.username,
    password: this.password,
  })
  .then(() => {
    this.setSnackbar({
      type: "success",
      msg: this.username + " has been signed in.",
    });
    // Pushing to the new page
    this.$router.push("/dashboard");
  })
  .catch(() => {
    this.setSnackbar({
      type: "error",
      msg: "Invalid Username/Password",
    });
    this.$refs.form.reset();
  })
},
```

## Data

```js
data: () => ({
  model: 0,
  username: "",
  password: "",
}),
```

## Created

When the page is loaded, we immediately check if the user is logged in already. If so, the user obviously does not need to be on this page, and we push them to the home page.

```js
created() {

  if (this.$store.getters['cognito/isLoggedIn'])
    this.$router.push("/");

  document.title = "Sign In | Shopping Stories";
},
```
