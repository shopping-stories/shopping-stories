# Transcription.vue

## Description

Transcription.vue is a simple page that explains how the ledgers used in this project were transcribed from their original paper form into a modern format that could more easily be used for research purposes.

## HTML

The APIs used in this view are `<v-container>`, `<v-row>`, and `<v-img>`.  `d-flex` is used throughout the HTML to wrap content on screen depending on various viewport sizes.

The components used in this view are `<v-sheet>` and `<v-card>`. `<v-sheet>` is sized appropriately to best fit the overlayed image. `<v-card>` is used on the same `<v-row>` and holds the content text of this page. Some text is made clickable  through `<a href="">`.

## Style

```
<style scoped>
p {
  font-family: "Iowan Old Style";
  font-size: 18px;
}

#title {
  font-family: "Iowan Old Style";
  font-size: 24px;
}



</style>
```

The styled components of this view are`p` and `#title`. Components `p` and `title` are only styled to change font-family and font-size.

## Created

Simple method to set the title of the webpage that a browser displays.