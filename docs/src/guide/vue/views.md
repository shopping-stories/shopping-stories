# Views

The `web/src/views` directory houses all of the unique pages that clients visit when using the Vue web application. These Vue components are registered in the router and have unique routes, such as `https://shoppingstories.org/about` for `About.vue`.

```bash
web  
├── src
│   └── views
│       ├── About.vue
│       ├── Acknowledgements.vue
│       ├── Dashboard.vue
│       ├── Files.vue
│       ├── ForgotPassword.vue
│       ├── Home.vue
│       ├── Ledgers.vue
│       ├── Profile.vue
│       ├── Project.vue
│       ├── Search.vue
│       ├── Signin.vue
│       ├── Signup.vue
│       ├── Team.vue
│       ├── Transcription.vue
│       └── Verify.vue
```
