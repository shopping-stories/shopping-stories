# Vue + Vuetify

This section is to help explain and clarify the parts of the Shopping Stories website regarding Vue and Vuetify. Shopping Stories uses [Vue](https://vuejs.org/) as the frontend framework to build the website. [Vuetify](https://vuetifyjs.com/en/) is the UI Library used on top of Vue for the Material Design look that the website has. 

## Website File Structure

All the relvant files to the frontend website are under the "web" folder in the git file structure. All the folders and files referenced in this section will be under this folder. "App.vue" is the top level of a vue app where the different elements and pages of the website laid out to be rendered. Inside the "Views" are all the individual pages of the website. This would include the Home Page, About Page, and more. Some .vue pages use components that have been made in a separate .view file and those components can be found in the "components" folder.  When components are used in a page, they will be imported on the <script> portion of the .view file of said page:

```
<script>
import UserSettings from "@/components/UserSettings.vue";
import ImportData from "@/components/ImportData.vue";
import UserManagement from "@/components/UserManagement.vue";
import CheckImports from '@/components/CheckImports.vue';



export default {
  components: { UserSettings, ImportData, UserManagement, CheckImports },
```

Images that are in use throughout the website are contained in the "images" folder. The "plugins" folder contains a .js file that sets up the color scheme and icon library used on the website. The "router" folder contains a .js file that sets up the native vue [router](https://router.vuejs.org/) functionality for all the routes and URL paths for the website.

## Setup

Setting up the environment for this Vue project is fairly straight foward. On your command prompt, change directory to the web folder and the run `npm install` to install all required dependencies that are laid out in the included package.json files. 

The `npm run serve` command will start a local developmental server on your machine that will allow you to view the website locally and see any changes you make to the HTML or JS files live.

