# Vuetify.js

## Description

Vuetify.js is a  file located in `web/src/plugins` that simply defines the colors used throughout the website as well as the website's icon library.

## Javascript

```
export default new Vuetify({
    icons: {
        iconfont: 'mdi', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
    },
    theme: {
        themes: {
            light: {
                primary: '#BBDEFB',
                secondary: '#1565C0',
                backgroundColor: '#FFF8E1',
                accent: '#B23850',
                appBarAccent: "#1565C0",
                error: '#b71c1c',
            },
        },
    },
});
```

 In this javascript file, you can easily change the colors that are used on the website. The `primary` color for example is the color used on the navigation bar at the top of the website (currently a light blue at the time of this writing). The `secondary` color for example is the color used in buttons on the home page carousel. The `backgroundColor` is the beige color that is used for the background of most pages on the website. Simply changing the hexcode of these definitions will change the color across the website.

Another potential use for this file would be to define another set of colors for a dark mode version of the website. Vuetify makes this funtionality very simple to implement if you wish.

You can also uncomment any of the other icon libraries listed in the file to have even more options for icons.