# Linking Cognito with Vue

The Vue webapp uses a Veutify module to connect to a Cognito User Pool. The process is relatively simple.

Documentation does already exist for the specific module used, see [here]ccccccccccccccc. This guide will touch some of the same ground as it relates to the Shopping Stories project.

## Pre-requisites

-> Set up a Cognito User Pool (see here (TODO: Fix link) for the setup process)

## Install

Install the Vuex module through npm

```sh
npm install @vuetify/vuex-cognito-module
```

## Import

Import this module into the entry point for the project. In ShoppingStories.org, that is in `/web/src/main.js`

```js
import attachCognitoModule from '@vuetify/vuex-cognito-module'
```

Then attach the module to `web/src/store/index.js`. Make sure to grab the required ID's and region code from the Cognito User Pool you want to link to.

```js
import store from './store'

attachCognitoModule(store, {
  userPoolId:'USER-POOL-ID',
  identityPoolId: 'IDENTITY-POOL-ID',
  userPoolWebClientId: 'WEB-CLIENT-ID',
  region: 'REGION'
}, 'cognito')
```
