# Signing into AWS

There are two ways to get to the login console.
1. Click [this link](https://console.aws.amazon.com/?nc2=h_m_mc) to be brought directly to the sign-in portal.
---
2. Or navigate there manually:

```link
https://aws.amazon.com/console/  
```

- Click the drop down on the top-right of the webpage:  
![link-to-sign-in](./images/sign-in/sign-in-AWS-Sign-In-Link.png)

---
From there you will be brought to the sign-in portal.  
Ensure that you’re signing in as an IAM user and not the root user.  
![Sign-in Portal](./images/sign-in/sign-in-IAM-Sign-In.png)  


Consult Molly if you need to setup an IAM user name/password for the project.  

---
Once you are signed in, ensure that your region is set to **US East (N. Virginia) us-east-1**:  

![region-check](./images/sign-in/sign-in-Region-Check.png)  


### Congratulations, you're now signed in and ready to utilize AWS for Shopping Stories!
