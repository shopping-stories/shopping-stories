# Route 53 (DNS)


Route 53 is Amazon's Domain Name System (DNS) service. Molly originally purchased the domain through domaindiscount24, but it was transferred to Route 53 successfully on October 2nd, 2020.

---

The elastic IP associated with the instance should remain consistent indefinitely. However, some operations exist which require the manipulation/transfer of the Elastic IP, such as instance restoration from a backup. If the associated Elastic IP is inadvertently released, you must obtain a new Elastic IP and update the Route 53 record shown below.

**SHOULD THE ASSIGNED IP BE LOST:**  
The following A-Record will need to be updated to reflect the new Elastic IP.
#### 1.
![Route53 Console](./images/route53/route53-dashboard.png)  
#### 2.
![A_record](./images/route53/route53-A-record.png)  


## Subdomains

There are currently 4 subdomains for `shoppingstories.org`. They are

- `docs.shoppingstories.org`

- `browser.shoppingstories.org`

- `api.shoppingstories.org`

- `www.shoppingstories.org`

These subdomains are all currently implemented within Route 53:

![Subdomains](./images/route53/route53-subdomains.png)

### api.shoppingstories.org

The subdomain `api.shoppingstories.org` is registered as an alias for `shoppingstories.org` in Route 53. However, the NGINX proxy server on the EC2 monitors for that domain request and reroutes traffic to the Express App. Learn more about the proxy server [here](http://api.shoppingstories.org/guide/ec2/nginx.html).

### browser.shoppingstories.org

The subdomain `browser.shoppingstories.org` is registered as an alias for `shoppingstories.org` in Route 53. However, the NGINX proxy server on the EC2 monitors for that domain request and reroutes traffic to the internal Neo4j Browser. Learn more about the proxy server [here](http://api.shoppingstories.org/guide/ec2/nginx.html).

### www.shoppingstories.org

The subdomain `www.shoppingstories.org` is registered as an alias for `shoppingstories.org` in Route 53. The NGINX proxy server on the EC2 monitors for that domain request and reroutes traffic to `shoppingstories.org`. Learn more about the proxy server [here](http://api.shoppingstories.org/guide/ec2/nginx.html).

### docs.shoppingstories.org

The subdomain `docs.shoppingstories.org` is the only subdomain that is not an alias for `shoppingstories.org`. This domain is, instead, registered under the `docs.shoppingstories.org` S3 bucket. That bucket can be accessed via AWS Management Console. To learn more about this and other S3 buckets, click [here](http://api.shoppingstories.org/guide/aws/s3.html)
