
# Certificate Manager

*An Amazon-issued certificate exists, but at the time of writing is not being utilized...*

For certificate-related information for this project, check out [Relevant Certificates](../ec2/certificates.md)!

---

An Amazon-issued certificate was generated while attempting to setup a redirect for HTTP -> HTTPS, so one could simply type:
```
shoppingstories.org
```
into their browser and be redirected to:
```
https://shoppingstories.org
```
Ultimately, it was not utilized. Rather than delete it, it was left for potential use in the future. The certificate that was issued will expire on 2021-11-12. It can be renewed via the console.


[Click here](https://console.aws.amazon.com/acm/home?region=us-east-1#/) to be brought to the Certificate Manager console.
