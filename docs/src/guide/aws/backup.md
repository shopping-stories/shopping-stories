# <img src="./images/backup/AWS-Backup-300x300.png" width="30" /> AWS Backup

*At the time of writing, there are currently daily backups, each of which are retained for 5 days before being* ***permanently deleted.***

Backups of the entire instance, which includes the Neo4j database and Shopping Stories website are entirely managed by AWS Backup. AWS Backup was chosen for the high reliability and simplicity that it offers.  


**NOTE:**
* *Server backups will continue uninterrupted even if the instance is stopped.*  
*  *If you find any of these images too small:* **right-click>Open image in new tab** *to view the full-size image.*
## Restoring a Backup
So you made an oof and need to restore the instance to a happier time? No problem!  

What you are about to do is create a *new* instance from one of the backups. You are then going to transfer a few different configurations within AWS to that new instance, but fret not! Just follow this guide step-by-step and it should be a breeze!



#### 1. First you need to navigate to the AWS Backup dashboard and click 'Restore Backup'.
![dashboard_restore](./images/backup/backup-dash-restore.png)
#### 2. Select the instance you are attempting to restore a backup from. (*There should only be one*)
![backup_protected](./images/backup/backup-protected.png)
#### 3. Select the backup you want to restore from and click 'Restore'.
![resource_details](./images/backup/backup-restore-button.png)
#### 4. All you should need to do here is click 'Restore backup'. All of the settings should be valid by default. You can reference the screen shot below to ensure they are correct, though.
![restore_options](./images/backup/backup-restore-options.png)
#### 5. You will then be directed to the 'Restore jobs' page.
![restore_jobs_1](./images/backup/backup-restore-jobs.png)
#### 6. The restore job should change from 'pending' to 'running' to 'completed'. This process generally takes several minutes; you may need to refresh the page to see the status update.
![restore_jobs_2](./images/backup/backup-restore-jobs-complete.png)
#### 7. Now you need to navigate to the EC2 Dashboard and click on 'Instances'.
![EC2_dash](./images/backup/backup-ec2-Instances.png)
#### 8. After the backup's instance has successfully initialized and passed its status checks, you need to stop the previous instance. Click the checkbox next to the old instance and stop it.
![EC2_stop](./images/backup/backup-ec2-stop-previous.png)
#### 9. Then on the same old instance, select Actions>Networking>Disassociate Elastic IP address
![EC2_diss](./images/backup/backup-ec2-dissociate-elastic.png)
#### 10. Now navigate the EC2 sidebar to Elastic IPs and select Allocate Elastic IP address (*There should only be one*)
![EC2_EIP_Side](./images/backup/backup-elastic-IP.png)
#### 11. Now select the newly created instance and click 'Associate'.
![EC2_EIP_Side](./images/backup/backup-elastic-assign.png)


### *DO NOT FORGET TO ASSIGN A BACKUP POLICY TO THE NEW INSTANCE!!!*
![Assign_new_backup_target](./images/backup/backup-assign-backup.png)
Congratulations! You've successfully restored the server!  
Now check out *[Launch Services](../ec2/launch_services.md)* to get all of the services up and running.

## Backup Policy

In order to reduce the amount of used space and consequently save on costs, the current number of backup iterations is set to 5.

#### To change the backup settings, navigate to the AWS Backup dashboard and go to Backup Plans. Below is the view of the existing "Neo4j-Daily".
![Backup_daily](./images/backup/policy/backup-daily.png)
#### Selecting the radio button and clicking edit will bring you here, where you can change the settings of the backup plan.
![Backup_daily](./images/backup/policy/backup-plan.png)
