# Installing Git on Ubuntu

This page will review the process for installing git on a Linux/Ubuntu EC2 instance.

## Checking Git Version

First, make sure Git is not already installed on the system

```sh
ubuntu@ip-172-31-40-249:~$ git --version
```

That command will return the version of Git, should it exist.

```sh
git version 2.17.1
```

## Installation

To install on the instance, you must use the `sudo` command

```sh
ubuntu@ip-172-31-40-249:~$ sudo apt install git
```

If Git is already installed on the system, the console will simply spit out what version you have and be done with it.

```sh{4}
Reading package lists... Done
Building dependency tree
Reading state information... Done
git is already the newest version (1:2.17.1-1ubuntu0.7).
git set to manually installed.
0 upgraded, 0 newly installed, 0 to remove and 2 not upgrade.
```

Otherwise, simply follow the commands given by the console. You can verify Git was successfully installed by running the `git --version` command again. 
