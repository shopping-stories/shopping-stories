# Installing Node on Ubuntu

This guide outlines the procedure for installing Node.js on the Linux/Ubuntu EC2 instance.

::: tip Note
The team pretty much followed this [guide](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html) word for word
:::

## NVM

First, install the node version manager (nvm) by typing the following on the command line

```sh
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
```

::: tip Note
We use nvm here to install Node.js because nvm can install multiple versions, and allows you to switch between them at ease
:::

Activate nvm by entering the following on command line

```sh
.~/.nvm/nvm.sh
```

Congrats! You should have nvm installed on your instance.

## Node.js

Now that nvm is installed on the system, simply type the following command to install Node.js

```sh
nvm install node
```

::: tip Note
Installing Node.js also installs npm, the Node Package Manager, which allows you to install future additional modules as needed
:::

Once that command has completed, go ahead and verify Node.js is correctly installed and running by typing the following on the command line

```sh
node -e "console.log('Running Node.js ' + process.version)"
```

This should spit out the version of Node currently running on your system, which means you have successfully installed Node.js! Yay!

::: warning Note
This installation, like all others, only exist on this particular EC2 instance. This process will have to be repeated should you create a new EC2 instance
:::
