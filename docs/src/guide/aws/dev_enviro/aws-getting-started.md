
# Getting Started

*This section is intended for the creation of new instances.*  

Everything covered in this section has already been done for the EC2 instance running the Neo4j server & website. Should you find yourself needing to create additional instances, the following may expedite the initialization.
