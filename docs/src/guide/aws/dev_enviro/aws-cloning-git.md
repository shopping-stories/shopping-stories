# Cloning a Git Repository

Once you verify Git is installed in your system, you can then clone your target repository into whatever directory you want. In this example, we cloned our repository into the home folder.

## Git Clone

Use the `git clone` command, followed by the URL of your repository.

```sh
ubuntu@ip-172-31-40-249:~$ git clone https://gitlab.com/shopping-stories/shopping-stories
Cloning into 'shopping-stories'...
warning: redirecting to https://gitlab.com/shopping-stories/shopping-stories.git/
remote: Enumerating objects: 31, done.
remote: Counting objects: 100% (31/31), done.
remote: Compressing objects: 100% (31/31), done.
remote: Total 1318 (delta 15), reused 0 (delta 0), pack-reused 1287
Receiving objects: 100% (1318/1318), 75.32 MiB | 31.15 MiB/s, done.
Resolving deltas: 100% (596/596), done.
```

## Verify

Once the cloning process is complete, you can verify that your repository exists in your current directory by typing the `ls` command.

Then, simply `cd` into the repository

```sh
ubuntu@ip-172-31-40-249:~$ cd shopping-stories
ubuntu@ip-172-31-40-249:~/shopping-stories$
```
