# <img src="./images/cognito/cognito-400x400.png" width="30" /> Cognito

AWS's Cognito Service is where all of the user information lives. Between the sheer amount of documentation and the API's that the service comes with, the team decided it was a good decision to house the very important and sensitive user data with Cognito rather than hacking together a credential service in the Express App.

## Cognito User Pools

When accessing Cognito through the AWS Management console, you will be presented with two options: User Pools and Federated Identities.

User Pools are where the users and their information/attributes live. It is also where the Client ID lives, as well as various customizable Cognito-specific attributes.

![Cognito Console](./images/cognito/cognito-portal.png)

::: tip
It is important to make sure you are in the proper region. At the time of this writing, the User Pool currently in use is set in N. Virgina. You can verify by checking the dropdown menu at the top right of the page next to your username:

![Region Dropdown](./images/cognito/region-dropdown.png)
:::

## shoppingstories User Pool

The User Pool currently in use at the time of this writing is called `shoppingstories`.

![User Pool](./images/cognito/userpools.png)

Clicking on that leads you into the User Pool Management Console for `shoppingstories`, where you will find access to all of the Cognito functionality.

![User Pool Console](./images/cognito/cognito-console.png)

### Attributes

There are various attributes associated with user account creation. We require that all users have a unique username as well as an email address. We also allow users to sign in with either, as long as the email address is **verified**.

All other attributes are optional. Currently the website requires:
- First Name
- Last Name
- Username
- Email Address
- Password
- Confirm Password

But the names are optional in Cognito.

![User Pool Attributes](./images/cognito/cognito-attributes.png)

### App Client

This part is important and is part of a process of connecting the `shoppingstories.org` website to this Cognito User Pool.

The `App Client ID` is one of the credentials grabbed by the Vue and Express apps in order for the API calls to function properly.  

*blur photos!!!!!*

![User Pool App Client](./images/cognito/cognito-app-client.png)

![User Pool Client Settings](./images/cognito/cognito-client-settings.png)

### Email Settings

We currently use a customized email address for all `shoppingstories.org` related functions. Whenever a user requests a password change or creates a new account, `hri@historyrevealed.co` is the email address that serves any verification codes or links. New email addresses can be provided, but must first be verified by AWS as part of their spam prevention measures.

To verify a new address, simply click the `Verify an SES identity` link on this page:

![User Pool Email](./images/cognito/cognito-email.png)

You can also customize the messages that are sent by `hri@historyrevealed.co`. See below:

![User Pool Customize Email](./images/cognito/cognito-customize-message.png)

### Cognito Domain

In addition to Cognito features being used by `shoppingstories.org`, Cognito also provides a domain for authorizations. Though it is not used, it is nice to have an additional option for user logins/creations. The current domain can be seen at https://shoppingstories.auth.us-east-1.amazoncognito.com, however it is not currently implemented.

![User Pool Domain](./images/cognito/cognito-domain.png)

You can customize the domain as well, though options are limited:

![User Pool UI](./images/cognito/cognito-ui.png)

### Users and Groups

Here is where you can manage users and groups. All the features the Admin has access to on the website, as well as additional ones, can be accessed here. As such, it is important to think twice about changing an active user's account.

![User Pool Users](./images/cognito/cognito-users.png)

You can also create and edit groups in the `Groups` tab:

![User Pool Groups](./images/cognito/cognito-groups.png)

### Policies

We currently implement a fairly strict password policy. A password must be at least 8 characters long and have at least one of the following:

- Number
- Special Character
- Uppercase Letters
- Lowercase Letters

That is for the safety of the users. Although nothing of value is saved on the website currently, it is best security practices to implement strong authorization standards.

![User Pool Policies](./images/cognito/cognito-policies.png)

### Triggers

Currently not implemented, Triggers allow extra functionality. More information can be found [here](https://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-identity-pools-working-with-aws-lambda-triggers.html).

![User Pool Triggers](./images/cognito/cognito-triggers.png)

### Security

Cognito also provides extra security features that are currently not implemented, but you can read more about them [here](https://aws.amazon.com/blogs/security/how-to-use-new-advanced-security-features-for-amazon-cognito-user-pools/).

![User Pool Security](./images/cognito/cognito-security.png)

### Identity Providers

Identity Providers allows users to sign in via their favorite apps. This feature is not currently implemented due to time constraints but the idea is worth investment of future teams. Learn more about the process [here](https://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-pools-identity-provider.html).

![User Pool Identity Providers](./images/cognito/cognito-id-providers.png)

### Multi-Factor Authorization

Multi-Factor Authorization is currently set for account creation only. A user must verify their email address via a link sent by `hri@historyrevealed.co` after they create an account before being able to sign into `shoppingstories.org`.

However, Cognito allows MFA for signing into services as well, not just after account creation. This is currently not implemented but more information can be found [here](https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-settings-mfa.html).

![User Pool MFA](./images/cognito/cognito-mfa.png)

## Federated IDs

Federated IDs are very important and is part of the process when connecting the Vue and Express apps with AWS Cognito.

You can reach this page back at the Cognito Console, where your options were `User Pools` and `Federated Identities`. Currently, there is one Federated Identity called `shoppingstories`.

![User Pool FedIDs Console](./images/cognito/cognito-federated-console.png)

By clicking on `Sample Code` on the sidebar of the page, you can find the Identity Pool ID needed for the `web/.env` and `server/.env` files.

![User Pool FedID](./images/cognito/cognito-federated-id.png)

## Cognito Credentials

For the Express and Vue Apps to work properly with Cognito, the proper credentials must be added to their respective `.env` files.

Here's `server/.env`
```.env
# Cognito Credentials
USER_POOL_ID="XXXX"
USER_POOL_REGION="XXXX"
AWS_ACCESS_KEY_ID="XXXX"
AWS_SECRET_KEY="XXXX"
```

and here's `web/.env`
```.env
# Cognito Credentials
VUE_APP_COGNITO_USERPOOLID="XXXX"
VUE_APP_COGNITO_IDENTITYPOOLID="XXXX"
VUE_APP_COGNITO_WEBCLIENTID="XXXX"
VUE_APP_COGNITO_REGION="XXXX"
```

- For the `USER_POOL_ID` and `VUE_APP_COGNITO_USERPOOLID`, that information can be found at the top of the `shoppingstories` User Pool Console Page, or General Settings Page.

- For the `USER_POOL_REGION` and the `VUE_APP_COGNITO_REGION` variables, they are both currently set to N. Virgina, which is `us-east-1`.

- For the `VUE_APP_COGNITO_WEBCLIENTID`, you can find that in the `App clients` tab under `General settings` on the `shoppingstories` User Pool page.

- For the `VUE_APP_COGNITO_IDENTITYPOOLID`, that can be found under the Federated Identities tab on the Cognito Home page, in the `shoppingstories` ID. The page is called `sample code` and is in the code sample under `Get AWS Credentials`.

- For the `AWS_ACCESS_KEY_ID` that can only be retried by the Admin.

- For the `AWS_SECRET_KEY` that can only be retried by the Admin.
