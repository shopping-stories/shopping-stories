# Setup Express App

This guide will help set up the Express app locally.

## Clone Repository

First step is cloning the Gitlab repository, which can be found on the top right corner of this guide as well as [here](https://gitlab.com/shopping-stories/shopping-stories).

Since Group 7 primarily used a Ubuntu bash shell, this guide will cover how to clone through Linux commands. It also assumes the reader has `git` already installed in their system. If not, or you would like other ways to clone the repo, click [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

The command below will clone the repository into your current directory, the result being a new directory called `shopping-stories`.

```bash
$ git clone https://gitlab.com/shopping-stories/shopping-stories
```

## Install Node Modules

Next step is installing all of the necessary node modules. Again, this guide assumes you have `node` installed on your system, as well as `npm`. If not, learn how to install these necessary resources [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

For installed the node modules for the Vue application, head into the new `shopping-stories` directory and do a `npm install`:

```bash
$ cd shopping-stories/server
$ npm install
```

This will install all of the necessary modules that are dictated in the `server/package.json` file.

## Creating The .env File

The `.env` file is where all of the environmental variables are housed. Here is a template:

```.env
# .env

# Designated Port
PORT="XXXX"

# Bearer Token
SECRET_TOKEN="XXXX"

# Cognito Credentials
USER_POOL_ID="XXXX"
USER_POOL_REGION="XXXX"
AWS_ACCESS_KEY_ID="XXXX"
AWS_SECRET_KEY="XXXX"

# Neo4j Browser Credentials
NEO4J_BOLT="bolt://browser.shoppingstories.org/"
NEO4J_USERNAME="XXXX"
NEO4J_PASSWORD="XXXX"
```

Set the `PORT` to whichever port is available. The Group 7 team primarily used port 3000.

The `SECRET_TOKEN` variable can be set to anything during development as long as it is the same token as the Vue App. Learn more about the Vue App setup [here](http://docs.shoppingstories.org/guide/vue/run-locally.html#creating-the-env-file).

The Cognito Credentials can be found in the Cognito AWS Management Portal, which the reader must have the necessary credentials to obtain. Read more about that [here](http://docs.shoppingstories.org/guide/aws/cognito.html#cognito-credentials). 

The Neo4j Browser Credentials are set to the Admin account, or in this case the `neo4j` account. That is subject to change. Ask Admin for the password.

## Running the Vue App

To run the Express App, an additional resource needs to be installed, `nodemon`. Read more about how to install [here](https://www.npmjs.com/package/nodemon).

When all the node modules are installed and the `.env` file is properly created, simply do `npm run start` in the `server` directory, which is the starting script found in `package.json`.

```bash
$ npm run start
```

Alternatively, PM2 can be used to run node processes in the background. Click [here](https://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/) to learn more about PM2.

```bash
$ pm2 start ecosystem.config.js
```

That will start a node process of the Vue application called `shoppingstories.org`. All processes that PM2 is currently handling can be viewed with by typing the command `pm2 list`.

Here are some additional PM2 commands:

- Restart shoppingstories.org
```bash
$ pm2 restart shoppingstories.org
```

- Stop shoppingstories.org
```bash
$ pm2 stop shoppingstories.org
```

- Delete shoppingstories.org
```bash
$ pm2 delete shoppingstories.orig
```

- Substitute 'shoppingstories.org' with the number attached
```bash
$ pm2 stop 0
```

## Conclusion

If everything goes well, the Express application can be viewed at `http://localhost:XXXX`, where `XXXX` is replaced with the port chosen during the creation of the `server/.env` file. Nodemon will also track any changes made while it is running, with the exception being any changes made to the `.env` file, where application has to completely restart.
