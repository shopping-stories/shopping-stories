# Auth

`server/auth/` is where the Express App handles tokens. It is a small directory but can be expanded upon in the future to make the Express App more secure.

```
server
├── auth
│   ├── authorization.js
│   └── index.js
```

## auth/index

`server/auth/index.js` is similar to `server/routes/index.js` in that it merely compiles all the functions that lives in the Auth directory together for one simple export.

```js
// server/auth/index.js

exports.auth = require('./authorization');
```

## auth/authorization.js

`server/auth/authorization.js` is where the functionality for checking the identity of the API request live. It will let the Express App know if the caller has proper credentials to access the API endpoints.

The `checkToken` function is called by `server/app.js` at the beginning of every API transaction. That means in order to test API's on an external system such as [Postman](https://www.postman.com/), grabbing the token in the `server/.env` file is necessary and must be accompanied on any API call.

The function returns a `boolean` value indicating if the client has valid access to the API endpoint.

To test without having the proper token, head over to [here](https://api.shoppingstories.org/api/v1/import/count) and see the message it gives you. (Hint: should be `{"error":"Forbidden!"}`).

```js
// server/auth/authorization.js

// checks token before any api call is completed
exports.checkToken = function (req, res, next) {

  // check for headers
  if (!req.headers)
    return false

  // check for token
  else if (!req.headers['authorization'])
    return false

  // grab token
  const auth = req.headers['authorization']
  const token = auth && auth.split(' ')[1]

  // if exists
  if (!token)
    return false

  // check if correct
  if (token !== process.env.SECRET_TOKEN)
    return false

  // user is authorized
  return true
}
```

## Notes on Security

In a perfect world, the Express App would create an Access Token everytime a user logs into the website, perhaps one that is associated with the Cognito token system. Right now, though, `authorization.js` is just checking for the existence of a token and if that token matches the secret token it currently has saved in the `server/.env` file. It is highly encourage that this new security feature be implemented as soon as possible in the future, especially as the website grows in scale and in user base.
