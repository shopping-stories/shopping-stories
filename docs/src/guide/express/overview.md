# Express App Overview

The API endpoints live in an Express-Node application found in the `server/` directory. Here are the contents of that directory:

```
server
├── app.js
├── auth
│   ├── authorization.js
│   └── index.js
├── bin
│   └── www
├── ecosystem.config.js
├── neo4j
│   └── dbUtils.js
├── package.json
├── package-lock.json
├── public
│   └── stylesheets
│       └── style.css
├── routes
│   ├── cognito.js
│   ├── import.js
│   ├── index.js
│   ├── neo4j.js
│   └── utils
│       ├── data-parser
│       │   ├── parseAccounts.js
│       │   ├── parseOrganizations.js
│       │   ├── parsePeople.js
│       │   ├── parsePlaces.js
│       │   ├── parser.js
│       │   └── parseTransactions.js
│       ├── import-driver
│       │   └── importDriver.js
├── tree.txt
└── views
    ├── error.jade
    ├── index.jade
    └── layout.jade
```

## App.js

`server/app.js` is where the Express App is effectively 'built'. It houses all the major building blocks of the app and forms them together to form the backend of the website. Here you will find most of the `npm` packages, the API endpoint routes, and the functions that process the routes.

Below is what the routing system looks like:

![Routing System](./images/app-routes.png)

When an API call is made to one of these API Endpoints, the Express App uses the corresponding functions that exist in `server/routes`.

## Top Level System Files

There are three top level files that relate to the Express App.

### server/package.json & server/package-lock.json

`server/package.json` is the starting point for the Express App. It houses the start scripts and the version of the npm packages the team has installed for use in the app. `server/package-lock.json` is auto-maintained by npm and maintains the specifics of the packages so that the app runs smooth on every computer.

![package.json](./images/package.png)

### server/ecosystem.config.js

`server/ecosystem.config.js` is a start script used by PM2, which is a Node manager. The team highly recommends utilizing this tool as it can simplify web development. More information can be found [here](https://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/).

![ecosystem.config.js](./images/ecosystem.png)

## Environmental Variables

When cloning this repository, the `server/.env` file will not appear even though it is needed to run the application both locally and in the production build. Here is a breakdown of what that file would look like:

```.env
# .env

# Designated Port
PORT="XXXX"

# Bearer Token
SECRET_TOKEN="XXXX"

# Cognito Credentials
USER_POOL_ID="XXXX"
USER_POOL_REGION="XXXX"
AWS_ACCESS_KEY_ID="XXXX"
AWS_SECRET_KEY="XXXX"

# Neo4j Browser Credentials
NEO4J_BOLT="bolt://browser.shoppingstories.org/"
NEO4J_USERNAME="XXXX"
NEO4J_PASSWORD="XXXX"
```


### Port

This portion of the `.env` file will tell the Express App which port to run on. This can be changed, just make sure to avoid running multiple Node instances on the same port.

```.env
# Designated Port
PORT="XXXX"
```


### Bearer Token

This portion of the `.env` file will tell the Express App what the secret Bearer token is that it should check for in all the incoming API requests. This token can be set locally to something different than what is running on the production build, as long as it matches the Bearer Token found in `web/.env`.

```.env
# Bearer Token
SECRET_TOKEN="XXXX"
```

### Cognito Credentials

This portion of the `.env` file will tell the Express App what credentials to use when connecting to AWS Cognito. Learn more about where to find these [here](docs.shoppingstories.org/guide/aws/cognito.html)

_doublechecklink_

```.env
# Cognito Credentials
USER_POOL_ID="XXXX"
USER_POOL_REGION="XXXX"
AWS_ACCESS_KEY_ID="XXXX"
AWS_SECRET_KEY="XXXX"
```

### Neo4j Browser Credentials

This portion of the `.env` file will tell the Express App what credentials to use when connecting with the Neo4j Browser. The browser lives in a subdomain of shoppingstories.org:

```link
browser.shoppingstories.org
```

The first line indicates the type of connection to create with the browser. Bolt is currently used as it is the most secure.

```.env
# Neo4j Browser Credentials
NEO4J_BOLT="bolt://browser.shoppingstories.org/"
NEO4J_USERNAME="XXXX"
NEO4J_PASSWORD="XXXX"
```

## Routes Directory

This is where the corresponding functions live that process the API requests.

```
server
├── routes
│   ├── cognito.js
│   ├── import.js
│   ├── index.js
│   ├── neo4j.js
│   └── utils
│       ├── data-parser
│       │   ├── parseAccounts.js
│       │   ├── parseOrganizations.js
│       │   ├── parsePeople.js
│       │   ├── parsePlaces.js
│       │   ├── parser.js
│       │   └── parseTransactions.js
│       ├── import-driver
│       │   └── importDriver.js
```

### routes/index.js

`server/routes/index.js` compiles together all the functions that are exported to `server/app.js`.


![index](./images/index.png)

### routes/cognito.js

`server/routes/cognito.js` is where all of the functions that relate to AWS Cognito. These functions handle CRUD actions for users and Admin.


![cognito](./images/cognito.png)

### routes/import.js

`server/routes/import.js` is where all of the functions that relate to importing data. It also handles the S3 bucket where the uploaded files are stored.

![import](./images/import.png)

### routes/neo4j.js

`server/routes/neo4j.js` is where all of the functions that relate to the Search page on `shoppingstories.org`. These functions populate the tabs on the data table.

_insertneo4j.jspicture_

### routes/utils

`server/routes/utils/` is where all of the parsing functions live. They are called used by the functions inside `server/routes/import.js` and parse the Excel documents that are imported via the website. Read more about them [here](http://docs.shoppingstories.org/guide/backend/parser-getting-started.html#introduction)

_doublechecklink_

```
server
├── routes
│   └── utils
│       ├── data-parser
│       │   ├── parseAccounts.js
│       │   ├── parseOrganizations.js
│       │   ├── parsePeople.js
│       │   ├── parsePlaces.js
│       │   ├── parser.js
│       │   └── parseTransactions.js
│       ├── import-driver
│       │   └── importDriver.js
```

## Auth Directory

`server/auth/` is where the Express App handles tokens. It is a small directory but can be expanded upon in the future to make the Express App more secure.

```
server
├── auth
│   ├── authorization.js
│   └── index.js
```

### auth/index

`server/auth/index.js` is similar to `server/routes/index.js` in that it merely compiles all the functions that lives in the Auth directory together for one simple export.

![auth/index](./images/auth-index.png)

### auth/authorization.js

`server/auth/authorization.js` is where the functionality for checking the identity of the API request live. It will let the Express App know if the caller has proper credentials to access the API endpoints.

![auth/authorization](./images/auth-authorization.png)

## Bin Directory

The `server/bin/` is a small directory that houses the `www` that powers the Express App.

```
server
├── bin
│   └── www
```

### bin/www

`server/bin/www` is the file that powers up the Express App. It informs the app which port to use as well as various other functionalities.

![bin/www](./images/www.png)

## Neo4j Directory

_todo_

## Public & Views Directories

These directories simply inform the app of what pages should look like. These files are not utilized in the current version as this is merely for use of the shoppingstories.org website. However, in the future, it can be encouraged to flesh these out so that the app has a webpage that displays for clients or developers.

```
server
├── public
│   └── stylesheets
│       └── style.css
└── views
    ├── error.jade
    ├── index.jade
    └── layout.jade
```
