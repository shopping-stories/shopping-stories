# Document Parser

## Introduction

This piece of code is designed to extract data found in the Glassman & Company ledgers and plays a vital role in the process of database administration in regards to importing the data in a useable format.

There are several different functions designed to handel different file importation types. These types can be generalize into the following categories.

1. Transactions
2. Organizations
3. Accounts
4. People
5. Places

**Ledger Transactions** <br> The data parsed in transactions comes from the excel files with the following naming structure `dB_C_1760_xxx.xlsx`.

**Others** <br> The other categories come from the master persons list `c-1760-person-master-list.xlsx` where there is a collection of people, places, organizations, and accounts.

## Structure

### Ledger Transactions

Ledger transactions are made up of two components _transactions_, and _entries_. The parser extracts both of these components from the excel file. The following table describes the various attributes of found in the Excel file.

| Template            | Description                                                                           |
| :-------------------: | ------------------------------------------------------------------------------------- |
| reel                | Represents the microfilm that the data came from.                                     |
| folioPage           | Represents the physical page that the data was found on.                              |
| entryID             | Represents the individual line/transaction that took place.                           |
| storeName           | The name of store or the store location.                                              |
| ledgerYear          | Year where ledger was recorded.                                                       |
| marginalia          | Anything written in the margin of the source document.                                |
| prefix              | Prefix for account recipient.                                                         |
| accFirstName        | First name for the account recipient.                                                 |
| accLastName         | Last name for the account recipient.                                                  |
| suffix              | Suffix for the account recipient. Jr, Sr                                              |
| profession          | Profession for account recipient.                                                     |
| location            | Location for where transaction took place.                                            |
| reference           | Person,                                                                  |
| debitCredit (Dr/Cr) | Distinguishes credit or debit for the account.                                        |
| originalEntry (OO)  | Identifies the rows where there are multiple dates and transactions on one line       |
| year                | Year of transaction.                                                                  |
| month               | Month of transaction.                                                                 |
| day                 | Day of transaction.                                                                   |
| entry               | Broad range of items on a transaction.                                                |
| entryType           | Categorizes the entry with Items, Cash, Totals, Balance to/from, credit transactions. |
| folioReference      | Reference to another folio page                                                       |
| currencyType        | Categorizes currency into Tobacco, Sterling or Virginia.                              |
| currencyMeasure     | Measuring the currency type. ie: pounds (when Tobacco).                               |
| currencyParts       | Converted `<pounds>L<shilling>s<pence>` nomenclature from entry.                      |
| challenges          | This will be marked as 1 if there was any issues on the files.                        |
| comments            | Will describe the issues if challenges was set to one.                                |
| genMat              | Used to denote the presence of a person in the entry data.                            |

_Transactions_ <br>
Represents the physical line of data found on that particular page. These transactions can capture debits and credits, items, exchange of services, and payments to lenders.

_Entry_ <br> Represents the component(s) of that line of data. This can be items, interactions and-or services. There can be multiple entries per transaction made. Currently the parser only handles the item entries and the nuances associated with those.

### Organizations

| Template | Description |
| -------- | ----------- |
|  name         |  Name of the organization.                                |
|  associated   |  Association to the organization.                         |
|  prefix       |  Prefix of the organization.                              |
|  suffix       |  Any additional names added to the organization.          |
|  type         |  The type of organization.                                |
|  category     |  Categorization of organization.                          |
|  location     |  Where the organization is located.                       |
|  reference    |  Any additional information provided to the organization. |
|  variations   |  Different names of reference for the organization.       |
|  store        |  The store tied to the organization.                      |
|  account      |  Account tied to the organization.                        |

### Accounts

| Template | Description |
| -------- | ----------- |
|  name         |  Name of the account.                                     |
|  associated   |  Association to the account.                              |
|  prefix       |  Prefix of the account.                                   |
|  suffix       |  Any additional names added to the account.               |
|  type         |  The type of account.                                     |
|  category     |  Categorization of account.                               |
|  location     |  Where the account is located.                            |
|  reference    |  Any additional information provided to the account.      |
|  variations   |  Different names of reference for the account.            |
|  store        |  The store tied to the account.                           |
|  account      |  Account tied to the account.                             |

### People

| Template | Description |
| -------- | ----------- |
|  last_name    |   Last name of person.                                    |
|  first_name   |   First name of person.                                   |
|  prefix       |   Persons prefix.                                         |
|  suffix       |   Words that are attached to the persons name.            |
|  profession   |   The work the person does.                               |
|  profession_category  | What specific field they are in.                  |
|  profession_qualifier | Quality associated to the work.                   |
|  location     |  Location for where transaction occurred for this person. |
|  reference    | Any additional information provided to the person.        |
|  variations   | Different names of reference for the person.              |
|  store        | Where the person shopped.                                 |
|  gender       | Person's gender.                                          |
|  enslaved     | If they are enslaved or not.                              |
|  account      | Name of the persons account.                              |

### Places

| Template | Description |
| -------- | ----------- |
|  name         |  Name of the place.                                     |
|  associated   |  Association to the place.                              |
|  prefix       |  Prefix of the place.                                   |
|  suffix       |  Any additional names added to the place.               |
|  type         |  The type of place.                                     |
|  category     |  Categorization of place.                               |
|  location     |  Where the place is located.                            |
|  reference    |  Any additional information provided to the place.      |
|  variations   |  Different names of reference for the place.            |
|  store        |  The store tied to the place.                           |
|  account      |  Account tied to the place.                             |

## How to use

Each method in the parser expects an excel file to be passed in when the class is instantiated. Once the data has been parsed the method will return a `JSON` object containing the various components and their attributes.

### Create the Parser

Create a new object of the parser to access the various parsing routines. A new object will need to be used for each use of the parser. We recommend encapsulating the code to keep the number of objects to a minimum.

```javascript
const parse = new Parser(excelFileToParse);
```

### Transactions

Ledger transactions are broken down into two groups of information transaction, and entry. Both of the components are extracted from the data found in the Ledger Transactions.

```javascript
const parse = new Parser(excelFileToParse);

// Contains each transaction and individual items
// that make up each transaction.
const parsedTransactionData = parse.Transactions();
```

### Accounts

```javascript
const parse = new Parser(excelFileToParse);

// Contains all the accounts from the master-list
const parsedAccountData = parse.Accounts();
```

### Organizations

```javascript
const parse = new Parser(excelFileToParse);

// Contains all the organizations from the master-list
const parsedOrganizationData = parse.Organizations();
```

### People

```javascript
const parse = new Parser(excelFileToParse);

// Contains all the people from the master-list
const parsedPeopleData = parse.People();
```

### Places

```javascript
const parse = new Parser(excelFileToParse);

// Contains all the places from the master-list
const parsedPlacesData = parse.Places();
```
