# import/master

Takes a passed Master List Excel file, uploads it into the 'shoppingstories' S3 bucket, and parses it for data. It then creates a Cypher query which will then be queried into the database. This route will hopefully catch any errors that occur during each stage and return the reason why. If an error occurs, the function will then delete the affiliated file from the S3 bucket for future attempts.

## HTTP PUT Method

```js
// server/app.js
app.put('/api/v1/import/master', upload.single("file"), routes.import.master)
```

## Parameters

**file:** The Excel file that is to be uploaded and parsed into the Neo4j database.

## Returns

Returns the error message if it is a syntax error, or if there is an unknown error with parsing the file.

```json
{
  status: 'String',
  message: 'String',
}
```

## Status Codes

**200:** Success!
**201:** File Syntax Error.
**202:** Unknown File Error.
**500:** Server Error.

## Function

```js
// server/routes/import.js

exports.master = async function (req, res, next) {
  console.log('>>>> Starting new master list process');
  var returnStatus = {status: '', message: ''}
  const params = {
    Bucket: 'shoppingstories',
    Key: req.file.originalname,
  }

  try {
    console.log(">>>> Grabbing " + req.file.originalname + ' from bucket. . .')
    const getObject = await s3.getObject(params).promise()

    const jsonPackage = new Parser(getObject.Body)
    console.log(">>>> Successfully converted into workbook")

    // send off to parser
    const jsonOrganizations = jsonPackage.Organizations()
    const jsonPlaces = jsonPackage.Places()
    const jsonPeople = jsonPackage.People()
    const jsonAccounts = jsonPackage.Accounts()

    // send off to import driver to build query
    await Driver.accounts(jsonAccounts)
    await Driver.people(jsonPeople)
    await Driver.places(jsonPlaces)
    await Driver.organizations(jsonOrganizations)
    console.log(">>>> File successfully parsed, running query. . .")

    // run the query
    returnStatus = await Driver.runQuery();
    if (returnStatus.status != 200) {
      console.log(">>>> Error in upload, deleting " + params.Key + " from S3 bucket. . .")
      await s3.deleteObject(params).promise()
      return res.status(returnStatus.status).send(returnStatus.message)
    }
    else {
      console.log(">>>> File successfully uploaded to database!")
      return res.status(returnStatus.status).send(returnStatus.message)
    }

  } catch(err) {
      console.log(">>>> ERROR: ", err)
      await s3.deleteObject(params).promise()
      return res.status(500).send(err);
  }
}
```
