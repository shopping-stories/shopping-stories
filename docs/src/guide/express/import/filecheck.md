# import/filecheck

Checks for the existence of a file in the S3 bucket. 

## HTTP POST Method

```js
// server/app.js
app.post('/api/v1/import/filecheck', routes.import.filecheck)
```

## Parameters

**name:** The name of the file to be checked.

## Returns

Returns a Boolean value indicating the existence of the file in the associated S3 bucket.

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/import.js

exports.filecheck = async function (req, res, next) {

  console.log(">>>> Checking if file exists before upload. . .")

  const params = {
    Bucket: 'shoppingstories',
    Key: req.body.name,
  }

  try {

    await s3.getObject(params, function(err, data) {
      if (err) {
        console.log(">>>> File did not exist, proceeding with upload. . .")
        res.status(200).send({value: false})
      } else {
        console.log(">>>> File exists, will not upload")
        res.status(200).send({value: true})
      }
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err)
  }
}
```
