# import/count

Grabs the count of all the nodes that are currently in the Neo4j database.

## HTTP GET Method

```js
// server/app.js
app.get('/api/v1/import/count', routes.import.count)
```

## Parameters

None

## Returns

Returns a JSON package containing the total number of nodes in the Neo4j database.

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/import.js

exports.count = function (req, res, next) {

  let currentDate = dbUtils.getCurrentDate();
  const cypher = 'MATCH (n) RETURN count(n) as count';
  req.body.createdDate = currentDate;
  req.body.updatedDate = currentDate;
  const session = dbUtils.getSession(req);
  session.run(cypher)
    .then(result => {
      // on result, get count from first record
      const count = result.records[0].get('count');
      // send response
      res.status(200).send({count: count.toNumber()});
    })
    .catch(e => {
      // output error
      res.status(500).send(e);
    })
    .then(() => {
      // close session
      return session.close();
    });
}
```
