# import/download

Grabs the requested file for the client to download. It returns in the form of a data array that will need to be converted to a blob.

## HTTP GET Method

```js
// server/app.js
app.get('/api/v1/import/file/download', routes.import.downloadFile)
```

## Parameters

**filename:** The name of the file that will be downloaded by the client.

## Returns

If successful, an array of data that will be converted into a blob, and then eventually an Excel file. If errored,
an error code.

## Status Codes

**200:** Success!
**500:** Server Error.
**501:** File Did Not Exist.

## Function

```js
// server/routes/import.js

exports.downloadFile = async function (req, res, next) {

  console.log(">>>> Grabbing " + req.query.filename + " from S3 bucket. . .")

  const params = {
    Bucket: 'shoppingstories',
    Key: req.query.filename,
  }

  try {

    await s3.getObject(params, function(err, data) {
      if (err) {
        console.log(">>>> File did not exist. . .")
        res.status(501).send(err)
      } else {
        console.log(data)
        console.log(">>>> File exists, will send file")
        res.status(200).send(data.Body)
      }
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err)
  }
}
```
