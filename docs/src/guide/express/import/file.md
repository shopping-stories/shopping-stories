# import/file

Deletes the file associated with the name that is passed, should it exist in the 'shoppingstories' S3 bucket.

## HTTP DELETE Method

```js
// server/app.js
app.delete('/api/v1/import/file', routes.import.deleteFile)
```

## Parameters

**filename:** The name of the file to be deleted from the S3 bucket.

## Returns

None

## Status Codes

**200:** Success!
**300:** File was not found in S3 bucket.
**500:** Server Error.

## Function

```js
// server/routes/import.js

exports.deleteFile = async function (req, res, next) {
  console.log(">>>> Deleting file " + req.query.filename + ". . .")

  const params = {
    Bucket: 'shoppingstories',
    Key: req.query.filename,
  }

  try {

    await s3.deleteObject(params, function(err, data) {
      if (err) {
        console.log(">>>> File did not exist!")
        res.status(300)
      } else {
        console.log(">>>> File successfully deleted!")
        res.status(200)
      }
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err)
  }
}
```
