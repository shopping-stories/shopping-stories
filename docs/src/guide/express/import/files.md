# import/files

Grabs all the uploaded files in the 'shoppingstories' S3 bucket as well as the dates they were created. 

## HTTP GET Method

```js
// server/app.js
app.get('/api/v1/import/files', routes.import.files)
```

## Parameters

None

## Returns

A JSON package containing the names of all the files in the associated S3 bucket, as well as the time and date each file was uploaded.

```json
{
  file: 'string',
  time: 'string',
}
```

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/import.js

exports.files = function (req, res, next) {
  // create message object
  const message = []
  const params = {
    Bucket: 'shoppingstories',
  }

  console.log(">>>> Grabbing files....")

  s3.listObjects(params).promise()
  .then((data) => {
    data.Contents.forEach(file => {
      const format = new Date(file.LastModified)
      message.push({file: file.Key, time: format.toLocaleString()})
    })
    res.status(200).send({message: message})
  })
  .catch((err) => {
    console.log(err, err.stack);
    res.status(500).send(err)
  })
}
```
