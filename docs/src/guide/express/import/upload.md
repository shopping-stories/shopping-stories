# import/upload

Uploads a file to the 'shoppingstories' S3 bucket without first running through the parser scripts. This file and its contents will not be added to the database. Also checks to make sure the file made it to the bucket.

## HTTP PUT Method

```js
// server/app.js
app.put('/api/v1/import/file/upload', upload.single("file"), routes.import.uploadFile)
```

## Parameters

**file:** The file that is to be uploaded to the S3 bucket.

## Returns

Error message if it exists.

## Status Codes

**200:** Success!
**500:** Server Error.
**501:** Upload Error.

## Function

```js
// server/routes/import.js

exports.uploadFile = async function (req, res, next) {
    console.log(req.file.originalname)
    console.log(">>>> Grabbing " + req.file.originalname + " from S3 bucket. . .")

    const params = {
      Bucket: 'shoppingstories',
      Key: req.file.originalname,
    }

    try {

      await s3.getObject(params, function(err, data) {
        if (err) {
          console.log(">>>> File did not exist. . .")
          res.status(501)
        } else {
          console.log(">>>> File exists!")
          res.status(200)
        }
      })
    } catch(err) {
      console.error(err)
      res.status(500).send(err)
    }
}
```
