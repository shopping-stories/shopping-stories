# cognito/user/delete

Deletes the user from the Cognito User Pool permanently. Admin use only. 

## HTTP DELETE Method

```js
// server/app.js
app.delete('/api/v1/cognito/user/delete', routes.cognito.deleteUser)
```

## Parameters

**Username:** The username of the user who will be deleted from the User Pool.

## Returns

None

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js

exports.deleteUser = function (req, res, next) {
  console.log(">>>> Deleting " + req.query.username + ". . .")
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.query.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminDeleteUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not delete")
      }
      else {
        console.log(">>>> Successfully deleted user")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}
```
