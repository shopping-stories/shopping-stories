# cognito/user

Grabs the attributes of a user. For Admin use only. 

## HTTP GET Method

```js
// server/app.js
app.get('/api/v1/cognito/user', routes.cognito.getUser)
```

## Parameters

**Username:** username of who to get attributes for.

## Returns

A JSON file of user attributes specific to the passed username.

## Status Codes

**200:** Success!
**500:** Server Error

## Function

```js
// server/routes/cognito.js
exports.getUser = function (req, res, next) {
  console.log(req.query.username)
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.query.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminGetUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error at endpoint")
      }
      else {
        resolve(data)
        res.send(JSON.stringify(data)).status(200);
      }
    })
  });

  return payLoad
}
```
