# cognito/user/password

Allows a user to update their password. For all users. 

## HTTP PATCH Method

```js
// server/app.js
app.patch('/api/v1/cognito/user/password', routes.cognito.changePassword)
```

## Parameters

**token:** The Bearer Token spawned when requester's page is rendered.
**previous:** The previous password that is currently associated with the user's account.
**proposed:** The proposed password that the user wishes to use.

## Returns

None

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js

exports.changePassword = function (req, res, next) {
  console.log(">>>> Updating user's password")
  var params = {
    AccessToken: req.body.token,
    PreviousPassword: req.body.previous,
    ProposedPassword: req.body.proposed,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.changePassword(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}
```
