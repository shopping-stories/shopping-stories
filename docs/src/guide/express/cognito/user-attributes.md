# cognito/user/attributes

Allows a user to edit their own attributes. 

## HTTP PATCH Method

```js
// server/app.js
app.patch('/api/v1/cognito/user/attributes', routes.cognito.editUser)
```

## Parameters

**token:** The Access Token associated with the specific user.
**attributes:** The updated attributes that the user has edited.

## Returns

None

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js

exports.editUser = function (req, res, next) {
  console.log(">>>> Updating user " + req.body.username + " attributes. . .")
  var params = {
    AccessToken: req.body.token,
    UserAttributes: req.body.attributes,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.updateUserAttributes(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}
```
