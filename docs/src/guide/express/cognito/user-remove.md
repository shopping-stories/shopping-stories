# cognito/user/remove

Removes a user from a specified group. Admin use only. 

## HTTP PATCH Method

```js
// server/app.js
app.patch('/api/v1/cognito/user/remove', routes.cognito.removeGroup)
```

## Parameters

**username:** The username of the user who will be removed from the group.
**group:** The name of the group that the user will be removed from.

## Returns

None

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js

exports.removeGroup = function (req, res, next) {
  console.log(">>>> Removing " + req.body.username + " from Research group. . .")
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.body.username,
    GroupName: req.body.group,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminRemoveUserFromGroup(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not remove user from group")
      }
      else {
        console.log(">>>> Successfully removed user")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}
```
