# cognito/user/groups

Grabs a list of groups for associated username. For Admin use only.

## HTTP GET Method

```js
// server/app.js
app.get('/api/v1/cognito/user/groups', routes.cognito.getGroupsForUser)
```

## Parameters

**Username:** username of who to get groups from.

## Returns

Array of groups user is attached to.

## Status Codes

**200:** Success
**500:** Server Error

## Function

```js
// server/routes/cognito.js

exports.getGroupsForUser = function (req, res, next) {

  // get username from req
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.query.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminListGroupsForUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error at endpoint")
      }
      else {
        resolve(data)
        res.send(JSON.stringify(data)).status(200);
      }
    })
  });

  return payLoad
}
```
