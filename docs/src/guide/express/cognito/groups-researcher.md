# cognito/groups/researcher

Grabs the list of all users associated with the Researcher group. Admin use only.

## HTTP GET Method

```js
// server/app.js
app.get('/api/v1/cognito/groups/researcher', routes.cognito.getResearchGroup)
```

## Parameters

None

## Returns

A JSON file containing all users in the Researcher group in the associated Cognito User Pool, as well as each user's related attributes.

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js
exports.getResearchGroup = function (req, res, next) {
  var params = {
    UserPoolId: USER_POOL_ID,
    GroupName: "Researcher",
  }

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.listUsersInGroup(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error at endpoint")
      }
      else {
        resolve(data)
        res.send(JSON.stringify(data)).status(200);
      }
    })
  });

  return payLoad
}
```
