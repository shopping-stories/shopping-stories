# cognito/user/confirm

Changes an UNCOMFIRMED user to CONFIRMED. Admin use only. 

## HTTP PATCH Method

```js
// server/app.js
app.patch('/api/v1/cognito/user/confirm', routes.cognito.confirmUser)
```

## Parameters

**username:** The username of the user who is to be confirmed.

## Returns

None

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js

exports.confirmUser = function (req, res, next) {
  console.log(">>>> Confirming " + req.body.username + ". . .")

  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.body.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminConfirmSignUp(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not confirm")
      }
      else {
        console.log(">>>> Successfully confirmed user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}
```
