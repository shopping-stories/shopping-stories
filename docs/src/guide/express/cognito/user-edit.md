# cognito/user/edit

Updates a user's attributes. Admin use only. 

## HTTP PATCH Method

```js
// server/app.js
app.patch('/api/v1/cognito/user/edit', routes.cognito.editUserAdmin)
```

## Parameters

**username:** The username of the user whose attributes will be changed.
**attributes:** The updated attributes of the user.

## Returns

None

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js

exports.editUserAdmin = function (req, res, next) {
  console.log(">>>> Updating user " + req.body.username + " attributes. . .")
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.body.username,
    UserAttributes: req.body.attributes,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminUpdateUserAttributes(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}
```
