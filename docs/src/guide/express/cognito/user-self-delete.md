# cognito/user/self/delete

Allows a user to delete their own account from the Cognito User Pool permanently.

## HTTP PATCH Method

```js
// server/app.js
app.patch('/api/v1/cognito/user/self/delete', routes.cognito.deleteSelf)
```

## Parameters

**token:** The Access Token that is spawned when the user logs into the website.

## Returns

None

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js

exports.deleteSelf = function (req, res, next) {
  console.log(">>>> Deleting user. . .")
  var params = {
    AccessToken: req.body.token,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.deleteUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send(data);
      }
    })
  });

  return payLoad
}
```
