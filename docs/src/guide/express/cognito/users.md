# cognito/users

Grabs all of the users inside of the corresponding Cognito User Pool. For Admin use only.

## HTTP GET Method

```js
// app.js
app.get('/api/v1/cognito/users', routes.cognito.getUsers)
```

## Parameters

None

## Returns

A JSON file containing the users that exist in the Cognito User Pool as well as their attributes.

## Status Codes

**200:** Success!
**500:** Server Error

## Function

```js
exports.getUsers = function (req, res, next) {
  var params = {
    UserPoolId: USER_POOL_ID,
    AttributesToGet: [
      'name',
      'email',
    ],
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.listUsers(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error at endpoint")
      }
      else {
        resolve(data)
        res.send(JSON.stringify(data)).status(200);
      }
    })
  });

  return payLoad
}
```
