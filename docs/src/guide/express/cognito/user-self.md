# cognito/user/self

Grabs the information of the user who is currently logged in.

## HTTP Method

```js
// server/app.js
app.patch('/api/v1/cognito/user/self', routes.cognito.getSelf)
```

## Parameters

**token:** The Access Token that is spawned when the user logs into the website.

## Returns

A JSON package containing the user's information and attributes.

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js

exports.getSelf = function (req, res, next) {
  console.log(">>>> Grabbing user information. . .")
  var params = {
    AccessToken: req.body.token,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.getUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could find user.")
      }
      else {
        console.log(">>>> Successfully obtained data!")
        resolve(data)
        res.status(200).send(data);
      }
    })
  });

  return payLoad
}
```
