# cognito/user/resend

Allows a user to request a new confirmation email to be sent to the email the user is affiliated with.

## HTTP PATCH Method

```js
// server/app.js
app.patch('/api/v1/cognito/user/resend', routes.cognito.resendConfirmation)
```

## Parameters

**id:** The ID string associated with the user's Cognito account.
**username:** The username of the user.

## Returns

None

## Status Codes

**200:** Success!
**500:** Server Error.

## Function

```js
// server/routes/cognito.js

exports.resendConfirmation = function (req, res, next) {
  console.log(">>>> Resending confirmation email to " + req.body.username + ". . .")
  var params = {
    ClientId: req.body.id,
    Username: req.body.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.resendConfirmationCode(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}
```
