# cognito/user/join

Adds the user to a specified group that exists in the associated Cognito User Pool. Admin use only.

## HTTP PATCH Method

```js
// server/app.js
app.patch('/api/v1/cognito/user/join', routes.cognito.joinGroup)
```

## Parameters

**username:** The username of the user who will be added to the group.
**group:** The group in which the user will be added to.

## Returns

None

## Status Codes

**200:** Success!
**500:** Server error.

## Function

```js
// server/routes/cognito.js

exports.joinGroup = function (req, res, next) {
  console.log(">>>> Adding " + req.body.username + " to Research group. . .")
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.body.username,
    GroupName: req.body.group,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminAddUserToGroup(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not add to group")
      }
      else {
        console.log(">>>> Successfully added user")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}
```
