const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Shopping Stories Docs',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: 'https://gitlab.com/shopping-stories/shopping-stories',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: true,
    nav: [
      {
        text: 'Guide',
        link: '/guide/',
      },
      {
        text: 'Config',
        link: '/config/'
      },
      {
        text: 'VuePress',
        link: 'https://v1.vuepress.vuejs.org'
      }
    ],
    sidebar: {
      '/guide/': [
        {
          title: 'Introduction',
          collapsable: false,
          children: [
            'intro/intro',
            'intro/use-guide',
            'intro/add-guide',
            'intro/get-started',
          ]
        },
        {
          title: 'Neo4j',
          collapsable: true,
          children: [
            'neo4j/neo4j-getting-started',
            'neo4j/neo4j-frequent-scripts',
            {
              title: 'Frequent Scripts',
              collapsable: true,
              children: [
                'neo4j/frequent/creating.md',
                'neo4j/frequent/saving.md',
                'neo4j/frequent/updating.md',
                'neo4j/frequent/deleting.md',
              ]
            },
            {
              title: 'Example Scripts',
              collapsable: true,
              children: [
                'neo4j/example/example-relationships.md',
                'neo4j/example/example-creation.md',
                'neo4j/example/example-search.md'
              ]
            },
          ]
        },
        {
          title: 'Front-End',
          collapsable: true,
          children: [
            'vue/overview',
            'vue/run-locally',
            {
              title: 'Views',
              collapsable: true,
              children: [
                'vue/views/about',
                'vue/views/acknowledgements',
                'vue/views/dashboard',
                'vue/views/forgot-password',
                'vue/views/home',
                'vue/views/ledgers',
                'vue/views/profile',
                'vue/views/project',
                'vue/views/search',
                'vue/views/signin',
                'vue/views/signup',
                'vue/views/team',
                'vue/views/transcription',
                'vue/views/verify',
              ]
            },
            {
              title: 'Components',
              collapsable: true,
              children: [
                'vue/components/appbar',
                'vue/components/browser',
                'vue/components/check-imports',
                'vue/components/docs',
                'vue/components/footer',
                'vue/components/import-data',
                'vue/components/snackbar',
                'vue/components/user-management',
                'vue/components/user-settings',
                'vue/overview',
                'vue/run-locally',
                'vue/VuetifyJS',
              ]
            },
            'vue/store',
            'vue/services',
            'vue/router',
          ]
        },
        {
          title: 'Back-End',
          collapsable: true,
          children: [
            'express/overview',
            {
              title: 'Cognito Routes',
              collapsable: true,
              children: [
                'express/cognito/users',
                'express/cognito/user-groups',
                'express/cognito/groups-researcher',
                'express/cognito/user',
                'express/cognito/user-delete',
                'express/cognito/user-join',
                'express/cognito/user-remove',
                'express/cognito/user-edit',
                'express/cognito/user-confirm',
                'express/cognito/user-attributes',
                'express/cognito/user-password',
                'express/cognito/user-resend',
                'express/cognito/user-self',
                'express/cognito/user-self-delete',
              ]
            },
            {
              title: 'Import Routes',
              collapsable: true,
              children: [
                'express/import/count',
                'express/import/filecheck',
                'express/import/files',
                'express/import/transactions',
                'express/import/master',
                'express/import/file',
                'express/import/upload',
                'express/import/download',
              ]
            },
            'express/auth',
            'express/run-local',
            'express/neo4j',
            'express/parser',
          ]
        },
        {
          title: 'Amazon Web Services (AWS)',
          collapsable: true,
          children: [
            'aws/aws-sign-in',
            {
              title: 'Initializing AWS Dev Enviroment',
              collapsable: true,
              children: [
                'aws/dev_enviro/aws-getting-started',
                'aws/dev_enviro/aws-installing-git',
                'aws/dev_enviro/aws-cloning-git',
                'aws/dev_enviro/aws-installing-node',
              ]
            },
            'aws/backup',
            'aws/certificate-manager',
            'aws/cognito',
            'aws/route53',
            'aws/s3',
          ]
        },
        {
          title: 'Server Management (EC2)',
          collapsable: true,
          children: [
            'ec2/config',
            'ec2/connect',
            'ec2/linuxcmds',
            'ec2/launch_services',
            {
              title: 'Preventative Maintenance',
              collapsable: true,
              children: [
                'ec2/storagemaintenance',
                'ec2/updating',
              ]
            },
            'ec2/nginx',
            'ec2/certificates',
          ]
        },
      ]
    },

    /**
     * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
     */
    plugins: [
      '@vuepress/plugin-back-to-top',
      '@vuepress/plugin-medium-zoom',
    ]
  }
}
