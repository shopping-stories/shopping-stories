---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: Documentation of the Shopping Stories Project. Created by the UCF 2020 Senior Design Team
actionText: Quick Start →
actionLink: /guide/
features:
- title: Front-End Documentation
  details: Vue App Details & Breakdown
- title: Database Documentation
  details: Frequently Used Scripts & Commands
- title: Back-End Documentation
  details: Express App API & Breakdown
footer: Made by Group 7 with ❤️
---
