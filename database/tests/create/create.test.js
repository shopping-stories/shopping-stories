const node = require('../../node_actions/create')


test('adds node with no props', () => {
    const p = 'Create (p:Person{})'
    console.log(p)
    expect(node('Person')).toMatch(p);
});
