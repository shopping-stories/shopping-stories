# How to dev

Download all current packages used in this directory.
```bash
npm install
```

How to liveload. 
```bash
nodemon dir/filename.js
```
