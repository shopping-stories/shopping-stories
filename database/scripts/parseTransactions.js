

/**
 * Expects a json object and returns a parsed json object
 * @param { JSON } obj 
 */
function parseTransactions(obj) {

    let transaction = {
        reel_id: obj.reel,
        entry_id: obj.entryId,
        folio_page: obj.folioPage,
        challenges: obj.challenges,
        gen_mat: obj.gen_mat === '1' ? 1 : undefined,
        store_name: obj.storeName,
        ledger_year: obj.ledgerYear,
        marginalia: obj.marginalia,
        prefix: obj.prefix,
        first_name: obj.accFirstname || obj.accFirstName,
        last_name: obj.accLastname || obj.accLastName,
        suffix: obj.suffix,
        profession: obj.profession,
        location: obj.location,
        reference: obj.reference,
        transaction_type: obj.debitCredit,
        original_entry: obj.originalEntry,
        entry: obj.entry,
        entry_type: obj.entryType,
        folio_reference: obj.folioReference,
        comments: obj.comments,
        date_created: getDate()
    }

    if (transaction.ledger_year) transaction.ledger_year = transaction.ledger_year.replace(/[A-Za-z*]/, '/')
    if (transaction.store_name) transaction.store_name = transaction.store_name.toLowerCase()
    if (transaction.prefix) transaction.prefix = transaction.prefix.toLowerCase()
    if (transaction.first_name) transaction.first_name = transaction.first_name.toLowerCase()
    if (transaction.last_name) transaction.last_name = transaction.last_name.toLowerCase()
    if (transaction.suffix) transaction.suffix = transaction.suffix.toLowerCase()
    if (transaction.profession) transaction.location = transaction.profession.toLowerCase()
    if (transaction.location) transaction.location = transaction.location.toLowerCase()
    if (transaction.transaction_type) {
        if (transaction.transaction_type === 'Dr') transaction.transaction_type = 'debit'
        if (transaction.transaction_type === 'Cr') transaction.transaction_type = 'credit'
    }
    if (transaction.profession) transaction.profession = transaction.profession.toLowerCase()
    if (obj.day && obj.month && obj.year) transaction.transaction_date = [obj.month, obj.day, obj.year].join('-')
    else transaction.year = obj.year
    if (transaction.entry_type) transaction.entry_type = transaction.entry_type.toLowerCase()


    // Parsing the currencyType
    if (obj.currencyType) transaction.currency_type = obj.currencyType.toLowerCase()

    // Populate the currencyMeasure
    if (transaction.currency_type) {
        transaction.currency_measure = []
        let temp = obj.currencyMeasure ? obj.currencyMeasure.toLowerCase().split(',') : undefined
        transaction.currency_type.split(',').forEach((element, i) => {
            if (element === 'sterling') transaction.currency_measure[i] = 'currency'
            else if (element === 'virginia') transaction.currency_measure[i] = 'currency'
            else if (transaction.currency_measure[i]) transaction.currency_measure = temp.shift()
            else transaction.currency_measure[i] = ''
        })
        transaction.currency_measure.toString()
    }

    // Populate the currencyParts
    try {
        transaction.currency_parts = obj.currencyParts.split(',')
        transaction.currency_parts.forEach((element, i, array) => {
            let currency = element.replace('L', ',')
            currency = currency.replace('s', ',')
            array[i] = currency
        })
    } catch (e) {
        transaction.currency_parts = parseFloat(obj.currencyParts) || undefined
    }

    return transaction

}

function parseEntries(obj) {

    let entry = {
        reel_id: obj.reel,
        folio_page: obj.folioPage,
        entry_id: obj.entryId,
        date_created: getDate()
    }

    if (obj.challenges) entry.challenges = obj.challenges
    if (obj.gen_mat) entry.gen_mat = obj.genMat

    // Used to determine the parsing routine 
    if (obj.originalEntry) entry.original_entry = 1

    // If there is a conflict/challenge then skip that row and move to the next
    if (entry.genMat || entry.challenges || entry.original_entry || obj.entry === undefined) {
        entry.entry = obj.entry
        entry.comments = obj.comments
        return [entry]
    }

    // Parsing the entries
    let ledgerItem = obj.entry.split(';')


    // For each item in the transaction
    let parsedItems = []
    ledgerItem.forEach(el => {
        el = el.replace(/\r?\n|\r/, '') // Removing stray excel file EOL chars
        el = el.trim()
        el = el.split(',')

        // Building each item on the transaction
        let item = {
            reel_id: obj.reel,
            folio_page: obj.folioPage,
            entry_id: obj.entryId,
            date_created: getDate()
        }

        // Populating the attributes 
        item.amount = parseFloat(el[0]) || undefined
        item.unit_measure = el[1] ? el[1].toLowerCase() : undefined
        item.descriptor = el[2] ? el[2].toLowerCase().replace('-', ',') : undefined
        item.name = el[3] ? el[3].toLowerCase() : undefined

        // Accounting for unit price
        if (el.length > 5) {
            item.unit_price = el[4] ? currencySplitter(el[4]) : el[4]
            item.item_price = el[5] ? currencySplitter(el[5]) : el[5]
        }
        else {
            item.item_price = el[4] ? currencySplitter(el[4]) : el[4]
        }

        // Pushing the parsed data
        parsedItems.push(item)
    })

    // returning array of parsed items 
    return parsedItems

}

// Parses the currency
function currencySplitter(currency) {
    currency = currency.replace('L', ',')
    currency = currency.replace('s', ',')
    return currency
}

// Returns a formatted date time stamp mm-dd-yyyy hh:mm:ss
function getDate() {
    let currentDate = new Date()
    return `${currentDate.getMonth()}-${currentDate.getDay()}-${currentDate.getFullYear()} ${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}`
}



function parseLedgerDriver(jsonObj) {

    let parsedJsonObj = {}

    parsedJsonObj['transactions'] = []
    parsedJsonObj['entries'] = []


    jsonObj.forEach(row => {

        // Pushing parsed data to JSON
        parsedJsonObj['transactions'].push(parseTransactions(row))


        // Adding the parsed array of data to the JSON
        parseEntries(row).forEach(el => {
            parsedJsonObj['entries'].push(el)
        })
    })

    // Returning the LedgerJson containing the Transaction and Entries
    return parsedJsonObj
}


module.exports = { parseLedgerDriver }