


function parsePeople(obj) {

    // Person object
    let person = {
        last_name: obj.LastName,
        first_name: obj.FirstName,
        prefix: obj.Prefix,
        suffix: obj.Suffix,
        profession: obj.Profession,
        profession_category: obj.ProfessionCategory,
        profession_qualifier: obj.ProfessionQualifier,
        location: obj.Location,
        reference: obj.Reference,
        variations: obj.Variations,
        store: obj.Store,
        gender: obj.Gender,
        enslaved: obj.Enslaved,
        account: obj.Account
    }


    // Data cleaning and validation
    if (person.last_name) person.last_name = person.last_name.toLowerCase()
    if (person.first_name) person.first_name = person.first_name.toLowerCase()
    if (person.prefix) person.prefix = person.prefix.toLowerCase()
    if (person.suffix) person.suffix = person.suffix.toLowerCase()
    if (person.profession) person.profession = person.profession.toLowerCase()
    if (person.profession_category) person.profession_category = person.profession_category.toLowerCase()
    if (person.profession_qualifier) person.profession_qualifier = person.profession_qualifier.toLowerCase()
    if (person.location) person.location = person.location.toLowerCase()
    if (person.reference) person.reference = person.reference.toLowerCase()
    if (person.variations) person.variations = person.variations.toLowerCase()
    if (person.store) person.store = person.store.toLowerCase()
    if (person.gender) person.gender = person.gender.toLowerCase()
    if (person.enslaved) person.enslaved = person.enslaved.toLowerCase()
    if (person.account) person.account = person.account.toLowerCase()

    return person
}


/**
 * Expects a json object and returns a parsed json object
 * @param { JSON } obj 
 */
function parsePeopleDriver(obj) {

    let parsedData = []

    obj.forEach(el => {

        // Determining if there is a person to parse.
        let toParse = el.Reference !== 'Organization'
        toParse = toParse && el.Reference !== 'Account'
        toParse = toParse && el.Reference !== 'Place'

        if (toParse) {
            parsedData.push(parsePeople(el))
        }
    })

    return parsedData
}

module.exports = { parsePeopleDriver }