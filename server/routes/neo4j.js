// routes/neo4j.js

const dbUtils = require('../neo4j/dbUtils');

exports.data = function (req, res, next) {
  let currentDate = dbUtils.getCurrentDate();
  const cypher = 'MATCH (n:Transaction) RETURN n';
  req.body.createdDate = currentDate;
  req.body.updatedDate = currentDate;

  // start new neo4j session
  const session = dbUtils.getSession(req);

  // run cypher query
  session.run(cypher)
    .then(result => {
      console.log("records: ", result.records[0]._fields[0].properties.reel_id)
      res.send(JSON.stringify(result.records)).status(200); // send response
    })
    .catch(e => {
      console.log("error: ", e); // output error
      res.status(500).send(e); // send response
    })
    .then(() => {
      return session.close(); // close session
    });
}
