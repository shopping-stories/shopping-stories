// routes/import.js

const dbUtils = require('../neo4j/dbUtils');
const xlsx = require('xlsx')
const fs = require('fs');
const Parser = require('./utils/data-parser/parser');
const importDriver = require("./utils/import-driver/importDriver")

// grab AWS credentials
const username = process.env.NEO4J_USERNAME
const password = process.env.NEO4J_PASSWORD
const url = process.env.NEO4J_BOLT

// create new import driver 
const Driver = new importDriver(username, password, url)

console.log(">>>> Connected to import driver")

// connect to AWS S3 account
const aws = require('aws-sdk')
aws.config.update({
  secretAccessKey: process.env.AWS_SECRET_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  region: process.env.USER_POOL_REGION,
});
s3 = new aws.S3();


// called by app.js
// simply returns the number of nodes in db
// for confirming connection more than anything
exports.count = function (req, res, next) {

  let currentDate = dbUtils.getCurrentDate();
  const cypher = 'MATCH (n) RETURN count(n) as count';
  req.body.createdDate = currentDate;
  req.body.updatedDate = currentDate;
  const session = dbUtils.getSession(req);
  session.run(cypher)
    .then(result => {
      // on result, get count from first record
      const count = result.records[0].get('count');
      // send response
      res.status(200).send({count: count.toNumber()});
    })
    .catch(e => {
      // output error
      res.status(500).send(e);
    })
    .then(() => {
      // close session
      return session.close();
    });
}

// called by app.js
// grabs the file names and creation dates
// 200 = success, 500 = error
exports.files = function (req, res, next) {

  console.log(req.query.bucket)

  // create message object
  const message = []
  const params = {
    Bucket: req.query.bucket,
  }

  console.log(">>>> Grabbing files....")

  s3.listObjects(params).promise()
  .then((data) => {
    data.Contents.forEach(file => {
      const format = new Date(file.LastModified)
      message.push({file: file.Key, time: format.toLocaleString()})
    })
    res.status(200).send({message: message})
  })
  .catch((err) => {
    console.log(err, err.stack);
    res.status(500).send(err)
  })
}

// called by app.js
// receives file name and checks path
// 200 + boolean = success, 500 = error
exports.filecheck = async function (req, res, next) {

  console.log(">>>> Checking if file exists before upload. . .")

  const params = {
    //Bucket: 'shoppingstories',
    Bucket: req.body.bucket,
    Key: req.body.name,
  }

  try {

    await s3.getObject(params, function(err, data) {
      if (err) {
        console.log(">>>> File did not exist, proceeding with upload. . .")
        res.status(200).send({value: false})
      } else {
        console.log(">>>> File exists, will not upload")
        res.status(200).send({value: true})
      }
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err)
  }
}

// called by app.js
// receives file name and deletes from bucket
// 200 = success, 300 = file does not exist, 500 = error
exports.deleteFile = async function (req, res, next) {
  console.log(">>>> Deleting file " + req.query.filename + ". . .")

  const params = {
    Bucket: 'shoppingstories',
    Key: req.query.filename,
  }

  try {

    await s3.deleteObject(params, function(err, data) {
      if (err) {
        console.log(">>>> File did not exist!")
        res.status(300).send()
      } else {
        console.log(">>>> File successfully deleted!")
        res.status(200).send()
      }
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err)
  }
}

// called by app.js
// receives file name and deletes from bucket
// 200 = success, 300 = file does not exist, 500 = error
exports.deleteFileDocuments = async function (req, res, next) {
  console.log(">>>> Deleting file " + req.query.filename + ". . .")

  const params = {
    Bucket: 'ss-documents',
    Key: req.query.filename,
  }

  try {

    await s3.deleteObject(params, function(err, data) {
      if (err) {
        console.log(">>>> File did not exist!")
        res.status(300).send()
      } else {
        console.log(">>>> File successfully deleted!")
        res.status(200).send()
      }
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err)
  }
}

// called by app.js
// receives a file to be uploaded to S3 bucket
// 200 = success, 500 = error, 501 = upload error
exports.uploadFile = async function (req, res, next) {
    console.log(req.file.originalname)
    console.log(">>>> Grabbing " + req.file.originalname + " from S3 bucket. . .")

    const params = {
      Bucket: 'shoppingstories',
      Key: req.file.originalname,
    }

    try {

      await s3.getObject(params, function(err, data) {
        if (err) {
          console.log(">>>> File did not exist. . .")
          res.status(501).send()
        } else {
          console.log(">>>> File exists!")
          res.status(200).send()
        }
      })
    } catch(err) {
      console.error(err)
      res.status(500).send(err)
    }
}

// called by app.js
// receives a file to be uploaded to S3 bucket
// 200 = success, 500 = error, 501 = upload error
exports.uploadFileDocuments = async function (req, res, next) {
  console.log(req.file.originalname)
  console.log(">>>> Grabbing " + req.file.originalname + " from S3 bucket. . .")

  const params = {
    Bucket: 'ss-documents',
    Key: req.file.originalname,
  }

  try {

    await s3.getObject(params, function(err, data) {
      if (err) {
        console.log(">>>> File did not exist. . .")
        res.status(501).send()
      } else {
        console.log(">>>> File exists!")
        res.status(200).send()
      }
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err)
  }
}

// TODO: all
exports.downloadFile = async function (req, res, next) {

  console.log(">>>> Grabbing " + req.query.filename + "from S3 bucket. . .")

  const params = {
    Bucket: 'shoppingstories',
    Key: req.query.filename,
  }

  try {

    await s3.getObject(params, function(err, data) {
      if (err) {
        console.log(">>>> File did not exist. . .")
        res.status(500).send(err)
      } else {
        console.log(data)
        console.log(">>>> File exists, will send file")
        res.status(200).send(data.Body)
      }
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err)
  }
}

exports.downloadFileDocuments = async function (req, res, next) {

  console.log(">>>> Grabbing " + req.query.filename + "from S3 bucket. . .")

  const params = {
    Bucket: 'ss-documents',
    Key: req.query.filename,
  }

  try {

    await s3.getObject(params, function(err, data) {
      if (err) {
        console.log(">>>> File did not exist. . .")
        res.status(500).send(err)
      } else {
        console.log(data)
        console.log(">>>> File exists, will send file")
        res.status(200).send(data.Body)
      }
    })
  } catch(err) {
    console.error(err)
    res.status(500).send(err)
  }
}


// called by app.js
// Step 1.) upload FormData package (done during call in app.js)
// Step 2.) Convert FormData file into Excel File
// Step 3.) Parse Excel file and convert to JSON packages
// Step 4.) Parse JSON package and convert to Cypher Query
// Step 5.) Run Cypher query on Neo4j via driver
// 200 = success, 201 = syntax error, 202 = unknown file error, 500 = server error
exports.master = async function (req, res, next) {
  console.log('>>>> Starting new master list process');
  var returnStatus = {status: '', message: ''}
  const params = {
    Bucket: 'shoppingstories',
    Key: req.file.originalname,
  }

  try {
    console.log(">>>> Grabbing " + req.file.originalname + ' from bucket. . .')
    const getObject = await s3.getObject(params).promise()

    const jsonPackage = new Parser(getObject.Body)
    console.log(">>>> Successfully converted into workbook")

    // send off to parser
    const jsonOrganizations = jsonPackage.Organizations()
    const jsonPlaces = jsonPackage.Places()
    const jsonPeople = jsonPackage.People()
    const jsonAccounts = jsonPackage.Accounts()

    // send off to import driver to build query
    await Driver.accounts(jsonAccounts)
    await Driver.people(jsonPeople)
    await Driver.places(jsonPlaces)
    await Driver.organizations(jsonOrganizations)
    console.log(">>>> File successfully parsed, running query. . .")

    // run the query
    returnStatus = await Driver.runQuery();
    if (returnStatus.status != 200) {
      console.log(">>>> Error in upload, deleting " + params.Key + " from S3 bucket. . .")
      await s3.deleteObject(params).promise()
      return res.status(returnStatus.status).send(returnStatus.message)
    }
    else {
      console.log(">>>> File successfully uploaded to database!")
      return res.status(returnStatus.status).send(returnStatus.message)
    }

  } catch(err) {
      console.log(">>>> ERROR: ", err)
      await s3.deleteObject(params).promise()
      return res.status(500).send(err);
  }
}

// called by app.js
// Step 1.) upload FormData package (done during call in app.js)
// Step 2.) Convert FormData file into Excel File
// Step 3.) Parse Excel file and convert to JSON package
// Step 4.) Parse JSON package and convert to Cypher Query
// Step 5.) Run Cypher query on Neo4j via driver
// 200 = success, 201 = syntax error, 202 = unknown file error, 500 = server error
exports.transactions = async function (req, res, next) {
  console.log('>>>> Starting new transaction process');
  var returnStatus = {status: '', message: ''}
  const params = {
    Bucket: 'shoppingstories',
    Key: req.file.originalname,
  }
  try {
    // grab object from S3 bucket
    console.log(">>>> Grabbing " + req.file.originalname + ' from bucket. . .')
    const getObject = await s3.getObject(params).promise()

    // convert to readable json workbook file
    const jsonPackage = new Parser(getObject.Body)
    console.log(">>>> Successfully converted into json workbook")

    // parse file contents
    const jsonTransactions = await jsonPackage.Transactions()
    await Driver.transactions(jsonTransactions)
    console.log(">>>> File successfully parsed, running query. . .")

    // run built query
    returnStatus = await Driver.runQuery()
    if (returnStatus.status != 200) {
      console.log(">>>> Error in upload, deleting " + params.Key + " from S3 bucket. . .")
      await s3.deleteObject(params).promise()
      return res.status(returnStatus.status).send(returnStatus.message)
    }
    else {
      console.log(">>>> File successfully uploaded to database!")
      return res.status(returnStatus.status).send(returnStatus.message)
    }
  } catch(err) {
      console.log(">>>> ERROR: ", err)
      await s3.deleteObject(params).promise()
      return res.status(500).send(err);
  }
}
