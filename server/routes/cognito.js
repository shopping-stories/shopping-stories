// routes/cognito.js

const AWS = require('aws-sdk');
const USER_POOL_ID = process.env.USER_POOL_ID;
const USER_POOL_REGION = process.env.USER_POOL_REGION;
const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID;
const AWS_SECRET_KEY = process.env.AWS_SECRET_KEY;

exports.getUsers = function (req, res, next) {
  var params = {
    UserPoolId: USER_POOL_ID,
    AttributesToGet: [
      'name',
      'email',
    ],
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.listUsers(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error at endpoint")
      }
      else {
        resolve(data)
        res.send(JSON.stringify(data)).status(200);
      }
    })
  });

  return payLoad
}

exports.getResearchGroup = function (req, res, next) {
  var params = {
    UserPoolId: USER_POOL_ID,
    GroupName: "Researcher",
  }

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.listUsersInGroup(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error at endpoint")
      }
      else {
        resolve(data)
        res.send(JSON.stringify(data)).status(200);
      }
    })
  });

  return payLoad
}

exports.getGroupsForUser = function (req, res, next) {

  // get username from req
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.query.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminListGroupsForUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error at endpoint")
      }
      else {
        resolve(data)
        res.send(JSON.stringify(data)).status(200);
      }
    })
  });

  return payLoad
}

exports.getUser = function (req, res, next) {
  console.log(req.query.username)
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.query.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminGetUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error at endpoint")
      }
      else {
        resolve(data)
        res.send(JSON.stringify(data)).status(200);
      }
    })
  });

  return payLoad
}

exports.deleteUser = function (req, res, next) {
  console.log(">>>> Deleting " + req.query.username + ". . .")
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.query.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminDeleteUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not delete")
      }
      else {
        console.log(">>>> Successfully deleted user")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}

exports.confirmUser = function (req, res, next) {
  console.log(">>>> Confirming " + req.body.username + ". . .")

  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.body.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminConfirmSignUp(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not confirm")
      }
      else {
        console.log(">>>> Successfully confirmed user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}

exports.editUserAdmin = function (req, res, next) {
  console.log(">>>> Updating user " + req.body.username + " attributes. . .")
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.body.username,
    UserAttributes: req.body.attributes,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminUpdateUserAttributes(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}

exports.joinGroup = function (req, res, next) {
  console.log(">>>> Adding " + req.body.username + " to Research group. . .")
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.body.username,
    GroupName: req.body.group,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminAddUserToGroup(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not add to group")
      }
      else {
        console.log(">>>> Successfully added user")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}

exports.removeGroup = function (req, res, next) {
  console.log(">>>> Removing " + req.body.username + " from Research group. . .")
  var params = {
    UserPoolId: USER_POOL_ID,
    Username: req.body.username,
    GroupName: req.body.group,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminRemoveUserFromGroup(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not remove user from group")
      }
      else {
        console.log(">>>> Successfully removed user")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}

exports.editUser = function (req, res, next) {
  console.log(">>>> Updating user " + req.body.username + " attributes. . .")
  var params = {
    AccessToken: req.body.token,
    UserAttributes: req.body.attributes,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.updateUserAttributes(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}

exports.changePassword = function (req, res, next) {
  console.log(">>>> Updating user's password")
  var params = {
    AccessToken: req.body.token,
    PreviousPassword: req.body.previous,
    ProposedPassword: req.body.proposed,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.changePassword(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}

exports.resendConfirmation = function (req, res, next) {
  console.log(">>>> Resending confirmation email to " + req.body.username + ". . .")
  var params = {
    ClientId: req.body.id,
    Username: req.body.username,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.resendConfirmationCode(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}

exports.getSelf = function (req, res, next) {
  console.log(">>>> Grabbing user information. . .")
  var params = {
    AccessToken: req.body.token,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.getUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could find user.")
      }
      else {
        console.log(">>>> Successfully obtained data!")
        resolve(data)
        res.status(200).send(data);
      }
    })
  });

  return payLoad
}

exports.deleteSelf = function (req, res, next) {
  console.log(">>>> Deleting user. . .")
  var params = {
    AccessToken: req.body.token,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.deleteUser(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error: could not update user")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send(data);
      }
    })
  });

  return payLoad
}

exports.verifyEmail = function (req, res, next) {
  console.log(">>>> Verifying user's email address. . .")
  var params = {
    AccessToken: req.body.token,
    AttributeName: 'email',
    Code: req.body.code,
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.verifyUserAttribute(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(400).send("Wrong code!")
      }
      else {
        console.log(">>>> Successfully updated user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}

exports.resendCode = function (req, res, next) {
  console.log(">>>> Resending email address verification code. . .")
  var params = {
    AccessToken: req.body.token,
    AttributeName: 'email',
  };

  const payLoad = new Promise((resolve, reject) => {
    AWS.config.update({ region: USER_POOL_REGION, 'accessKeyId': AWS_ACCESS_KEY_ID, 'secretAccessKey': AWS_SECRET_KEY });
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.getUserAttributeVerificationCode(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
        res.status(500).send("Error!")
      }
      else {
        console.log(">>>> Successfully resent code to user!")
        resolve(data)
        res.status(200).send("Success");
      }
    })
  });

  return payLoad
}
