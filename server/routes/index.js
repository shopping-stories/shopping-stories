// routes/index.js

// import data controller
exports.import = require('./import');

// cognito controller
exports.cognito = require('./cognito');

// neo4j controller 
exports.neo4j = require('./neo4j')
