// routes/utils/import-driver/importDriver.js

const neo4j = require("neo4j-driver"); // Imports the neo4j server commands

class Import {

    // constructor
    constructor(_username, _password, _url) {
        this.username = _username;
        this.password = _password;
        this.url = _url;
        this.driver = this._establishDriver();
        this.query = '';
    }

    // Establish Driver
    _establishDriver() {
        // Validating the user info
        const authToken = neo4j.auth.basic(this.username, this.password);

        // Returns the driver object
        return neo4j.driver(this.url, authToken);
    }

    // Establish Session
    _establishSession() {
        // Returns a connection to the server
        return this.driver.session();
    }

    _queryBuilder(jsonObjToParse, label) {
        // get the keys needed for each object
        const objKeys = Object.keys(jsonObjToParse);
        let query = `CREATE (:${label}{\n`;

        // String Builder
        objKeys.forEach((key, index) => {
            if (objKeys.length - 1 > index) {
                query += `${key}:"${jsonObjToParse[key]}",\n`;
            } else {
                query += `${key}:"${jsonObjToParse[key]}"\n`;
            }
        });
        query += `})`;
        // Returning the concatenated query
        return query;
    }

    // runs built query
    async runQuery() {
      var returnStatus = {status: 200, message: 'Success!'}
      try {
        const session = this._establishSession();
        // Running the session and closing the session.
        await session
            .run(this.query) // run query
            .then(() => session.close()) // close session
            .finally(() => this.query = '') // dump query

      } catch(err) {
        // syntax error in the file
        if (err.code == 'Neo.ClientError.Statement.SyntaxError') {
          var str = err.toString();
          return returnStatus = { status: 201, message: str.slice(str.indexOf('(offset:'),
                          str.indexOf('at captureStacktrace'))}
        }
        // unknown error with file
        else
          return returnStatus = {status: 202, message: err}
      }
      return returnStatus;
    }

    /**
     *
     * Creates nodes in Neo4j of label Account
     * @param {*} jsonObj
     *
     */
    accounts(jsonObj) {
        const accounts = jsonObj;

        accounts.map((cur) => {
            this.query += this._queryBuilder(cur, "Account");
        });
    }

    // Import Categories
    categories(jsonObj) {
        const session = this._establishSession();
    }

    /**
     *
     * Creates nodes in Neo4j of label People
     * @param {*} jsonObj
     *
     */
    people(jsonObj) {
        const people = jsonObj;

        people.map((cur) => {
            this.query += "\n" + this._queryBuilder(cur, "Person");
        });
    }

    /**
     *
     * Creates nodes in Neo4j of label Place
     * @param {*} jsonObj
     *
     */
    places(jsonObj) {
        const places = jsonObj;

        places.map((cur) => {
            this.query += this._queryBuilder(cur, "Place");
        });
    }

    /**
     *
     * Creates nodes in Neo4j of label Organization
     * @param {*} jsonObj
     *
     */
    organizations(jsonObj) {
        const organizations = jsonObj;

        organizations.map((cur) => {
            this.query += this._queryBuilder(cur, "Organization");
        });
    }

    /**
     *
     * Creates nodes in Neo4j of label Transaction
     * Creates nodes in Neo4j of label Entry
     * @param {*} jsonObj
     *
     */
    transactions(jsonObj) {
        // Objects to be parsed and uploaded
        const transactions = jsonObj.transactions;
        const entries = jsonObj.entries;

        // Building the query for transactions
        transactions.map((cur) => {
            this.query += "\n" + this._queryBuilder(cur, "Transaction");
        });

        // Building the query for entries
        entries.map((cur) => {
            this.query += "\n" + this._queryBuilder(cur, "Entry");
        });
    }
}

module.exports = Import;
