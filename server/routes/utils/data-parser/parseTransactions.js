

/**
 * Expects a json object and returns a parsed json object
 * @param { JSON } obj 
 */
function parseTransactions(obj) {

    let transaction = {
        reel_id: obj.reel,
        entry_id: parseInt(obj.entryId || obj.EntryId),
        folio_page: obj.folioPage || obj.FolioPage,
        folio_reference: obj.folioReference || obj.FolioReference,
        original_entry: obj.originalEntry || obj.OriginalEntry,
        challenges: obj.challenges || obj.Challenges,
        gen_mat: obj.gen_mat === '1' ? 1 : undefined,
        marginalia: obj.marginalia || obj.Marginalia,
        store_name: obj.storeName || obj.StoreName,
        ledger_year: obj.ledgerYear || obj.LedgerYear,
        prefix: obj.prefix || obj.Prefix,
        suffix: obj.suffix || obj.Suffix,
        first_name: obj.accFirstname || obj.accFirstName,
        last_name: obj.accLastname || obj.accLastName,
        profession: obj.profession || obj.Profession,
        location: obj.location || obj.Location,
        reference: obj.reference || obj.Reference,
        transaction_type: getTransactionType(obj.debitCredit || obj.DebitCredit),
        entry: obj.entry || obj.Entry,
        entry_type: obj.entryType || obj.EntryType,
        transaction_date: createTransactionDate(obj.day, obj.month, obj.year),
        year: obj.year,
        comments: obj.comments || obj.Comments,
        date_created: getDate()
    }
    // Data Processing for the transactions
    if (transaction.ledger_year) transaction.ledger_year = transaction.ledger_year.replace(/[A-Za-z*]/, '/')
    if (transaction.store_name) transaction.store_name = transaction.store_name.toLowerCase()
    if (transaction.prefix) transaction.prefix = transaction.prefix.toLowerCase()
    if (transaction.first_name) transaction.first_name = transaction.first_name.toLowerCase()
    if (transaction.last_name) transaction.last_name = transaction.last_name.toLowerCase()
    if (transaction.suffix) transaction.suffix = transaction.suffix.toLowerCase()
    if (transaction.profession) transaction.location = transaction.profession.toLowerCase()
    if (transaction.location) transaction.location = transaction.location.toLowerCase()
    if (transaction.transaction_type) transaction.transaction_type = transaction.transaction_type.toLowerCase()
    if (transaction.profession) transaction.profession = transaction.profession.toLowerCase()
    transaction.transaction_date = createTransactionDate(obj.day, obj.month, obj.year)

    transaction.transaction_date = createTransactionDate(obj.day, obj.month, obj.year)

    if (transaction.entry_type) transaction.entry_type = transaction.entry_type.toLowerCase()


    // Parsing the currencyType
    if (obj.currencyType) transaction.currency_type = obj.currencyType.toLowerCase()

    // Populate the currencyMeasure
    if (transaction.currency_type) {
        transaction.currency_measure = []
        let temp = obj.currencyMeasure ? obj.currencyMeasure.toLowerCase().split(',') : undefined
        transaction.currency_type.split(',').forEach((element, i) => {
            if (element === 'sterling') transaction.currency_measure[i] = 'currency'
            else if (element === 'virginia') transaction.currency_measure[i] = 'currency'
            else if (transaction.currency_measure[i]) transaction.currency_measure = temp.shift()
            else transaction.currency_measure[i] = ''
        })
        transaction.currency_measure.toString()
    }

    // Populate the currencyParts
    try {
        transaction.currency_parts = obj.currencyParts.split(',')
        transaction.currency_parts.forEach((element, i, array) => {
            let currency = element.replace('L', ',')
            currency = currency.replace('s', ',')
            array[i] = currency
        })
    } catch (e) {
        transaction.currency_parts = parseFloat(obj.currencyParts) || undefined
    }

    return transaction

}


/**
 * Returns an object containing the parsed data from the excel file
 * @param {*} obj 
 */
function parseEntries(obj) {

    function createEntry() {
        return {
            reel_id: obj.reel || obj.Reel,
            folio_page: obj.folioPage || obj.FolioPage,
            entry_id: parseInt(obj.entryId || obj.EntryId),
            date_created: getDate(),
            transaction_date: createTransactionDate(obj.day, obj.month, obj.year),
            transaction_type: getTransactionType(obj.debitCredit || obj.DebitCredit),
            year: obj.year,
            challenges: obj.challenges || obj.Challenges,
            original_entry: parseInt(obj.originalEntry || obj.OriginalEntry) || undefined,
            comments: obj.comments || obj.Comments,
        }
    }


    // creating the initial entry
    let entry = createEntry();

    if (obj.gen_mat) entry.gen_mat = obj.genMat


    // If there is a conflict/challenge then skip that row and move to the next
    if (entry.genMat || entry.challenges || entry.original_entry || obj.entry === undefined) {
        entry.entry = obj.entry
        return [entry]
    }

    // Parsing the entries
    let entryItem = obj.entry.split(';')


    // For each item in the transaction
    let parsedItems = []
    entryItem.forEach(el => {
        el = el.replace(/\r?\n|\r/, '') // Removing stray excel file EOL chars
        el = el.trim()
        el = el.split(',')

        // Building each item on the transaction
        let item = createEntry();

        // {
        //     reel_id: obj.reel || obj.Reel,
        //     folio_page: obj.folioPage || obj.FolioPage,
        //     entry_id: parseInt(obj.entryId) || parseInt(obj.EntryId),
        //     transaction_type: obj.debitCredit || obj.DebitCredit,
        //     date_created: getDate()
        // }

        if (item.transaction_type) item.transaction_type = item.transaction_type.toLowerCase()

        // Date of purchase
        if (obj.day && obj.month && obj.year) {
            item.transaction_date = [obj.month, obj.day, obj.year].join('-')
        }
        else {
            item.year = obj.year
        }

        // Parsing the id
        if (item.entry_id) item.entry_id = parseInt(item.entry_id)

        // Populating the attributes 
        item.amount = parseFloat(el[0]) || undefined
        item.unit_measure = el[1] ? el[1].toLowerCase() : undefined
        item.descriptor = el[2] ? el[2].toLowerCase().replace('-', ',') : undefined
        item.item = el[3] ? el[3].toLowerCase() : undefined

        // Accounting for unit price
        if (el.length > 5) {
            item.unit_price = el[4] ? currencySplitter(el[4]) : el[4]
            item.item_price = el[5] ? currencySplitter(el[5]) : el[5]
        }
        else {
            item.item_price = el[4] ? currencySplitter(el[4]) : el[4]
        }

        // Pushing the parsed data
        parsedItems.push(item)
    })

    // returning array of parsed items 
    return parsedItems

}

// Parses the currency
function currencySplitter(currency) {
    currency = currency.replace('L', ',')
    currency = currency.replace('s', ',')
    return currency
}

// Returns a formatted date time stamp mm-dd-yyyy hh:mm:ss
function getDate() {
    let currentDate = new Date()
    return `${currentDate.getMonth()}-${currentDate.getDay()}-${currentDate.getFullYear()} ${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}`
}

function getTransactionType(transaction) {
    if (transaction === 'Dr') {
        return 'debit'
    }

    if (transaction === 'Cr') {
        return 'credit'
    }
}



function createTransactionDate(day, month, year) {
    if (day && month && year) {
        return [day, month, year].join('-')
    }
    // if there is not a day, month or year
    return undefined
}






function parseLedgerDriver(jsonObj) {

    let parsedJsonObj = {}

    // Inits the arrays to be populated
    parsedJsonObj['transactions'] = []
    parsedJsonObj['entries'] = []

    jsonObj.forEach(row => {

        // Pushing parsed data to JSON
        parsedJsonObj['transactions'].push(parseTransactions(row))


        // Adding the parsed array of data to the JSON
        parseEntries(row).forEach(el => {
            parsedJsonObj['entries'].push(el)
        })
    })

    // Returning the LedgerJson containing the Transaction and Entries
    return parsedJsonObj
}


module.exports = { parseLedgerDriver }