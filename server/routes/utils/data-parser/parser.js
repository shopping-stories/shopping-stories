// routes/utils/data-parser/parser.js

const xlsx = require('xlsx')

const { parseLedgerDriver } = require('./parseTransactions');
const { parsePeopleDriver } = require('./parsePeople');
const { parsePlacesDriver } = require('./parsePlaces');
const { parseAccountDriver } = require('./parseAccounts')
const { parseOrganizationDriver } = require('./parseOrganizations');

// The base class
class Parser {

    /**
     *
     * @param {File} filePath
     */
    constructor(filePath) {
        this.fileToParse = filePath;
        this.worksheetJson = this._convertToWorksheetJson()
    }

    _convertToWorksheetJson() {
        const workbook = xlsx.read(this.fileToParse, {type:'buffer'});
        const worksheetToUse = workbook.SheetNames[0]
        const worksheet = workbook.Sheets[worksheetToUse]
        return xlsx.utils.sheet_to_json(worksheet)
    }

    /**
     * Parses the ledger data and returns a json containing
     * an array of transaction data, and an array of entries.
     * @note Uses worksheetJson
     * @returns {JSON} Parsed Ledger
     */
    Transactions() {
        return parseLedgerDriver(this.worksheetJson)
    }


    /**
     * Parses Master list template for people and returns a
     * JSON containing an array of people and their attributes.
     * @note Uses worksheetJson
     * @returns {JSON} Parsed People
     */
    People() {
        return parsePeopleDriver(this.worksheetJson)
    }


    /**
     * Parses Master List template for places and returns a
     * JSON object containing array of places and their attributes.
     * @note Uses worksheetJson
     * @returns {JSON} Parsed Places
     */
    Places() {
        return parsePlacesDriver(this.worksheetJson)
    }


    /**
     * Parses Master List template for organizations and returns a
     * JSON object containing array of organizations and their attributes.
     * @note Uses worksheetJson
     * @returns {JSON} Parsed Organizations
     */
    Organizations() {
        return parseOrganizationDriver(this.worksheetJson)
    }


    /**
     * Parses Master List template for accounts and returns a
     * JSON object containing array of accounts and their attributes.
     * @note Uses worksheetJson
     * @returns {JSON} Parsed Accounts
     */
    Accounts() {
        return parseAccountDriver(this.worksheetJson)
    }

}

module.exports = Parser
