function parseAccount(obj) {

    let account = {
        name: obj.LastName,
        associated: obj.FirstName,
        prefix: obj.Prefix,
        suffix: obj.Suffix,
        type: obj.Profession,
        category: obj.ProfessionCategory,
        location: obj.Location,
        reference: obj.Reference,
        variations: obj.Variations,
        store: obj.Store,
        account: obj.Account
    }

    if (account.name) account.name = account.name.toLowerCase()
    if (account.associated) account.associated = account.associated.toLowerCase()
    if (account.prefix) account.prefix = account.prefix.toLowerCase()
    if (account.suffix) account.suffix = account.suffix.toLowerCase()
    if (account.type) account.type = account.type.toLowerCase()
    if (account.category) account.category = account.category.toLowerCase()
    if (account.location) account.location = account.location.toLowerCase()
    if (account.reference) account.reference = account.reference.toLowerCase()
    if (account.variations) account.variations = account.variations.toLowerCase()
    if (account.store) account.store = account.store.toLowerCase()
    if (account.account) account.account = account.account.toLowerCase()

    return account
}

function parseAccountDriver(obj) {

    let parsedData = []

    obj.forEach(el => {
        if (el.Reference === 'Account'){
            parsedData.push(parseAccount(el))
        }
    })

    return parsedData
}


module.exports = { parseAccountDriver }