# ss-server
Shopping Stories server repo

```
.
├── app.js
├── bin
│   └── www
├── neo4j
│   ├── configure.js
│   ├── dbUtils.js
│   └── neo4j.js
├── old
│   ├── package.json
│   ├── package-lock.json
│   └── README.md
├── package.json
├── package-lock.json
├── public
│   ├── images
│   ├── javascripts
│   └── stylesheets
│       └── style.css
├── README.md
├── routes
│   ├── index.js
│   └── users.js
├── src
│   ├── data
│   │   ├── 001_transactions.json
│   │   ├── XXX_transactions.json
│   │   ├── 161_transactions.json
│   │   ├── accounts.json
│   │   ├── organizations.json
│   │   ├── people.json
│   │   └── places.json
│   └── import.js
└── views
    ├── error.jade
    ├── index.jade
    └── layout.jade
```