require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const routes = require('./routes/index');
const bodyParser = require("body-parser");
const cors = require('cors')
const aws = require('aws-sdk')
const auth = require('./auth/index')

const app = express();

// file upload
const multer = require('multer');
const multerS3 = require('multer-s3');
aws.config.update({
  secretAccessKey: process.env.AWS_SECRET_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  region: process.env.USER_POOL_REGION,
});
s3 = new aws.S3();
const uploadShoppingStories = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'shoppingstories',
    key: function(req, file, cb) {
      cb(null, file.originalname);
    }
  })
})

const uploadSSDocuments = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'ss-documents',
    key: function(req, file, cb) {
      cb(null, file.originalname)
    }
  })
})

// parse encoded url data
app.use(bodyParser.urlencoded({ extended: false}));
// parse json packages
app.use(bodyParser.json());
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  // check for headers
  if (!req.headers.authorization) {
    return res.status(403).json({ error: 'Forbidden!' })
  }

  // check for token
  if (!auth.auth.checkToken(req)) {
    return res.status(401).json({ error: 'Unauthorized!' })
  }

  // user has proper headers and credentials
  next();
});

// import routes
app.get('/api/v1/import/count', routes.import.count)
app.post('/api/v1/import/filecheck', routes.import.filecheck)
app.get('/api/v1/import/files', routes.import.files)
app.put('/api/v1/import/transactions', uploadShoppingStories.single("file"), routes.import.transactions)
app.put('/api/v1/import/master', uploadShoppingStories.single("file"), routes.import.master)
app.delete('/api/v1/import/file', routes.import.deleteFile)
app.put('/api/v1/import/file/upload', uploadShoppingStories.single("file"), routes.import.uploadFile)
app.get('/api/v1/import/file/download', routes.import.downloadFile)

// new
app.put('/api/v1/import/upload/documents', uploadSSDocuments.single("file"), routes.import.uploadFileDocuments)
app.delete('/api/v1/import/file/documents', routes.import.deleteFileDocuments)
app.get('/api/v1/import/file/download/documents', routes.import.downloadFileDocuments)

// cognito routes
app.get('/api/v1/cognito/users', routes.cognito.getUsers)
app.get('/api/v1/cognito/user/groups', routes.cognito.getGroupsForUser)
app.get('/api/v1/cognito/groups/researcher', routes.cognito.getResearchGroup)
app.get('/api/v1/cognito/user', routes.cognito.getUser)
app.delete('/api/v1/cognito/user/delete', routes.cognito.deleteUser)
app.patch('/api/v1/cognito/user/join', routes.cognito.joinGroup)
app.patch('/api/v1/cognito/user/remove', routes.cognito.removeGroup)
app.patch('/api/v1/cognito/user/edit', routes.cognito.editUserAdmin)
app.patch('/api/v1/cognito/user/confirm', routes.cognito.confirmUser)
app.patch('/api/v1/cognito/user/attributes', routes.cognito.editUser)
app.patch('/api/v1/cognito/user/password', routes.cognito.changePassword)
app.patch('/api/v1/cognito/user/resend', routes.cognito.resendConfirmation)
app.patch('/api/v1/cognito/user/self', routes.cognito.getSelf)
app.patch('/api/v1/cognito/user/self/delete', routes.cognito.deleteSelf)

app.patch('/api/v1/cognito/verify/email', routes.cognito.verifyEmail)
app.patch('/api/v1/cognito/resend', routes.cognito.resendCode)

// neo4j routes
app.get('/api/v1/neo4j/data', routes.neo4j.data)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
