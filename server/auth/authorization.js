// auth/authorization.js

// checks token before any api call is completed
exports.checkToken = function (req, res, next) {

  // check for headers
  if (!req.headers)
    return false

  // check for token
  else if (!req.headers['authorization'])
    return false

  // grab token
  const auth = req.headers['authorization']
  const token = auth && auth.split(' ')[1]

  // if exists
  if (!token)
    return false

  // check if correct
  if (token !== process.env.SECRET_TOKEN)
    return false

  // user is authorized
  return true
}
